source: https://github.com/thatisuday/js-plugin-starter.git [

# Capibara
VanillaJS (_pure JavaScript_) plugin Timeline for Irricrops

> Here, `dist` is your destination directory in which the boilerplate code will be copied.

# Instructions
- Use `npm install` command to install dependencies.
- Execute command `npm run start` to run webpack development server and top open preview in the browser.
- Execute command `npm run build` to create plugin distribution files in the `dist` directory.
- Execute command `npm run build-dev` to create plugin development files in the `dist` directory.
- Tweak configuration inside `config` folder if necessary.
- Configure plugin API using [**this**](https://webpack.js.org/configuration/output/) documentation.
