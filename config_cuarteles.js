var config = {
  selector: "#timeline",
  timeline: {
    start: "2021-10-29",
    end: "2021-12-31",
    dark: false,
    withDarkToggle: true,
    dateSelectable: true,
    loading: false,

    contextMenu: {
      options: [
        {
          icon:
            "M19,3L13,9L15,11L22,4V3M12,12.5A0.5,0.5 0 0,1 11.5,12A0.5,0.5 0 0,1 12,11.5A0.5,0.5 0 0,1 12.5,12A0.5,0.5 0 0,1 12,12.5M6,20A2,2 0 0,1 4,18C4,16.89 4.9,16 6,16A2,2 0 0,1 8,18C8,19.11 7.1,20 6,20M6,8A2,2 0 0,1 4,6C4,4.89 4.9,4 6,4A2,2 0 0,1 8,6C8,7.11 7.1,8 6,8M9.64,7.64C9.87,7.14 10,6.59 10,6A4,4 0 0,0 6,2A4,4 0 0,0 2,6A4,4 0 0,0 6,10C6.59,10 7.14,9.87 7.64,9.64L10,12L7.64,14.36C7.14,14.13 6.59,14 6,14A4,4 0 0,0 2,18A4,4 0 0,0 6,22A4,4 0 0,0 10,18C10,17.41 9.87,16.86 9.64,16.36L12,14L19,21H22V20L9.64,7.64Z",
          label: "Copiar",
          onClick: ({ evt }) => {
            alert("copiado");
          },
          showIf: ({ name }) => name == "onTable",
          enabler: ({ item, date }) => {
            if (item && item.data.find((d) => d.date == date)) {
              return true;
            }
            return false;
          },
        },
        {
          label: "Pegar",
          showIf: ({ name }) => name == "onTable",
          onClick: () => {
            alert("pegado");
          },
        },

        {
          label: "copiar dia",
          showIf: ({ name }) => name == "onDateHeader",
          onClick: () => {
            alert("pegado");
          },
        },

        {
          label: "Pegar dia",
          showIf: ({ name }) => name == "onDateHeader",
          onClick: () => {
            alert("pegado");
          },
        },
      ],
    },

    plots: {
      vertical: [],
      horizontal: [],
    },
    tabs: [
      {
        id: 1,
        key: "foliar",
        label: "Aplicaciones",
        isHidden: true,
        color: "c-green",
        legends: [
          {
            id: 1,
            key: "foliar",
            color: "c-green",
            label: "foliar",
            shape: "circle",
            active: true,
          },
          {
            id: 2,
            key: "insecticida",
            color: "c-red",
            label: "insecticida",
            shape: "circle",
            active: false,
          },
          {
            id: 3,
            key: "herbicida",
            color: "c-amber",
            label: "herbicida",
            shape: "circle",
            active: true,
          },
          {
            id: 4,
            key: "fungicida",
            color: "c-blue",
            label: "fungicida",
            shape: "circle",
            active: true,
          },
        ],
        floatBox: {
          nums: [
            {
              varName: "foliar.value",
              tabKey: "foliar",
              color: "c-green",
              shape: "bar",
            },
            {
              varName: "next.foliar.value",
              tabKey: "foliar",
              color: "c-green",
              shape: "bar-outlined",
            },

            {
              varName: "insecticida.value",
              tabKey: "insecticida",
              color: "c-red",
              shape: "bar",
            },
            {
              varName: "next.insecticida.value",
              tabKey: "insecticida",
              color: "c-red",
              shape: "bar-outlined",
            },
            {
              varName: "herbicida.value",
              tabKey: "herbicida",
              color: "c-amber",
              shape: "bar",
            },
            {
              varName: "fungicida.value",
              tabKey: "fungicida",
              color: "c-blue",
              shape: "bar",
            },
            {
              varName: "next.herbicida.value",
              tabKey: "herbicida",
              color: "c-amber",
              shape: "bar-outlined",
            },
            {
              varName: "next.fungicida.value",
              tabKey: "fungicida",
              color: "c-blue",
              shape: "bar-outlined",
            },
          ],
        },
        inputs: [
          {
            label: "Mojamiento l",
            type: "number",
            suffix: " ltrs/ha",
            key: "input_foliar",
            varName: "values.foliar.value",
          },

          {
            label: "Aplicaciones Dosis",
            type: "title",
            suffix: " suffix",
          },
          {
            label: "Aplicacionesi Dosis",
            type: "input-box",
            key: "productos_list",
            varName: "values.aplicaciones",
            inputs: [
              {
                id: 1,
                label: "Producto numero 1 con un nombre largo",
                tooltip: "Producto numero 1 con un nombre largo ______",
                colorIcon: "c-green",
                key: "producto_1",
              },
              {
                id: 2,
                label: "P 2",
                colorIcon: "c-green",
                key: "producto_2",
              },
              {
                id: 3,
                label: "P 3",
                colorIcon: "c-green",
                key: "producto_3",
              },
              {
                id: 4,
                label: "P 3",
                colorIcon: "c-red",
                key: "producto_3",
              },
              {
                id: 5,
                label: "P 3",
                colorIcon: "c-red",
                key: "producto_3",
              },
              {
                id: 6,
                label: "P 3",
                colorIcon: "c-blue",
                key: "producto_3",
              },
              {
                id: 7,
                label: "P 3",
                colorIcon: "c-orange",
                key: "producto_3",
              },
            ],
          },
        ],
      },
      {
        id: 2,
        key: "insecticida",
        label: "Insecticida",
        isHidden: true,
        color: "c-blue",
        legends: [
          {
            id: 1,
            key: "foliar",
            color: "c-green",
            label: "foliar",
            //shape: "circle",
            active: true,
          },
          {
            id: 2,
            key: "insecticida",
            color: "c-red",
            label: "insecticida",
            //shape: "circle",
            active: false,
          },
          {
            id: 3,
            key: "herbicida",
            color: "c-amber",
            label: "herbicida",
            shape: "circle",
            active: true,
          },
          {
            id: 4,
            key: "fungicida",
            color: "c-blue",
            label: "fungicida",
            shape: "circle",
            active: true,
          },
        ],
        floatBox: {
          nums: [
            {
              varName: "foliar.value",
              tabKey: "foliar",
              color: "c-green",
              shape: "bar",
            },
            {
              varName: "next.foliar.value",
              tabKey: "foliar",
              color: "c-green",
              shape: "bar-outlined",
            },

            {
              varName: "insecticida.value",
              tabKey: "insecticida",
              color: "c-red",
              shape: "bar",
            },
            {
              varName: "next.insecticida.value",
              tabKey: "insecticida",
              color: "c-red",
              shape: "bar-outlined",
            },
            {
              varName: "herbicida.value",
              tabKey: "herbicida",
              color: "c-amber",
              shape: "bar",
            },
            {
              varName: "fungicida.value",
              tabKey: "fungicida",
              color: "c-blue",
              shape: "bar",
            },
            {
              varName: "next.herbicida.value",
              tabKey: "herbicida",
              color: "c-amber",
              shape: "bar-outlined",
            },
            {
              varName: "next.fungicida.value",
              tabKey: "fungicida",
              color: "c-blue",
              shape: "bar-outlined",
            },
          ],
        },
        inputs: [
          {
            label: "Mojamiento ¿?",
            type: "number",
            suffix: " suffix",
            key: "input_foliar",
            varName: "values.foliar.value",
          },

          {
            label: "Aplicaciones Dosis",
            type: "title",
            suffix: " suffix",
          },
          {
            label: "Aplicacionesi Dosis",
            type: "input-box",
            key: "productos_list",
            varName: "values.aplicaciones",
            inputs: [
              {
                id: 1,
                label: "Producto numero 1 con un nombre largo",
                tooltip: "Producto numero 1 con un nombre largo ______",
                colorIcon: "c-green",
                key: "producto_1",
              },
              {
                id: 2,
                label: "P 2",
                colorIcon: "c-green",
                key: "producto_2",
              },
              {
                id: 3,
                label: "P 3",
                colorIcon: "c-green",
                key: "producto_3",
              },
              {
                id: 4,
                label: "P 3",
                colorIcon: "c-red",
                key: "producto_3",
              },
              {
                id: 5,
                label: "P 3",
                colorIcon: "c-red",
                key: "producto_3",
              },
              {
                id: 6,
                label: "P 3",
                colorIcon: "c-blue",
                key: "producto_3",
              },
              {
                id: 7,
                label: "P 3",
                colorIcon: "c-orange",
                key: "producto_3",
              },
            ],
          },
        ],
      },
    ],
    corner: {
      left: {
        label: "Cuarteles",
      },
      initial: {},
      extend: {},
      right: {
        label: "",
      },
    },
    footer: {
      enabled: false,
    },
    nav: {
      left: {
        initial: {
          mobile: {
            width: 150,
          },
          vars: [],
        },
        extend: {
          enabled: false,
          vars: [
            {
              label: "Clon",
              key: "clon",
              varName: "clon",
            },
            {
              label: "ha",
              varName: "ha",
              round: 2,
            },
            {
              label: "freq",
              varName: "freq",
              input: "number",
              round: 2,
            },
            {
              label: "freq foliar",
              varName: "freq",
              input: "number",
              round: 2,
              onlyTab: "foliar",
            },
          ],
        },
        buttons: {
          enabled: false,
          vars: [],
        },
      },
      right: {},
    },
    header: {
      height: 70,
      extend: {
        enabled: false,
        data: [],
      },
      rightButtons:[
        {
          icon:
            "M19,3L13,9L15,11L22,4V3M12,12.5A0.5,0.5 0 0,1 11.5,12A0.5,0.5 0 0,1 12,11.5A0.5,0.5 0 0,1 12.5,12A0.5,0.5 0 0,1 12,12.5M6,20A2,2 0 0,1 4,18C4,16.89 4.9,16 6,16A2,2 0 0,1 8,18C8,19.11 7.1,20 6,20M6,8A2,2 0 0,1 4,6C4,4.89 4.9,4 6,4A2,2 0 0,1 8,6C8,7.11 7.1,8 6,8M9.64,7.64C9.87,7.14 10,6.59 10,6A4,4 0 0,0 6,2A4,4 0 0,0 2,6A4,4 0 0,0 6,10C6.59,10 7.14,9.87 7.64,9.64L10,12L7.64,14.36C7.14,14.13 6.59,14 6,14A4,4 0 0,0 2,18A4,4 0 0,0 6,22A4,4 0 0,0 10,18C10,17.41 9.87,16.86 9.64,16.36L12,14L19,21H22V20L9.64,7.64Z",
          onClick: ({ evt }) => {
            alert("test");
          },
        },
      ]
    },
    day: {
      width: 40,
      height: 40,
      mobile: {
        width: 30,
        height: 35,
      },
    },
    detailDay: {
      capsules: [
        {
          title: "Foliar",
          color: "c-green",
          shape: "outlined",
          values: [
            {
              name: "foliar",
              varName: "foliar.value",
              suffix: " ltrs ¿?",
            },
          ],
        },
        {
          title: "Prox. Aplicación Foliar",
          color: "c-green",
          shape: "outlined",
          values: [
            {
              name: "Próxima Foliar",
              varName: "next.foliar.value",
              suffix: " ltrs ¿?",
            },
          ],
        },
        {
          title: "Insecticida",
          color: "c-red",
          values: [
            {
              name: "insecticida",
              varName: "insecticida.value",
              suffix: " ltrs ¿?",
            },
          ],
        },
        {
          title: "Herbicida",
          color: "c-orange",
          values: [
            {
              name: "herbicida",
              varName: "herbicida.value",
              suffix: " ltrs ¿?",
            },
          ],
        },
        {
          title: "Fungicida",
          color: "c-blue",
          values: [
            {
              name: "fungicida",
              varName: "fungicida.value",
              suffix: " ltrs ¿?",
            },
          ],
        },
      ],
    },
  },
};
