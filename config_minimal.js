var config = {
  selector: "#timeline",
  timeline: {
    start: "2020-11-15",
    end: "2020-12-15",
    //"dark": true,
    //"loading": false,
    //editable: false,
    //selectable:false,
    //plots: {
      //horizontal: [],
      //vertical: [],
    //},
    tabs: [
      {
        id: 1,
        label: "Riego",
        color: "c-blue",
        floatBox: {
          nums: [
            {
              varName: "riego.mm",
              color: "c-blue",
            }
          ],
          //dots: [],
        },
        inputs: [
          {
            label: "Horas de Riego",
            type: "time",
            key: "horas_riego",
            varName: "values.riego.hh",
          },
          {
            label: "Volumen de Riego",
            type: "number",
            suffix: "m<sup>3</sup>",
            key: "volumen",
            varName: "values.riego.volumen",
          },
          {
            label: "¿Con fertilización?",
            type: "checkbox",
            key: "con_fertilizacion",
            varName: "values.con_fertilizacion",
          },
          {
            label: "fertilización",
            type: "input-box",
            key: "fertilizacion_list",
            showIf: "con_fertilizacion",
            varName: "values.fertilizacion",
            inputs: [
              {
                id: 1,
                label: "P 1",
                key: "producto_1",
              },
              {
                id: 2,
                label: "P 2",
                key: "producto_2",
              },
              {
                id: 3,
                label: "P 3",
                key: "producto_3",
              },
            ],
          },
          {
            label: "Mensaje",
            type: "message",
            key: "msg_input",
            varName: "message.value",
          },
        ],
      },
    ],
    corner: {
      left: {
        label: "Sectores",
      },
      initial: {},
      extend: {},
      right: {
        label: "Inputs",
      },
    },
    nav: {
      columnWidth: 50,
      left: {
        initial: {
          //enabled: false,
          width: 200,
          mobile: {
            width: 100,
          },
          vars: [
            {
              label: "ha",
              varName: "ha",
              round: 2,
            },
          ],
        },
        extend: {
          enabled: false,
          vars: [
            {
              label: "Riego Nominal <br /> mm",
              varName: "riego_nominal_mm",
              input: "number",
              key: "riego_nominal_mm",
            },
            {
              label: "Riego Nominal <br /> HH",
              varName: "riego_nominal_hh",
              input: "hour",
              key: "riego_nominal_hh",
            },
            {
              label: "Rep <br /> %",
              varName: "rep",
            },
            {
              label: "Rep <br /> Hoy",
              varName: "rep_hoy",
              round: 1,
              suffix: "%",
            },
          ],
        },
        /*buttons: {
          showInMobile: true,
          width: 90,
          vars: [
            {
              shape: "drop",
              tooltip: "tooltip hover",
              className: "",
              key: "icon-estados-fenologicos",
            },
            {
              shape: "circle",
              tooltip: "tooltip hover",
              className: "",
              key: "icon-etapas-fertilizacion",
            },
          ],
        },*/
      },
      right: {},
    },
    header: {
      top: {
        //height: 80,
      },
      extend: {
        //height: 50,
        enabled: false,
        data: [
          {
            label: "Eto",
            color: "green",
            average: true,
            isPersistent: true,
            editableFuture: false,
            editableToday: false,
            data: [],
          },
        ],
      },
    },
    day: {
      width: 35,
      height: 35,
          mobile: {
            width: 30,
            height: 30,
          },
    },
    detailDay: {
      pillsTitle: "Unidades / ha:",
      pillsVarName: "elementos",
      capsules: [
        {
          title: "Riego Real",
          color: "c-blue",
          values: [
            {
              name: "Riego MM",
              varName: "riego.mm",
              suffix: " mm",
            },
            {
              name: "Riego HH",
              varName: "riego.hh",
              suffix: " hrs",
            },
            {
              name: "Volumen",
              varName: "riego.volumen",
              suffix: " m<sup>3</sup>",
            },
          ],
        },
      ],
    },
  },
  data: [],
};
