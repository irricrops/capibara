const path              = require('path');
const webpack           = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const config            = require('config');

/*-------------------------------------------------*/

module.exports = {
    // webpack optimization mode
    mode: "development", //( process.env.NODE_ENV ? process.env.NODE_ENV : 'development' ),

    // entry file(s)
    entry: './src/index.js',

    target:"web",

    // output file(s) and chunks
    output: {
        library: 'Capibara',
        libraryTarget: 'umd',
        globalObject: '(typeof self !== "undefined" ? self : this)',
        libraryExport: 'default',
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js',
        publicPath: config.get('publicPath')
    },

    // module/loaders configuration
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
            },
            //{ test: /\.woff(2)?(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/, use: "url-loader?limit=10000&mimetype=application/font-woff" },
            //{ test: /\.(ttf|eot|svg)(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/, use: "file-loader" },
        ],
    },

    plugins: [
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, 'index.html')
        }),
        new HTMLWebpackPlugin({
            filename:'minimal.html',
            template: path.resolve(__dirname, 'minimal.html')
        }),
        new HTMLWebpackPlugin({
            filename:'cuarteles.html',
            template: path.resolve(__dirname, 'cuarteles.html')
        }),
    ],

    // development server configuration
    devServer: {

        // must be `true` for SPAs
        historyApiFallback: true,

        // open browser on server start
        open: config.get('open')
    },

    // generate source map
    devtool: ( 'production' === process.env.NODE_ENV ? 'source-map' : 'inline-source-map' ),
};
