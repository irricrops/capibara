      config.data = data;
      // Your code for handling the data you get from the API

      var capibara = null;
      document.addEventListener('DOMContentLoaded', function() {
          // create instance of a plugin
          capibara = new Capibara({ config });

          capibara.init();

          capibara.$capibara.addEventListener("headerPrev", function(e) {
              capibara.loadingBtn("[cap-btn-prev]", true);
          });

          capibara.$capibara.addEventListener("save", function(e) {
              /* TEST CODE*/
              capibara.loading("right");

              setTimeout(() => {
                  capibara.loading("right", false);

                  e.detail.selected.forEach(s => {
                      dataDay = s.item.data.find(d => {
                          return d.date == s.date;
                      });
                      if (!dataDay) {
                          dataDay = {
                              date: s.date,
                              values: {},
                              message: { id: 0, }
                          }
                          s.item.data.push(dataDay);
                      }
                      //dataDay.message.value = "test saved";
                      capibara.redraw({
                          floatBox: true,
                      });
                  });

                  capibara.cleanMultiInput();
              }, 3000);

              /* END TEST CODE*/

          });

      });
