var data = [
    {
      "label": "Equipo 1",
      "type": "container",
      "id": 123,
      "values": {},
      "selectable": false,
      "children": [
        {
          "label": "Cuartel demo 1",
          "children": [
            {}
          ],
          "type": "cuartel",
          "id": 356,
          "data": [
            {
              "date": "2021-12-01",
              "errors": [],
              "values": {
                "foliar": {
                  "value": 1
                },
                "insecticida": {
                  "value": 1
                },
                "herbicida": {
                  "value": 1
                },
                "fungicida": {
                  "value": 1
                }
              }
            },
            {
              "date": "2021-12-02",
              "errors": [],
              "values": {
                "foliar": {
                  "value": 1
                },
                "insecticida": {
                  "value": 1
                },
                "fungicida": {
                  "value": 1
                }
              }
            },
            {
              "date": "2021-12-03",
              "errors": [],
              "values": {
                "foliar": {
                  "value": 1
                },
                "insecticida": {
                  "value": 1
                },
              }
            },
            {
              "date": "2021-12-04",
              "errors": [],
              "values": {
                "insecticida": {
                  "value": 1
                },
              }
            },
            {
              "date": "2021-12-07",
              "errors": [],
              "values": {
                "next": {
                  "foliar": {
                    "value": 2
                  },
                  "insecticida": {
                    "value": 2
                  },
                  "herbicida": {
                    "value": 2
                  },
                  "fungicida": {
                    "value": 2
                  }
                }
              }
            }
          ],
          "values": {
            "clon": "asd",
            "plantas": 2,
            "ha": 1,
            freq: 7
          }
        },
      {
        "label": "cuartel demo 2",
        "children": [
          {}
        ],
        "type": "cuartel",
        "id": 570,
        "data": [
          {
            "date": "2021-12-01",
            "errors": [],
            "values": {
              "foliar": {
                "value": 1
              },
              "insecticida": {
                "value": 1
              },
              "herbicida": {
                "value": 1
              },
              "fungicida": {
                "value": 1
              }
            }
          },
        {
          "date": "2021-12-08",
          "errors": [],
          "values": {
            "next": {
              "foliar": {
                "value":""
              },
              "insecticida": {
                "value":""
              },
              "herbicida": {
                "value": 2
              },
              "fungicida": {
                "value": 2
              }
            }
          }
        }
        ],
        "values": {
          "clon": "asd",
          "plantas": 2,
          "ha": 1
        }
      }
      ]
    }
  ];
