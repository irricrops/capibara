var data = [
  {
    id: 1,
    label: "EquiDemo 1 con un nombre super largo 2",
    selectable: false,
    values: {},
    data: [],
    children: [
      {
        id: 1,
        label: "Sector Demo con un nombre super largo de prueba test test",
        clickeable: true,
        light: "red",
        navOver: {
          subtitle: "A demo - Carmenere demo",
          chip: "-74.65%",
          content: "asd asd a d sa ds ad sa ",
        },
        values: {
          ha: 1.2,
          variedad: "Carmenere demo",
          riego_nominal_mm: 20,
          riego_nominal_hh: "10:00",
          reposicion_teorica: 50,
          precip: 2,
          caudal: 24,
          rep_hoy: 18.64,
          et_acum_mm: 55,
          limite_scholander: 10,
        },
        data: [
          {
            date: "2020-12-01",
            values: {
              riego: {
                mm: 6,
                hh: "3:00",
                volumen: 72,
                volumen_hora: 24,
              },
              et_acum: {
                value: 69,
              },
              reposicion_real: {
                value: 11.59,
              },
            },
            message: {},
          },
        ],
      },
    ],
  },
];
