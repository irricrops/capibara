import utils from './utils';
import _ from "lodash";

export default {
    cleanStyle(config) {

        utils.setCSSVariable(config, "--nav-width", null);
        utils.setCSSVariable(config, "--nav-right-width", null);
        utils.setCSSVariable(config, "--nav-item-col-width", null);
        utils.setCSSVariable(config, "--initial-nav-width", null);
        utils.setCSSVariable(config, "--ex-nav-width", null);
        utils.setCSSVariable(config, "--btn-nav-width", null);
        utils.setCSSVariable(config, "--header-height", null);
        utils.setCSSVariable(config, "--header-top-height", null);
        utils.setCSSVariable(config, "--ex-header-height", null);
        utils.setCSSVariable(config, "--day-width", null);
        utils.setCSSVariable(config, "--item-height", null);
    },

    setNavStyle(config) {
        if (config.timeline.nav.left.initial.width && !utils.checkIsMobile()) {
            utils.setCSSVariable(config, "--nav-width", config.timeline.nav.left.initial.width + "px");
        }
        if (config.timeline.nav.left.initial.mobile && utils.checkIsMobile()) {
            utils.setCSSVariable(config, "--nav-width", config.timeline.nav.left.initial.mobile.width + "px");
        }
        if (config.timeline.nav.right.width) {
            utils.setCSSVariable(config, "--nav-right-width", config.timeline.nav.right.width + "px");
        }
        if (config.timeline.nav.columnWidth) {
            utils.setCSSVariable(config, "--nav-item-col-width", config.timeline.nav.columnWidth + "px");
        }
    },
    setNavLeftInitialStyle(config, isShow, tab) {
        this.setSubNavStyle(config, isShow, "left.initial", "--initial-nav-width", tab);
    },
    setNavLeftExtendedStyle(config, isShow, tab) {
        this.setSubNavStyle(config, isShow, "left.extend", "--ex-nav-width", tab);
    },
    setNavLeftButtonsStyle(config, isShow, tab) {
        this.setSubNavStyle(config, isShow, "left.buttons", "--btn-nav-width", tab);
    },

    setSubNavStyle(config, isShow, path, cssVarName, tab = null) {
        let columnWidth = parseFloat(utils.getCSSVariable(config, "--nav-item-col-width"));
        let nav = _.get(config, "timeline.nav." + path);
        let width = null;
        if (nav) {
            if (isShow) {
                if (nav.columnWidth) {
                    columnWidth = nav.columnWidth;
                }
                if (nav.width) {
                    width = nav.width;
                } else if (nav.vars && columnWidth) {
                    let navVarsFiltered = nav.vars.filter(v => (v.onlyTab == undefined || (v.onlyTab && tab && v.onlyTab == tab.key)));
                    width = columnWidth * navVarsFiltered.length;
                }
            } else {
                width = 0;
            }

            if (width !== null) {
                utils.setCSSVariable(config, cssVarName, width + "px");
            }
        }
    },

    setHeaderStyle(config) {
        if (config.timeline.header.height) {
            utils.setCSSVariable(config, "--header-height", config.timeline.header.height + "px");
        }

        if (config.timeline.header.top && config.timeline.header.top.height) {
            utils.setCSSVariable(config, "--header-top-height", config.timeline.header.top.height + "px");
        }

        if (config.timeline.header.extend.enabled === false) {
            utils.setCSSVariable(config, "--ex-header-height", "0px");
        }

        if (config.timeline.header.extend.height) {
            utils.setCSSVariable(config, "--ex-header-height", config.timeline.header.extend.height + "px");
        }

    },

    setTableStyle(config) {
        if (config.timeline.day.width) {
            if (config.timeline.day.mobile && utils.checkIsMobile()) {
                utils.setCSSVariable(config, "--day-width", config.timeline.day.mobile.width + "px");
            } else {
                utils.setCSSVariable(config, "--day-width", config.timeline.day.width + "px");
            }
        }
        if (config.timeline.day.height) {
            if (config.timeline.day.mobile && utils.checkIsMobile()) {
                utils.setCSSVariable(config, "--item-height", config.timeline.day.mobile.height + "px");
            } else {
                utils.setCSSVariable(config, "--item-height", config.timeline.day.height + "px");
            }
        }

    }

}
