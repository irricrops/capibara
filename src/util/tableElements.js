import dom from "../util/domUtils";
import utils from "../util/utils";
import dateUtils from "../util/dateUtils";
import icons from "../util/icons";

export default {
  /**
   * Crea elemento Table
   * Contendrá los elementos que apareceran dentro de la tabla de planilla
   * Se Usa para :
   *    - Agregar background con patron estilo tabla
   *    - Agregar dayMouseOver, div que indica posicion de mouse
   *    - Agregar DetailDay, para detalles de datos en tabla
   *    - Agregar SelectedContainer, para dias seleccionados
   *    - Agregar FloatBoxContainer, para visualizacion de datos en tabla
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom creado
   */
  createAgTable(config) {
    let e = dom.createElement("div", { className: "capibara" });
    if (config.timeline.dark) {
      e.classList.add("dark-theme");
    }
    if (config.timeline.header.extend.isShow) {
      e.classList.add("header-extended");
    }
    return e;
  },

  /**
   * Recrea Background, que es el fondo grid, como tabla
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} $elem - Elemento relacionado, donde se cre el background
   * @return {Object} Elemento Dom Corner Footer
   */
  redrawBackground(config, $elem) {
    return this.createBackground(config, $elem);
  },

  /**
   * Crea y posiciona fondo de tabla
   * Este fondo contiene un patron de CSS que le da la apariencia de tabla
   * por lo cual se debe reposicionar segun la fecha de inicio, para hacer
   * calzar los fines de semana
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} [$elem = null] - Elemento background, si es null, lo crea
   * @return {Object} elemento Dom Background
   */
  createBackground(config, $elem = null) {
    let { height, width, dayWidth } = utils.getTableDimensions(config);

    let start = new Date(config.timeline.start);
    let end = new Date(config.timeline.end);

    end.setTime(end.getTime() + dateUtils.dayInMilliseconds); // add 24 hours

    let dCorrected = new Date(
      start.valueOf() + start.getTimezoneOffset() * 60000
    );
    let weekDay = dCorrected.getDay();
    let diffWeekDays = (weekDay - 1) * dayWidth * -1;

    $elem = $elem || dom.createElement("div", { className: "background-grid" });
    $elem.style["background-position"] =
      diffWeekDays + "px 0, 0 0, " + diffWeekDays + "px 0";
    $elem.style.width = width + "px";
    $elem.style.height = height + 1 + "px";
    return $elem;
  },

  /**
   * Crea Plot Container Vertical
   * Los plots son elementos que agregan secciones de fondo en la planilla
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom plotVerticalContainer
   */
  createPlotVerticalContainer(config) {
    let $elem = dom.createElement("div", {
      className: "plot-vertical-container",
    });
    return this.redrawPlotVerticalContainer(config, $elem);
  },

  createPlotHorizontalContainer(config) {
    let $elem = dom.createElement("div", {
      className: "plot-horizontal-container",
    });
    return this.redrawPlotHorizontalContainer(config, $elem);
  },

  /**
   * Recrea plotVerticalContainer, usando elemento existente
   * usa un elemento existente para usar la funcion para redibujar el elemento
   *
   * Los Plots se insertan como divs, con posicion especificada,
   * con ancho de dia
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} $elem - Elemento Dom donde crea plotVerticalContainer
   * @return {Object} Elemento Dom plotVerticalContainer
   */
  redrawPlotVerticalContainer(config, $elem) {
    $elem.innerHTML = "";
    if (config.timeline.plots && config.timeline.plots.vertical) {
      let { height, dayWidth } = utils.getTableDimensions(config);

      config.timeline.plots.vertical.forEach((p) => {
        let diffDays = Math.floor(
          dateUtils.diffDays(config.timeline.start, p.date)
        );
        let left = diffDays * dayWidth;
        let plot = dom.createElement("div", {
          classNames: ["plot-vertical", p.className || ""],
          style: {
            "margin-left": left + "px",
            height: height + "px",
          },
        });
        $elem.appendChild(plot);
      });
    }
    return $elem;
  },

  redrawPlotHorizontalContainer(config, $elem) {
    $elem.innerHTML = "";
    if (config.timeline.plots && config.timeline.plots.horizontal) {
      let { width, itemHeight } = utils.getTableDimensions(config);
      config.timeline.plots.horizontal.forEach((p) => {
        let item = utils.getItemById(config, p.idNode, p.typeNode);
        let topPosition = utils.getPositionByItem(config, item, "parent");
        let plot = dom.createElement("div", {
          classNames: ["plot-horizontal", p.className || ""],
          style: {
            "margin-top": topPosition * itemHeight + "px",
            width: width + "px",
          },
        });
        $elem.appendChild(plot);
      });
    }
    return $elem;
  },

  /**
   * Crea Corner Izquierdo
   * Se usa para :
   *    - titulos relacionados con los datos del sidebar izquierdo
   *    - titulos para inputs de header, absolute a la derecha
   *    - boton para mostrar/ocultar sidebar izquierdo
   *    - promedios
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom Corner
   */
  createCorner(config, tab = null) {
    let $elem = dom.createElement("div", { className: "corner" });
    return this.redrawCorner($elem, config, tab);
  },

  redrawCorner($elem, config, tab = null) {
    let labelsInitialCorner = config.timeline.nav.left.initial.vars
      .filter(
        (v) =>
          v.onlyTab == undefined || (v.onlyTab && tab && v.onlyTab == tab.key)
      )
      .map((d) => `<span class="item-col">${d.label}</span>`)
      .join("");

    let labelsExtendCorner = config.timeline.nav.left.extend.vars
      .filter(
        (v) =>
          v.onlyTab == undefined || (v.onlyTab && tab && v.onlyTab == tab.key)
      )
      .map((d) => `<span class="item-col">${d.label}</span>`)
      .join("");

    let labelsHeaderExtend = config.timeline.header.extend.data
      .map((d) => {
        if (d.isHidden) return "";

        let avg = "";
        let tooltip = "";
        let className = "";
        let isAccent = d.isAccent;

        if (d.average) {
          avg = ` <span class="average "> ( <span> <span class="symbol">x̄</span> ${d.average} </span>)  </span>   `;

          tooltip = `cap-tooltip="Promedio ${d.label} : ${d.average} ${
            d.suffix || ""
          }"`;
        }

        if (d.color) {
          if (isAccent) {
            className = d.color ? "c-" + d.color + "--background" : "";
          } else {
            className = d.color ? "c-" + d.color + "--text " : "";
          }
        }

        return `<div class="input-title-container ${
          isAccent ? "input-title--accent" : ""
        } ${d.isPersistent ? "persistent" : ""}">
                                  <span ${tooltip} class="input-title ${className}">
                                    ${avg}
                                    ${d.label}
                                  </span>

                                </div>`;
      })
      .join("");

    let toggleSidebarBtnString = `
          <button toggle-sidebar class="cap-btn-icon cap-btn-icon-raised cap-btn-toggle-sidebar ">
          ${icons.chevronRight}
          </button> `;

    if (config.timeline.nav.left.hideToggleSidebarButton) {
      toggleSidebarBtnString = "";
    }

    let template = `<div>

          ${toggleSidebarBtnString}
        <div class="corner-title">
          <span class="bottom-text">
            ${config.timeline.corner.left.label}
          </span>
        </div>

        <div class="initial-corner">
          <div class="initial-corner-title">
            <span class="bottom-container hide">
              ${config.timeline.corner.initial.label}
            </span>

            <div class="bottom-container">
              ${labelsInitialCorner}
            </div>

          </div>
        </div>


        <div class="extend-corner">
          <div class="ex-corner-title">
            <span class="bottom-container hide">
              ${config.timeline.corner.extend.label}
            </span>

            <div class="bottom-container">
              ${labelsExtendCorner}
            </div>

          </div>
        </div>


        <div class="cap-btn-nav-corner">
          <div class="input-titles">
              ${labelsHeaderExtend}
          </div>
        </div>

        <div class="progress-bar progress-bar-left hide-progress"></div>

      </div>`;

    let r = dom.createElementFromHTML(template);

    $elem.innerHTML = "";
    $elem.append(r);

    let toggleSidebar = $elem.querySelector("[toggle-sidebar]");
    if (toggleSidebar) {
      toggleSidebar.addEventListener("click", (e) => {
        r.dispatchEvent(
          new CustomEvent("toggleSidebar", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    }

    return $elem;
  },

  /**
   * Crea Corner Derecho
   * Se usa para :
   *    - titulos relacionados con los datos del sidebar Derecho
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom corner
   */
  createCornerRight(config, tab = null) {
    let $elem = dom.createElement("div", { classNames: ["corner", "right"] });
    return this.redrawCornerRight($elem, config, tab);
  },

  redrawCornerRight($el, config, tab = null) {
    let label = "";

    if (config.timeline.corner.right.label) {
      ` <span>
            ${config.timeline.corner.right.label}
          </span>`;
    }

    let str = `
        <div class="corner-title">
        ${label}
        <div class="btn-container"></div>
        </div>
        <div class="progress-bar progress-bar-right hide-progress"></div>
    `;
    $el.innerHTML = str.trim();
    let $btnContainer = $el.querySelector(".btn-container");

    if (config.timeline.corner.right.buttons) {
      config.timeline.corner.right.buttons.forEach((btn) => {
        let isEnabled = btn.getIsEnabled ? btn.getIsEnabled() : true;
        if (isEnabled) {
          let $btn = dom.createElementFromHTML(`
                        <button class="corner-btn cap-btn-raised">
                          ${btn.label || ""}
                        </button> `);

          if (btn.onClick) {
            $btn.addEventListener("click", btn.onClick);
          }

          $btnContainer.appendChild($btn);
        }
      });
    }
    if (config.timeline.corner.right.inputs) {
      config.timeline.corner.right.inputs.forEach((input) => {
        let $input = this.createCornerRightInput({ inputConfig: input });
        $btnContainer.appendChild($btn);
      });
    }
    return $el;
  },

  createCornerRightInput({ inputConfig, dataDay = null, daySelected = null }) {
    let r = null;
    switch (inputConfig.type) {
      case "autocomplete-crud":
        r = this.createCornerRightInputAutocompleteCrud({
          inputConfig,
          dataDay,
          daySelected,
        });
        break;
    }
    return r;
  },

  createCornerRightInputAutocompleteCrud({
    inputConfig,
    dataDay,
    daySelected,
  }) {
    let elem = "test";
    return elem;
  },

  /**
   * Crea Header Top
   * Se usa para :
   *    - Manejo de tabs
   *    - Botones de navegación y selección de fecha
   *    - Boton toggle Dark mode
   *    - Boton toggle Header extended
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom
   */

  createHeaderTop(config, tabSelected = null) {
    return this.redrawHeaderTop(config, tabSelected, dom.createElement("div"));
  },

  redrawHeaderTop(config, tabSelected = null, $elem) {
    let str = `
      <div class="header-top">`;

    if (config.timeline.tabs) {
      let showTabs = true;

      if (config.timeline.tabs.filter((t) => !t.isHidden).length == 0) {
        showTabs = false;
      }

      if (showTabs) {
        str += ` <div class="tab-select-container c-d-desktop-none c-d-mobile-flex">
                  <select class="tabs-select" >`;

        config.timeline.tabs.forEach((tab) => {
          if (!tab.isHidden) {
            str += ` <option ${
              tab == tabSelected ? 'selected="selected"' : ""
            } value="${tab.id}"> ${tab.label} </option>`;
          }
        });

        str += `</select> </div>`;

        str += ` <div class="tabs-container c-d-desktop-flex c-d-mobile-none">`;

        config.timeline.tabs.forEach((tab) => {
          if (!tab.isHidden) {
            str += ` <button
                        class="tab ${tab.disabled ? "disabled" : ""}  ${
                          tab == tabSelected ? "active" : ""
                        }  ${tab.color}"
                        color="${tab.color}"
                        input-key="${tab.id}"
                        ${
                          tab.tooltip ? 'cap-tooltip="' + tab.tooltip + '"' : ""
                        } >
                    <span>${tab.label}</span>
                    ${tab.icon || ""}
                  </button>
                  `;
          }
        });

        str += `</div>`;
      }
    }

    if (tabSelected) {
      if (tabSelected.legends) {
        str += ` 
        <button toggle-legends class="cap-btn-icon cap-btn-icon-raised ">
          ${icons.toggleLegends}
        </button> 

        <div class="legends-container force-mobile-style hide"> `;

        tabSelected.legends.forEach((legend) => {
          let colorClass = "";
          let colorBg = "";

          if (utils.isHexColor(legend.color)) {
            colorBg = "background:" + legend.color;
          } else {
            colorClass = legend.color ? legend.color + "--background" : "";
          }

          str += ` <div
                      class="legend ${legend.active ? "active" : ""}"
                      input-key="${legend.key}"
                      ${
                        legend.tooltip ? `cap-tooltip="${legend.tooltip}"` : ""
                      } >
                  <div class="icon-legend ${colorClass} ${
                    legend.shape
                  }" style="${colorBg}"></div>
                  <span>${legend.label}</span>
                </div>`;
        });

        str += `</div>`;
      }
    }

    str += `

          <div class="spacer"></div>



          <button cap-btn-prev class="cap-btn-icon cap-btn-icon-raised">
             ${icons.arrowLeft}
             ${icons.loadingIcon} 
          </button>


          <button cap-btn-calendar class="cap-btn-icon cap-btn-icon-raised">
            ${icons.calendar}
            ${icons.loadingIcon}
          </button>

          <button cap-btn-next class="cap-btn-icon cap-btn-icon-raised">
            ${icons.arrowRight}
            ${icons.loadingIcon}
          </button>

          <div class="spacer"></div>

          <button toggle-dark class="cap-btn-icon cap-btn-header c-d-mobile-none ${
            config.timeline.withDarkToggle == false ? "hide" : ""
          }">
            ${icons.dark}
            ${icons.light}
          </button>`;

    if (config.timeline.header.extend.enabled !== false) {
      str += `<button toggle-header class="cap-btn-icon cap-btn-icon-raised">
          ${icons.chevronDown}
          </button>`;
    }

    str += ` <div class="button-right-container"></div>
        </div>`;
    /*
          `
          <button toggle-sidebar-right class="cap-btn-icon cap-btn-icon-raised">
            <span>
              <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z" />
              </svg>
            </span>
          </button>

      </div>
    `;
    */

    let $r = dom.createElementFromHTML(str);
    $elem.replaceWith($r);

    if (config.timeline.header.rightButtons) {
      let $buttonContainer = $r.querySelector(".button-right-container");
      config.timeline.header.rightButtons.forEach((button) => {
        let hasBadge = false;
        if (button.hasBadge) {
          hasBadge = button.hasBadge();
        }
        let str = `<button class="cap-btn-icon cap-btn-icon-raised ${
          hasBadge ? "cap-btn-has-badge" : ""
        }">
            <span>
              <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="currentColor" d="${button.icon}" />
              </svg>
            </span>
          </button>`;
        let $button = dom.createElementFromHTML(str);
        $button.addEventListener("click", (evt) => button.onClick({ evt }));
        $buttonContainer.append($button);
      });
    }

    let $toggleDark = $r.querySelector("[toggle-dark]");
    if ($toggleDark) {
      $toggleDark.addEventListener("click", (e) => {
        $r.dispatchEvent(
          new CustomEvent("toggleDark", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    }

    let $btnCalendar = $r.querySelector("[cap-btn-calendar]");
    if ($btnCalendar) {
      $btnCalendar.addEventListener("click", (e) => {
        let x = e.x;
        let y = e.y;
        $r.dispatchEvent(
          new CustomEvent("clickCalendarIcon", {
            bubbles: true,
            cancelable: false,
            detail: { x, y },
          })
        );
      });
    }

    let $btnToggleHeader = $r.querySelector("[toggle-header]");
    if ($btnToggleHeader) {
      $btnToggleHeader.addEventListener("click", (e) => {
        $r.dispatchEvent(
          new CustomEvent("toggleHeader", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    }

    /*
    $r.querySelector("[toggle-sidebar-right]").addEventListener("click", e => {
      $r.dispatchEvent(new CustomEvent("toggleSidebarRight", {
        bubbles: true,
        cancelable: false,
      }));
    });
    */

    $r.querySelector("[cap-btn-prev]").addEventListener("click", (e) => {
      $r.dispatchEvent(
        new CustomEvent("headerPrev", {
          bubbles: true,
          cancelable: false,
          detail: {
            originalEvt: e,
          },
        })
      );
    });

    $r.querySelector("[cap-btn-calendar]").addEventListener("click", (e) => {
      $r.dispatchEvent(
        new CustomEvent("headerCalendar", {
          bubbles: true,
          cancelable: false,
          detail: {
            originalEvt: e,
          },
        })
      );
    });

    $r.querySelector("[cap-btn-next]").addEventListener("click", (e) => {
      $r.dispatchEvent(
        new CustomEvent("headerNext", {
          bubbles: true,
          cancelable: false,
          detail: {
            originalEvt: e,
          },
        })
      );
    });

    let $toggleLegendsBtn = $r.querySelector("[toggle-legends]");
    if ($toggleLegendsBtn) {
      $toggleLegendsBtn.addEventListener("click", (e) => {
        let $legendContainer = $r.querySelector(".legends-container");
        if ($legendContainer) {
          $legendContainer.classList.toggle("hide");
        }
      });
    }

    $r.querySelectorAll(".legend").forEach(($legend) => {
      $legend.addEventListener("click", (e) => {
        let key = e.currentTarget.getAttribute("input-key");

        if (key) {
          let legendSelected = tabSelected.legends.find(
            (tab) => tab.key === key
          );
          if (legendSelected) {
            $r.dispatchEvent(
              new CustomEvent("selectLegendHeader", {
                bubbles: true,
                cancelable: false,
                detail: {
                  legend: legendSelected,
                  $legend,
                },
              })
            );
          }
        }
      });
    });

    $r.querySelectorAll(".tab:not(.disabled)").forEach(($tab) => {
      $tab.addEventListener("click", (e) => {
        let key = e.currentTarget.getAttribute("input-key");

        if (key) {
          let tabSelected = config.timeline.tabs.find((tab) => tab.id == key);
          if (tabSelected) {
            $r.dispatchEvent(
              new CustomEvent("selectTabHeader", {
                bubbles: true,
                cancelable: false,
                detail: {
                  tab: tabSelected,
                },
              })
            );
          }
        }
      });
    });

    $r.querySelectorAll(".tabs-select:not(.disabled)").forEach(($tab) => {
      $tab.addEventListener("change", (e) => {
        let key = parseFloat(e.currentTarget.value);

        if (key) {
          let tabSelected = config.timeline.tabs.find((tab) => tab.id == key);
          if (tabSelected) {
            $r.dispatchEvent(
              new CustomEvent("selectTabHeader", {
                bubbles: true,
                cancelable: false,
                detail: {
                  tab: tabSelected,
                },
              })
            );
          }
        }
      });
    });

    return $r;
  },

  /**
   * Crea Header
   * Se usa para :
   *    - Mostrar fechas en header
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom
   */
  createHeader(config) {
    let $elem = dom.createElement("header");
    return this.redrawHeader(config, $elem);
  },

  /**
   * Recrea Header, usado para redibujar
   * utiliza $elem como elemento dom donde recrearlo
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} $elem - Elemento Dom donde crea Header
   * @return {Object} Elemento Dom
   */
  redrawHeader(config, $elem) {
    let datesTree = dateUtils.getDateTreeRange(
      new Date(config.timeline.start),
      new Date(config.timeline.end)
    );

    let str = `<div class="header-container"> `;

    Object.keys(datesTree).forEach((keyY) => {
      let year = datesTree[keyY];
      str += `<div class="agno-calendario">
              <span class="agno hide">${keyY}</span>
              <div class="meses">`;

      Object.keys(year).forEach((keyM) => {
        let month = datesTree[keyY][keyM];
        str += ` <div class="mes-calendario">
                    <div class="mes ${
                      Object.keys(month).length < 4
                        ? "mes-reduced mes-reduced-" + Object.keys(month).length
                        : ""
                    }">
                      <div class="mes-label">
                        ${utils.getMonthByPosition(keyM)} ${keyY}
                      </div>
                    </div>
                    <div class="dias">`;
        Object.keys(month).forEach((keyD) => {
          let day = datesTree[keyY][keyM][keyD];
          let isWeekend = day.isWeekend;
          let isToday = day.isToday;
          let hasAlert = config.timeline.header.dateHasAlert
            ? config.timeline.header.dateHasAlert(day)
            : false;
          let date = dateUtils.formatDate(day.date);
          str += `<div class="dia-calendario 
            ${isToday ? "today" : ""}
            ${isWeekend ? "weekend" : ""}
            ${hasAlert ? "has-alert" : ""}
            ${config.timeline.dateSelectable ? "is-clickeable" : ""}
            ${config.timeline.dateWithContextMenu ? "is-clickeable" : ""}
            " date="${date}">${keyD}</div>`;
        });
        str += `</div></div>`;
      });
      str += `</div> </div>`;
    });

    str += `</div>`;
    $elem.innerHTML = "";
    $elem.append(dom.createElementFromHTML(str));

    let $days = $elem.querySelectorAll(".dia-calendario[date]");
    $days.forEach(($dia) => {
      if (config.timeline.dateWithContextMenu) {
        $dia.addEventListener("contextmenu", (evt) => {
          $elem.dispatchEvent(
            new CustomEvent("contextMenuDate", {
              bubbles: true,
              cancelable: false,
              detail: {
                $elem: $dia,
                date: $dia.getAttribute("date"),
                clientX: evt.clientX,
                clientY: evt.clientY,
              },
            })
          );
        });
      }
      if (config.timeline.dateSelectable) {
        $dia.addEventListener("click", (evt) => {
          $elem.dispatchEvent(
            new CustomEvent("clickDate", {
              bubbles: true,
              cancelable: false,
              detail: {
                $elem: $dia,
                date: $dia.getAttribute("date"),
              },
            })
          );
        });
      }
      if (config.timeline.onMouseOverDate) {
        $dia.addEventListener("mouseover", (evt) => {
          config.timeline.onMouseOverDate({
            $dia,
            $elem,
            date: $dia.getAttribute("date"),
            evt,
          });
        });
      }
      if (config.timeline.onMouseOutDate) {
        $dia.addEventListener("mouseout", (evt) => {
          config.timeline.onMouseOutDate({
            $dia,
            $elem,
            date: $dia.getAttribute("date"),
            evt,
          });
        });
      }
    });

    return $elem;
  },

  /**
   * Crea Header extended
   * Se usa para :
   *    -Mostrar inputs y datos para fechas en header
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom headerExtended
   */
  createHeaderExtended(config) {
    if (config.timeline.header.extend.enabled == false) return null;

    let dates = dateUtils.getDateRange(
      new Date(config.timeline.start),
      new Date(config.timeline.end)
    );
    let str = `<div class="header-container-extended">`;

    config.timeline.header.extend.data.forEach((d, d_index) => {
      if (d.isHidden) return "";

      let isPersistent = d.isPersistent;
      str += `<div class="input-list ${isPersistent ? "persistent" : ""}">`;
      str += dates
        .map((date) => {
          let value = "";
          let className = "";
          let isFuture = dateUtils.isFuture(date);
          let isToday = dateUtils.isToday(date);

          if (d.data) {
            let dataFounded = d.data.find((dataInput) => {
              return dateUtils.isSameDate(new Date(dataInput.date), date);
            });
            if (dataFounded) {
              value = dataFounded.value || "";
              className += dataFounded.className + " " || "";
            }
          }

          if (
            d.editable === false ||
            (isFuture && d.editableFuture == false) ||
            (isToday && d.editableToday == false)
          ) {
            return `<div>
                      <span date="${date}" 
                            index="${d_index}" 
                            class=" ${className} ${date.varName || ""} ${
                              d.color ? "c-" + d.color + "--text" : ""
                            }" > 
                            ${value} 
                            </span>
                    </div>`;
          }

          let isDisabled = false;
          if (config.timeline.editable === false) {
            isDisabled = true;
          }

          return `<div class="dia-input">
                  <input 
                  date="${date}" 
                  index="${d_index}" 
                  class="header-input 
                  ${date.varName} ${d.color ? "c-" + d.color + "--text" : ""}" 
                  type="number"
                  ${isDisabled ? "disabled" : ""} 
                  value="${value}">
                </div>`;
        })
        .join("");
      str += `</div>`;
    });

    str += `</div>`;

    let r = dom.createElementFromHTML(str);

    r.querySelectorAll("input.header-input").forEach((input) => {
      input.addEventListener("input", (e) => {
        let date = e.currentTarget.getAttribute("date");
        let index = e.currentTarget.getAttribute("index");
        let value = e.currentTarget.value;
        let typeData = config.timeline.header.extend.data[index];

        //refresh config value
        let data = typeData.data.find((d) =>
          dateUtils.isSameDate(new Date(d.date), new Date(date))
        );
        if (data) {
          data.value = value;
        } else {
          typeData.data.push({
            date: date,
            value: value,
          });
        }

        r.dispatchEvent(
          new CustomEvent("changeInputHeader", {
            bubbles: true,
            cancelable: false,
            detail: {
              date: date,
              typeData: typeData,
              value: value,
            },
          })
        );
      });
    });

    return r;
  },

  /**
   * Crear Sidebar Izquierdo
   *    - Maneja items como arbol de 2 niveles (parent, children)
   *    - El sidebar se divide en navList - InitialNav - extendedNav - BtnNav
   *    - Se agrega itemDetail, para detalles de items
   *
   *
   * TODO: para evitar incrustar las ids en los inputs, se deben crear
   * los nodos uno por uno como elemento DOM, y agregarles los eventos directamente
   * ahora se hace seleccionando todos los inputs con querySelectorAll
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom Sidebar Izquierdo
   */
  createNavLeft(config, tab = null) {
    let $elem = dom.createElement("nav", { className: "left" });
    return this.redrawNavLeft($elem, config, tab);
  },

  /**
   * Recrea Sidebar izquierdo, usado para redibujar
   * Utiliza $elem como elemento dom donde recrearlo
   *
   *
   * @param {Object} $elem - Elemento Dom donde crea el sidebar izquierdo
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom navLeft
   */

  redrawNavLeft($elem, config, tab = null) {
    let navList = "";
    let initialNavList = "";
    let exNavList = "";
    let btnNavList = "";
    let isDisabled = false;
    if (config.timeline.editable === false) {
      isDisabled = true;
    }

    config.data.forEach((d) => {
      if (d.isHidden) return;

      navList += `
       <div class="nav-item">


        <div class="nav-item-container parent">`;

      if (d.children) {
        navList += `<button class="tree-icon ${
          d.isClosed ? "" : "cap-rotate"
        }" item-id="${d.id || -1}" type-node="${d.type || ""}" >
        ${icons.chevronDown}
                       </button>`;
      }

      navList += `
          <div class="item-title truncate full-width ${
            d.clickeable ? "clickeable" : ""
          }
                      ${d.navOver ? "hoverable" : ""} " item-id="${
                        d.id || -1
                      }" type-node="${d.type || ""}" >
            ${d.label}
          </div>
        </div>`;

      if (d.children) {
        navList += `
              <div class="children-items ${
                d.isClosed ? "hide" : ""
              } " child-list="${d.id || -1}">`;

        d.children.forEach((c) => {
          if (c.isHidden) return;

          let lightColor = null;
          let lightClass = null;
          let lightSpan = "";
          let multiButtonsDetailTmpl = "";

          if (c.light) {
            if (utils.isHexColor(c.light)) {
              lightColor = c.light;
            } else {
              lightClass = c.light;
            }
          }

          if (c.light) {
            lightSpan = `<span 
                          class="light ${
                            lightClass ? lightClass + "-light" : ""
                          }" 
                          style="${
                            lightColor ? "--light-color:" + lightColor : ""
                          }">
                        </span>`;
          }

          if (c.hasMultibuttons) {
            let iconFormatter = null;
            if (
              config.timeline.nav.left.initial.multibuttons &&
              config.timeline.nav.left.initial.multibuttons.iconFormatter
            ) {
              iconFormatter =
                config.timeline.nav.left.initial.multibuttons.iconFormatter;
            }
            let btnContent = iconFormatter
              ? iconFormatter({ item: c, config })
              : "";

            multiButtonsDetailTmpl = `<button class="multibuttons-btn" 
              item-id="${c.id || -1}" 
              type-node="${c.type || ""}" 
              ${isDisabled ? "disabled" : ""}>
                ${btnContent} 
              </button>`;
          }

          navList += `
                <div class="child-nav-item">
                  <div class="nav-item-container">
                    `;

          /*
                    <span class="nav-item-icon">
                      <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                          <path fill="currentColor" d="M19,15L13,21L11.58,19.58L15.17,16H4V4H6V14H15.17L11.58,10.42L13,9L19,15Z" />
                      </svg>
                    </span>
                      */

          navList += `
                    ${lightSpan}
                    ${multiButtonsDetailTmpl}
                    <div 
                      class="item-title ${c.clickeable ? "clickeable" : ""} ${
                        c.navOver ? "hoverable" : ""
                      }" 
                      item-id="${c.id || -1}" 
                      type-node="${c.type || ""}">

                      <div class="truncate">
                        ${c.label}
                      </div>
                    </div>
                  </div>
                </div>`;
        });

        navList += `
              </div>`;
      }

      navList += `</div>`;

      //initial
      initialNavList += `<div class="initial-nav-item parent">`;
      if (d.withNav) {
        config.timeline.nav.left.initial.vars.forEach((v) => {
          let value = utils.getVarByPath(d.values, v.varName);

          if (v.round && !isNaN(value)) {
            value = utils.round(value, v.round);
          }
          if (v.input) {
            switch (v.input) {
              case "hour":
                value = value.split(":");
                let hhValue = value[0];
                let mmValue = value[1];
                initialNavList += `
                            <div class="item-col has-input">
                              <div class="hour-input">
                                <input type="number"
                                      input-type="time"
                                      max="23"
                                      min="00"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      value="${hhValue}"
                                      ${isDisabled ? "disabled" : ""}
                                      class="hh">
                                <span>:</span>
                                <input type="number"
                                      input-type="time"
                                      max="59"
                                      min="00"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      value="${mmValue}"
                                      ${isDisabled ? "disabled" : ""}
                                      class="mm">
                              </div>
                            </div>`;
                break;
              case "text":
              case "number":
              default:
                initialNavList += `
                            <div class="item-col has-input">
                              <input  
                                      type="${v.input || "text"}"
                                      input-type="${v.input || "text"}"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      ${isDisabled ? "disabled" : ""}
                                      value="${value}"/>
                            </div>`;
            }
          } else {
            initialNavList += `
                          <div class="item-col">
                            ${value}
                          </div>`;
          }
        });
      }

      initialNavList += `
                  </div>`;

      if (d.children) {
        d.children.forEach((c) => {
          if (c.isHidden) return;
          initialNavList += `<div class="child-initial-nav-item ${
            d.isClosed ? "hide" : ""
          }" child="${d.id || -1}">`;

          if (c.withNav !== false) {
            config.timeline.nav.left.initial.vars.forEach((v) => {
              if (v.onlyTab && tab && v.onlyTab !== tab.key) return;

              let value = utils.getVarByPath(c.values, v.varName);
              if (v.round && !isNaN(value)) {
                value = utils.round(value, v.round);
              }
              if (v.input) {
                switch (v.input) {
                  case "hour":
                    value = value.split(":");
                    let hhValue = value[0];
                    let mmValue = value[1];
                    initialNavList += `
                                  <div class="item-col has-input">
                                    <div class="hour-input">
                                      <input type="number"
                                            input-type="time"
                                            max="23"
                                            min="00"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            value="${hhValue}"
                                            ${isDisabled ? "disabled" : ""}
                                            class="hh">
                                      <span>:</span>
                                      <input type="number"
                                            input-type="time"
                                            max="59"
                                            min="00"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            value="${mmValue}"
                                            ${isDisabled ? "disabled" : ""}
                                            class="mm">
                                    </div>
                                  </div>`;
                    break;
                  case "text":
                  case "number":
                  default:
                    initialNavList += `
                                  <div class="item-col has-input">
                                    <input
                                            type="${v.input || "text"}"
                                            input-type="${v.input || "text"}"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            ${isDisabled ? "disabled" : ""}
                                            value="${value}"/>
                                  </div>`;
                }
              } else {
                initialNavList += `
                                <div class="item-col">
                                  ${value}
                                </div>`;
              }
            });
          }
          initialNavList += `
                      </div>`;
        });
      }

      //Ext
      exNavList += `<div class="ex-nav-item parent">`;
      if (d.withExNav) {
        config.timeline.nav.left.extend.vars.forEach((v) => {
          let value = utils.getVarByPath(d.values, v.varName);
          if (v.round && !isNaN(value)) {
            value = utils.round(value, v.round);
          }
          if (v.input) {
            switch (v.input) {
              case "hour":
                value = value.split(":");
                let hhValue = value[0];
                let mmValue = value[1];
                exNavList += `
                            <div class="item-col has-input">
                              <div class="hour-input">
                                <input type="number"
                                      input-type="time"
                                      max="23"
                                      min="0"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      value="${hhValue}"
                                      ${isDisabled ? "disabled" : ""}
                                      class="hh">
                                <span>:</span>
                                <input type="number"
                                      input-type="time"
                                      max="59"
                                      min="0"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      value="${mmValue}"
                                      ${isDisabled ? "disabled" : ""}
                                      class="mm">
                              </div>
                            </div>`;
                break;
              case "text":
              case "number":
              default:
                exNavList += `
                            <div class="item-col has-input">
                              <input
                                      type="${v.input || "text"}"
                                      input-type="${v.input || "text"}"
                                      input-key="${v.key || ""}"
                                      item-id="${d.id || -1}"
                                      type-node="${d.type || ""}"
                                      ${isDisabled ? "disabled" : ""}
                                      value="${value}"/>
                            </div>`;
            }
          } else {
            exNavList += `
                          <div class="item-col">
                            ${value}
                          </div>`;
          }
        });
      }

      exNavList += `
                  </div>`;

      if (d.children) {
        d.children.forEach((c) => {
          if (c.isHidden) return;
          exNavList += `<div class="child-ex-nav-item ${
            d.isClosed ? "hide" : ""
          }" child="${d.id || -1}">`;

          if (c.withNav !== false) {
            config.timeline.nav.left.extend.vars.forEach((v) => {
              if (v.onlyTab && tab && v.onlyTab !== tab.key) return;

              let value = utils.getVarByPath(c.values, v.varName);
              if (v.round && !isNaN(value)) {
                value = utils.round(value, v.round);
              }

              if (v.input) {
                switch (v.input) {
                  case "hour":
                    value = value ? value.split(":") : [0, 0];
                    let hhValue = value[0];
                    let mmValue = value[1];
                    exNavList += `
                                  <div class="item-col has-input">
                                    <div class="hour-input">
                                      <input type="number"
                                            input-type="time"
                                            max="23"
                                            min="0"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            value="${hhValue}"
                                            ${isDisabled ? "disabled" : ""}
                                            class="hh">
                                      <span>:</span>
                                      <input type="number"
                                            input-type="time"
                                            max="59"
                                            min="0"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            value="${mmValue}"
                                            ${isDisabled ? "disabled" : ""}
                                            class="mm">
                                    </div>
                                  </div>`;
                    break;
                  case "text":
                  case "number":
                  default:
                    exNavList += `
                                  <div class="item-col has-input">
                                    <input  
                                            type="${v.input || "text"}"
                                            input-type="${v.input || "text"}"
                                            input-key="${v.key || ""}"
                                            item-id="${c.id || -1}"
                                            type-node="${c.type || ""}"
                                            ${isDisabled ? "disabled" : ""}
                                            value="${value}"/>
                                  </div>`;
                }
              } else {
                exNavList += `
                                <div class="item-col">
                                  ${value}
                                </div>`;
              }
            });
          }
          exNavList += `
                      </div>`;
        });
      }

      //buttons
      btnNavList += `<div class="cap-btn-nav-item parent">
                        <div class="cap-btn-nav-item-content">`;
      if (
        config.timeline.nav.left.buttons &&
        config.timeline.nav.left.buttons.vars
      ) {
        config.timeline.nav.left.buttons.vars.forEach((btn) => {
          if (d.withBtnNav) {
            let btnContent = "";
            let className = btn.className || "";

            if (btn.icon) {
              btnContent = btn.icon ? btn.icon : "";
            }

            if (btn.iconFormatter) {
              btnContent = btn.iconFormatter ? btn.iconFormatter(d, btn) : "";
            }

            if (btn.shape) {
              className += " cap-btn-" + btn.shape;
              btnContent += `<span> ${
                btn.formatter ? btn.formatter(d, btn) : ""
              } </span> `;
            }
            if (btn.btnClassNameFormatter) {
              let newClassName = btn.btnClassNameFormatter(d, btn);
              if (newClassName) {
                className += " " + newClassName;
              }
            }

            btnNavList += `
                    <button class="cap-btn-shape ${className}"
                            input-key="${btn.key || ""}"
                            item-id="${d.id || -1}"
                            ${isDisabled ? "disabled" : ""}
                            type-node="${d.type || ""}">
                            ${btnContent}
                    </button>`;
          }
        });
      }
      btnNavList += `
                      </div>
                    </div>`;

      if (d.children) {
        d.children.forEach((c) => {
          if (c.isHidden) return;

          //buttons
          btnNavList += `<div class="cap-btn-child-nav-item parent ${
            d.isClosed ? "hide" : ""
          }" child="${d.id || -1}">
                            <div class="cap-btn-nav-item-content">`;

          if (
            config.timeline.nav.left.buttons &&
            config.timeline.nav.left.buttons.vars
          ) {
            config.timeline.nav.left.buttons.vars.forEach((btn) => {
              if (c.withNav !== false) {
                let btnContent = "";
                let className = btn.className || "";

                if (btn.icon) {
                  btnContent = btn.icon ? btn.icon : "";
                }

                if (btn.iconFormatter) {
                  btnContent = btn.iconFormatter
                    ? btn.iconFormatter(c, btn)
                    : "";
                }

                if (btn.shape) {
                  className += " cap-btn-" + btn.shape;
                  btnContent += `<span> ${
                    btn.formatter ? btn.formatter(c, btn) : ""
                  } </span> `;
                }
                if (btn.btnClassNameFormatter) {
                  let newClassName = btn.btnClassNameFormatter(c, btn);
                  if (newClassName) {
                    className += " " + newClassName;
                  }
                }

                btnNavList += `
                      <button class="cap-btn-shape ${className}"
                              input-key="${btn.key || ""}"
                              item-id="${c.id || -1}"
                              ${isDisabled ? "disabled" : ""}
                              type-node="${c.type || ""}">
                              ${btnContent}
                      </button>`;
              }
            });
          }

          btnNavList += `
                          </div>
                        </div>`;
        });
      }
    });

    let itemDetail = ` <div class="detail-nav-item hide">
                          ${this.getDetailNavItemContent()}
                        </div>`;

    let str = `<div class="nav-container">
                      ${itemDetail}
                    <div class="nav-item-list">
                      ${navList}
                    </div>
                    <div class="initial-nav-list">
                      ${initialNavList}
                    </div>
                    <div class="extend-nav-list">
                      ${exNavList}
                    </div>
                    <div class="cap-btn-nav-list">
                      ${btnNavList}
                    </div>
                  </div>`;

    let navContainer = dom.createElementFromHTML(str);
    $elem.innerHTML = "";
    $elem.append(navContainer);

    //detalle de nav-item
    $elem.querySelectorAll(".item-title.hoverable").forEach((i) => {
      i.addEventListener("mouseover", (e) => {
        let pos = utils.eventToPositionNavLeft(config, e, $elem);
        let itemId = i.getAttribute("item-id");
        let item = utils.getItemById(config, itemId);
        let typeNode = i.getAttribute("type-node");
        i.dispatchEvent(
          new CustomEvent("overNavItem", {
            bubbles: true,
            cancelable: false,
            detail: {
              originalEvt: e,
              item: item,
              type: typeNode,
              pos: pos,
            },
          })
        );
      });
    });

    //oculta detalle de nav-item, al salir del nav-item-list
    $elem.querySelectorAll(".nav-item-list").forEach((i) => {
      i.addEventListener("mouseleave", (e) => {
        i.dispatchEvent(
          new CustomEvent("leaveNavItem", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    });

    //oculta detalle de nav-item-list, al salir de nav-item
    $elem.querySelectorAll(".item-title:not(.hoverable)").forEach((i) => {
      i.addEventListener("mousemove", (e) => {
        i.dispatchEvent(
          new CustomEvent("leaveNavItem", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    });

    $elem.querySelectorAll(".item-title.clickeable").forEach((i) => {
      i.addEventListener("click", (e) => {
        let itemId = i.getAttribute("item-id");
        let typeNode = i.getAttribute("type-node");
        let value = i.value;
        i.dispatchEvent(
          new CustomEvent("clickNavLeftItem", {
            bubbles: true,
            cancelable: false,
            detail: {
              itemId: itemId,
              type: typeNode,
              value: value,
            },
          })
        );
      });
    });

    $elem.querySelectorAll(".multibuttons-btn").forEach((i) => {
      i.addEventListener("click", (e) => {
        let pos = utils.eventToPositionNavLeft(config, e, $elem);
        let itemId = i.getAttribute("item-id");
        let typeNode = i.getAttribute("type-node");
        i.dispatchEvent(
          new CustomEvent("clickMultiButtonsBtn", {
            bubbles: true,
            cancelable: false,
            detail: {
              itemId: itemId,
              type: typeNode,
              pos,
              button: i,
              event: e,
            },
          })
        );
      });
    });

    $elem.querySelectorAll("input").forEach((i) => {
      i.addEventListener("change", (e) => {
        let inputType = i.getAttribute("input-type");
        let key = i.getAttribute("input-key");
        let itemId = i.getAttribute("item-id");
        let typeNode = i.getAttribute("type-node");
        let item = utils.getItemById(config, itemId);
        let value = i.value;
        if (inputType == "time") {
          let hh_input = i.parentElement.querySelector("input.hh").value;
          let mm_input = i.parentElement.querySelector("input.mm").value;
          mm_input = mm_input < 10 ? "0" + parseFloat(mm_input) : mm_input;
          i.parentElement.querySelector("input.mm").value = mm_input;
          value = hh_input + ":" + mm_input;
        }
        i.dispatchEvent(
          new CustomEvent("saveNavLeftInput", {
            bubbles: true,
            cancelable: false,
            detail: {
              key: key,
              itemId: itemId,
              item: item,
              type: typeNode,
              value: value,
              tabSelected: tab,
            },
          })
        );
      });
    });

    $elem.querySelectorAll("button.cap-btn-shape").forEach((i) => {
      i.addEventListener("click", (e) => {
        let key = i.getAttribute("input-key");
        let itemId = i.getAttribute("item-id");
        let typeNode = i.getAttribute("type-node");
        i.dispatchEvent(
          new CustomEvent("clickNavLeftButton", {
            bubbles: true,
            cancelable: false,
            detail: {
              key: key,
              itemId: itemId,
              type: typeNode,
            },
          })
        );
      });
    });

    $elem.querySelectorAll("button.tree-icon").forEach((i) => {
      i.addEventListener("click", (e) => {
        let itemId = i.getAttribute("item-id");
        let typeNode = i.getAttribute("type-node");
        let dataConfig = config.data.find((d) => d.id == itemId);

        let $navItem = e.currentTarget.closest(".nav-item");
        let $children = $navItem.querySelector(".children-items");
        let itemsToHide = e.currentTarget
          .closest("nav.left")
          .querySelectorAll(
            "[child='" + itemId + "'], [child-list='" + itemId + "']"
          );

        if ($children) {
          if ($children.classList.contains("hide")) {
            e.currentTarget.classList.add("cap-rotate");
            itemsToHide.forEach((i) => i.classList.remove("hide"));
            dataConfig.isClosed = false;
          } else {
            e.currentTarget.classList.remove("cap-rotate");
            itemsToHide.forEach((i) => i.classList.add("hide"));
            dataConfig.isClosed = true;
          }
        }

        i.dispatchEvent(
          new CustomEvent("clickNavLeftTreeIcon", {
            bubbles: true,
            cancelable: false,
            detail: {
              itemId: itemId,
              type: typeNode,
            },
          })
        );

        //e.currentTarget.parent
      });
    });

    return $elem;
  },

  /**
   * Crea Html como String, para el detalle de items en el Sidebar Izquierdo
   *
   * @param {Object} [item={}] - Configuracion de item
   * @return {String} Html de ventana de detalle
   */
  getDetailNavItemContent(item = {}) {
    if (item.navOver == undefined) item.navOver = {};

    return `
                    <div class="header-detail">
                      <div class="detail-title truncate">
                        ${item.label || ""}
                      </div>
                      <div class="detail-subtitle">
                        ${item.navOver.subtitle || ""}
                      </div>
                      <div class="header-chip ${
                        item.light ? item.light + "-light" : ""
                      }  ${item.navOver.chip ? "" : "hide"}">
                        ${item.navOver.chip || ""}
                      </div>
                    </div>

                    <div class="detail-container">
                      ${item.navOver.content || ""}
                    </div>`;
  },

  /**
   * Crea Sidebar Derecho
   * Usado principalmente para formulario de ingreso de datos para los dias
   * seleccionados
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} tab - Tab de referencia, para la creacion del formulario
   * @return {Object} {element, inputs}, element es el elemento Dom del sidebar,
   *                  inputs es un array de elementos Dom de cada input creado
   *                  (no son elementos html de tipo input)
   */
  createNavRight(config, tab = null) {
    var $elem = dom.createElement("nav", { className: "right" });
    return this.redrawNavRight(config, $elem, tab, null, [], null);
  },

  /**
   * Recrea Sidebar Derecho
   * Utiliza $elem como elemento dom donde recrearlo
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} $elem - Elemento Dom donde crea el sidebar derecho
   * @param {Object} tab - Tab de referencia, para la creacion del formulario
   * @param {Object} dataDay - datos del dia seleccionando, para autorrelleno de formulario
   * @param {Object} daySelected - datos del dia seleccionado
   * @param {String} daySelected.date - fecha del dia seleccionado
   * @param {Object} daySelected.item - Item de Sidebar relacionado al dia seleccionado
   * @param {Object} daySelected.pos - posicion de dia seleccionado
   * @return {Object} Elemento Dom Sidebar Derecho
   */
  redrawNavRight(
    config,
    $elem,
    selected = null,
    tab = null,
    dataDay = null,
    daySelected = null
  ) {
    let $navRight = $elem; //dom.createElement("nav",{className:"right"});
    let $divContainer = dom.createElement("div", {
      className: "nav-input-container",
    });
    let $inputs = [];
    let checkboxInputs = [];
    let inputsWithReload = [];
    let tabSelected = tab;
    selected = selected || [daySelected];

    if (tabSelected && tabSelected.inputs) {
      tabSelected.inputs.forEach((i) => {
        if (i.disabled) return;
        if (
          i.showInputIf &&
          !i.showInputIf(dataDay, daySelected, selected, tabSelected)
        ) {
          return;
        }

        let $input = this.createInput(
          i,
          dataDay,
          daySelected,
          selected,
          tabSelected
        );
        if ($input) {
          $divContainer.appendChild($input);
          $inputs.push($input);

          if (i.type == "checkbox") {
            checkboxInputs.push($input);
          }

          if (i.reloadOnSelect != null) {
            inputsWithReload.push({ $input, config: i });
          }
        }
      });
    }

    //manejo de eventos para inputs con showIf
    if (checkboxInputs) {
      checkboxInputs.forEach(($c) => {
        //obtiene key de checkbox
        let key = $c.getAttribute("input-key");
        let props = tabSelected.inputs.find((i) => {
          return i.key == key;
        });

        let isChecked = false;
        if (dataDay) {
          isChecked = utils.getVarByPath(dataDay, props.varName);
        }

        //obtiene inputs relacionados desde config para mostrar/ocultar
        let inputsToHide = tabSelected.inputs.filter((i) => {
          return i.showIf && i.showIf == key;
        });
        let inputsToHideKeys = inputsToHide.map((i) => i.key);

        //obtiene inputs del dom, filtrando con inputsToHide
        let $inputsHidden = $inputs.filter(($i) => {
          let inputKey = $i.getAttribute("input-key");
          return inputsToHideKeys.indexOf(inputKey) > -1;
        });

        //si hay inputs,
        if ($inputsHidden) {
          if (!isChecked) {
            $inputsHidden.forEach(($i) => {
              $i.classList.add("hide");
            });
          }
          $c.addEventListener("input", (e) => {
            $inputsHidden.forEach(($i) => {
              let fn = $c.querySelector("input[type='checkbox']").checked
                ? "remove"
                : "add";
              $i.classList[fn]("hide");
            });
          });
        }
      });
    }

    let saveCancelBtns = this.createNavRightbuttons(config);
    $divContainer.appendChild(saveCancelBtns);
    $navRight.innerHTML = "";
    $navRight.appendChild($divContainer);

    return {
      element: $navRight,
      inputs: $inputs,
      inputsWithReload,
      saveCancelBtns,
    };
  },

  /**
   * Crea Corner inferior izquierdo, como titulo para footer
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom Corner Footer
   */
  createCornerFooter(config) {
    if (
      config.timeline.footer === undefined ||
      config.timeline.footer.enabled == false
    )
      return null;
    let str = "";

    if (config.timeline.footer) {
      str += `<div class="corner-footer">
          <div class="corner-title">
            <span>
              ${config.timeline.footer.label}
            </span>
          </div>
        </div>`;
    }
    return dom.createElementFromHTML(str);
  },

  /**
   * Crea Footer para planilla
   * Inicialmente creado para totales
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom Corner Footer
   */
  createFooter(config) {
    if (
      config.timeline.footer === undefined ||
      config.timeline.footer.enabled == false
    )
      return null;

    let str = `
        <footer>
          <div class="footer-container">
          `;
    config.timeline.footer.data.forEach((d) => {
      str += `
              <div class="footer-item">${d.value}</div>
            `;
    });
    str += `
          </div>
        </footer>
        `;

    return dom.createElementFromHTML(str);
  },

  /**
   * Crea Footer para planilla
   * Inicialmente creado para totales
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object[]} selected - array de objetos, de dias seleccionados
   * @return {Object} Elemento Dom Corner Footer
   */
  createSelectBoard(config, selected) {
    let $elem = dom.createElement("div", { className: "select-container" });
    return this.redrawSelectBoard($elem, selected);
  },

  /**
   * Crea linea vertical para fecha Actual
   *
   * @param {Object} config - Configuracion capibara
   * @return {Object} Elemento Dom Corner Footer
   */
  createTodayLine(config) {
    return this.redrawTodayLine(
      config,
      dom.createElement("div", { className: "today-line" })
    );
  },

  /**
   * Recrea linea vertical para fecha Actual
   * si el elemento $elem no es enviado, se crea un nuevo elemento
   *
   * Si el dia actual no esta dentro del rango de fechas del config,
   * deshabilita la linea con la clase css .disable-line
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} [$elem=null] - Elemento Dom donde recrea la linea, si es null, la crea
   * @return {Object} Elemento Dom Corner Footer
   */
  redrawTodayLine(config, $elem = null) {
    let endTime = new Date(config.timeline.end).getTime();
    let todayTime = new Date().getTime();
    let { height, dayWidth } = utils.getTableDimensions(config);

    if ($elem == null)
      $elem = dom.createElement("div", { className: "today-line" });

    if (endTime < todayTime) {
      $elem.classList.add("disable-line");
    } else {
      $elem.classList.remove("disable-line");
    }

    let diffDays = Math.floor(
      dateUtils.diffDays(
        config.timeline.start,
        dateUtils.formatDate(new Date())
      )
    );
    let diff = diffDays * dayWidth;
    diff += dayWidth / 2 - 2; // para centrar linea
    Object.assign($elem.style, {
      "margin-left": diff + "px",
      height: height + "px",
    });
    return $elem;
  },

  /**
   * Crea elemento FloatBox, que contiene los datos que se muestra en la tabla
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} tab - Tab de referencia, para mostrar datos configurados por tab
   * @return {Object} Elemento Dom Corner Footer
   */
  createFloatBox(config, tab) {
    let $container = dom.createElement("div", {
      className: "float-box-container",
    });
    return this.redrawFloatBox(config, tab, $container);
  },

  /**
   * Recrea elemento FloatBox, que contiene los datos que se muestra en la tabla
   * Se usa principalmente para mostrar datos en la tabla de la planilla
   * los muestra de manera flotante, calculando el Top y el Left segun la poscion
   *
   * Se consideran los elementos padres ocultos, para ocultar los datos
   *
   * Se insertan los elementos FloatBox, como elemento con datos de un item y dia especifico
   *
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} tab - Tab de referencia, para mostrar datos configurados por tab
   * @return {Object} Elemento Dom Corner Footer
   */
  redrawFloatBox(config, tab, $container) {
    $container.innerHTML = "";
    //let tabSelected = tab || null;

    config.data.forEach((de) => {
      if (de.isHidden) return;
      de.children.forEach((ds) => {
        if (ds.isHidden) return;

        ds.data.forEach((data) => {
          if (
            (!data.message || data.message.value == null) &&
            (data.values == null ||
              data.values == undefined ||
              Object.entries(data.values).length === 0)
          ) {
            return null;
          }

          //children Data
          let position = this.getPositionFloatBox(config, data, ds);

          let className = "";
          let hasMessage = false;

          if (data.errors && data.errors.length > 0) {
            className += "has-errors ";
            if (data.errors[0]) {
              className += data.errors[0].classNameIcon + " ";
            }
          }

          if (de.isClosed) {
            className += "hide ";
          }

          let msg = null;
          if (tab.floatBox && tab.floatBox.messagePath) {
            msg = utils.getVarByPath(data, tab.floatBox.messagePath);
          }

          if (msg || (data.message && data.message.value)) {
            className += "has-message ";
            hasMessage = true;
          }

          let strMsg = "";
          if (hasMessage) {
            let viewBox = "0 0 24 24";
            //email outline icon
            let icon =
              "M22 6C22 4.9 21.1 4 20 4H4C2.9 4 2 4.9 2 6V18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6M20 6L12 11L4 6H20M20 18H4V8L12 13L20 8V18Z";

            strMsg = `
              <div class="icon-message">
                <svg viewBox="${viewBox}">
                  <path d="${icon}" />
                </svg>
              </div>
            `;
          }

          let str = `
                <div class="float-box ${className} "
                     style="${position ? position : ""}"
                     item-id="${ds.id || -1}"
                     type-node="${ds.type || ""}">

                     ${strMsg}
                  <div class="float-box-content">`;

          let strExt = `
                <div class="float-box-ext ${de.isClosed ? "hide" : ""}"
                      style="${position ? position : ""}"
                      item-id="${ds.id || -1}-ext"
                      type-node="${ds.type || ""}">
                  <div class="long-bar-container">`;

          if (tab && tab.floatBox) {
            if (tab.floatBox.plots) {
              str += `<div class="plot-floatbox-container">`;
              tab.floatBox.plots.forEach((n) => {
                let cssVars = "";
                let className = "";
                if (utils.getVarByPath(data.values, n.varName) !== null) {
                  if (n.color) {
                    cssVars = `--local-color:${n.color};`;
                  }

                  className = n.className || "";

                  str += `<div class="plot-floatbox ${className} ${
                    n.style || ""
                  }" style="${cssVars} "></div>`;
                }
              });
              str += `
                        </div>`;
            }

            if (tab.floatBox.icons) {
              tab.floatBox.icons.forEach((iconConfig) => {
                let className = "";
                let colorClass = "";
                let colorStyle = "";
                let viewBox = "0 0 24 24";
                let icon = iconConfig.icon || null;

                if (iconConfig.varName) {
                  let iconVar = utils.getVarByPath(
                    data.values,
                    iconConfig.varName
                  );
                  if (iconVar) {
                    icon = iconVar;
                  }
                }

                if (iconConfig.className) {
                  className = iconConfig.className;
                }

                if (iconConfig.color) {
                  if (utils.isHexColor(iconConfig.color)) {
                    colorStyle = `--local-color: ${iconConfig.color};`;
                  } else {
                    colorClass = iconConfig.color;
                  }
                }

                if (iconConfig.colorVarName) {
                  let colorVar = utils.getVarByPath(
                    data.values,
                    iconConfig.colorVarName
                  );
                  if (colorVar) {
                    if (utils.isHexColor(colorVar)) {
                      colorStyle = `--local-color: ${colorVar};`;
                    } else {
                      colorClass = colorVar;
                    }
                  }
                }

                str += `
                <div class="icon-floatbox ${className} ${
                  colorClass ? "has-color" : ""
                }" style="${colorStyle}">
                  <svg viewBox="${viewBox}">
                    <path d="${icon}" />
                  </svg>
                </div>
                `;
              });
            }

            //numeros en el floatBox
            if (tab.floatBox.nums) {
              tab.floatBox.nums.forEach((n) => {
                if (tab.legends && n.tabKey) {
                  let legend = tab.legends.find((l) => l.key == n.tabKey);
                  if (legend && !legend.active) return;
                }

                let dataFloatBox = null;
                let className = "";
                let showIf = true;
                let hideIf = false;
                let colorClass = "";
                let colorStyle = "";
                let classNamesExt = "";
                let colorStyleExt = "";
                let cssVars = "";

                if (n.varName) {
                  dataFloatBox = utils.getVarByPath(data.values, n.varName);
                }

                if (n.varShow) {
                  showIf = Boolean(utils.getVarByPath(data.values, n.varShow));
                  dataFloatBox = dataFloatBox || "";
                }

                if (n.varHide) {
                  hideIf = Boolean(utils.getVarByPath(data.values, n.varHide));
                }

                if (n.shape) {
                  className += "shape-" + n.shape;
                }

                if (n.color) {
                  if (utils.isHexColor(n.color)) {
                    colorStyle = `--local-color: ${n.color};`;
                  } else {
                    colorClass = n.color;
                  }
                }

                if (n.longBar) {
                  let longBarColor = "";
                  let longBarOpacity = "";

                  if (n.longBarColor) {
                    longBarColor = utils.getVarByPath(
                      data.values,
                      n.longBarColor
                    );
                  }

                  if (longBarColor) {
                    if (utils.isHexColor(longBarColor)) {
                      colorStyleExt = `--local-color: ${longBarColor};`;
                    } else {
                      classNamesExt += longBarColor + "-light ";
                    }
                  } else if (colorClass && colorClass != "") {
                    classNamesExt += colorClass;
                  } else if (colorStyle && colorStyle != "") {
                    colorStyleExt = colorStyle;
                  }

                  if (n.longBarOpacity) {
                    longBarOpacity =
                      utils.getVarByPath(data.values, n.longBarOpacity) || null;
                    if (longBarOpacity) {
                      colorStyleExt +=
                        "--local-opacity: " + longBarOpacity + ";";
                    }
                  }

                  if (n.longBarClassName) {
                    classNamesExt +=
                      utils.getVarByPath(data.values, n.longBarClassName) || "";
                  }

                  let longBar = utils.getVarByPath(data.values, n.longBar);
                  if (longBar) {
                    cssVars = `--long:${longBar};`;
                  }
                }

                if (dataFloatBox !== null && showIf && !hideIf) {
                  str += `<span class="data ${colorClass} ${className}" style="${colorStyle}">
                                ${
                                  typeof dataFloatBox == "string"
                                    ? dataFloatBox
                                    : ""
                                }
                                ${
                                  typeof dataFloatBox == "number"
                                    ? utils.round(dataFloatBox, n.round)
                                    : ""
                                }
                              </span>`;

                  if (n.longBar && cssVars) {
                    strExt += `<div class="long-bar ${classNamesExt}" style="${colorStyleExt} ${cssVars} "></div> `;
                  }
                }
              });
            }

            //puntos en el floatBox
            if (tab.floatBox.dots) {
              str += `<div class="dot-container-middle">`;
              tab.floatBox.dots.forEach((dot) => {
                if (utils.getVarByPath(data.values, dot.varName) !== null) {
                  let className = "";
                  let colorClass = "";
                  let colorStyle = "";

                  if (dot.shape) {
                    className += dot.shape;
                  }

                  if (dot.color) {
                    if (utils.isHexColor(dot.color)) {
                      colorStyle = `--local-color: ${dot.color}`;
                    } else {
                      colorClass = dot.color;
                    }
                  }

                  str += `
                       <div class="dot ${colorClass} ${className}" style="${colorStyle}" ></div>`;
                }
              });
              str += `
                        </div>`;
            }

            //fila de puntos en floatBox
            if (tab.floatBox.rowDots) {
              tab.floatBox.rowDots.forEach((row) => {
                let freeColumns = 4;
                str += `<span class="rowdot--row ">`;

                row.forEach((dot) => {
                  if (tab.legends && dot.tabKey) {
                    let legend = tab.legends.find((l) => l.key == dot.tabKey);
                    if (legend && !legend.active) return;
                  }

                  let dataFloatBox = null;
                  let className = "";
                  let showIf = true;
                  let hideIf = false;
                  let colorClass = "";
                  let colorStyle = "";

                  if (dot.varName) {
                    dataFloatBox = utils.getVarByPath(data.values, dot.varName);
                    dataFloatBox = parseInt(dataFloatBox);
                  }

                  if (dot.varShow) {
                    showIf = Boolean(
                      utils.getVarByPath(data.values, dot.varShow)
                    );
                    dataFloatBox = dataFloatBox || "";
                  }

                  if (dot.varHide) {
                    hideIf = Boolean(
                      utils.getVarByPath(data.values, dot.varHide)
                    );
                  }

                  if (dot.shape) {
                    className += " shape-" + dot.shape;
                  }

                  if (dot.color) {
                    if (utils.isHexColor(dot.color)) {
                      colorStyle = `--local-color: ${dot.color}`;
                    } else {
                      colorClass = dot.color;
                    }
                  }

                  if (
                    dataFloatBox !== null &&
                    dataFloatBox > 0 &&
                    showIf &&
                    !hideIf &&
                    freeColumns > 0
                  ) {
                    let n =
                      dataFloatBox > freeColumns ? freeColumns : dataFloatBox;
                    freeColumns -= n;
                    for (let index = 0; index < n; index++) {
                      str += `<div class="rowdot--dot ${colorClass} ${className}" style="${colorStyle}"></div>`;
                    }
                  }
                });

                str += `</span>`;
              });
            }
          }

          str += `
                </div>
                </div>`;
          strExt += `
              </div>
                </div>`;

          let $elem = dom.createElementFromHTML(str);

          let $elemExt = dom.createElementFromHTML(strExt);

          $elem.addEventListener("mouseover", (e) => {
            $elem.dispatchEvent(
              new CustomEvent("mouseOverFloatBox", {
                bubbles: true,
                cancelable: false,
                detail: {
                  item: ds,
                  data: data,
                  originalEvt: e,
                },
              })
            );
          });

          $elem.addEventListener("click", (e) => {
            $elem.dispatchEvent(
              new CustomEvent("clickFloatBox", {
                bubbles: true,
                cancelable: false,
                detail: {
                  item: ds,
                  data: data,
                  originalEvt: e,
                },
              })
            );
          });

          $container.appendChild($elem);
          $container.appendChild($elemExt);
        });
      });
    });

    return $container;
  },

  /**
   * Crea DetailDay, detalle de valores de item y Dia
   * se usa para:
   *    - Muestra nombre de item y detalles
   *    - Muestra capsulas de datos
   *    - Muestra Pills, pildoras de datos resumidos
   *    - Muestra alertas
   *    - Muestra Mensaje
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} item - Item relacionado
   * @param {Object} dataDay - valores de item y Dia
   * @param {Boolean} [onlyStringContent = false] - para escoger si devuelve String o DomElement
   * @return {String|Object} Elemento como String o DomElement
   */
  createDetailDay(
    config,
    item = {},
    dataDay,
    tabSelected,
    onlyStringContent = false
  ) {
    let str = `
          <div class="header-detail">
            <div class="title truncate">
              ${item.label || ""}
            </div>
            <div class="subtitle">
              ${dataDay ? dateUtils.formatDateReadeable(dataDay.date) : ""}
            </div>
          </div>

          <div class="list-detail list">`;

    //CAPSULES
    if (dataDay) {
      config.timeline.detailDay.capsules.forEach((cap) => {
        //para comprobar si tiene datos, se obtiene el primer valor
        //de la lista de values de una capsula
        let hasValues =
          cap.values.filter((v) => {
            let value = utils.getVarByPath(dataDay.values, v.varName);
            return (
              value !== null && !(Array.isArray(value) && value.length === 0)
            );
          }).length > 0;

        let hideIf = false;

        if (cap.hideIf) {
          hideIf = Boolean(utils.getVarByPath(dataDay.values, cap.hideIf));
        }

        let colorClass = "";
        let colorStyle = "";

        if (cap.color) {
          if (utils.isHexColor(cap.color)) {
            colorStyle = `--local-color: ${cap.color};`;
            colorStyle += `--local-color-font: ${cap.color};`;
          } else {
            colorClass = cap.color;
          }
        }
        if (hasValues && !hideIf) {
          str += `
              <div class="item-detail ${colorClass}" style="${colorStyle}">

                <div class="left-icon ${cap.shape || ""} "></div>

                <div class="name">
                  <span>
                    ${cap.title || ""}
                  </span>
                </div>`;

          str += `
          <div class="item-description"> `;

          cap.values.forEach((v) => {
            let value = v.varName
              ? utils.getVarByPath(dataDay.values, v.varName)
              : null;

            if (cap.isLabeledArray) {
              value.forEach((valueLabeled) => {
                let outputValue = null;
                let valueName = v.valueName ? v.valueName : "value";

                let tempValue = utils.getVarByPath(valueLabeled, valueName);

                if (tempValue) {
                  outputValue = v.round
                    ? utils.round(tempValue, v.round)
                    : tempValue;
                }

                if (v.formatter) {
                  outputValue = v.formatter(valueLabeled);
                }

                if (typeof outputValue == "number") {
                  outputValue = utils.formatNumber(outputValue);
                }

                str +=
                  outputValue == null
                    ? ""
                    : `
                      <div class="desc ${v.showColumn ? "desc-column" : ""}">
                      
                        <div class="label">
                          ${v.hideLabel ? "" : valueLabeled.label}
                        </div >
                        <div class="val">` +
                      (v.hidePreffix !== true
                        ? `
                          <span class="prefix">
                            ${v.prefix || ""}
                          </span>`
                        : "") +
                      `<span class="value">
                            ${outputValue}
                          </span>` +
                      (v.hideSuffix !== true
                        ? `
                          <span class="suffix">
                            ${valueLabeled.suffix || v.suffix || ""}
                          </span>`
                        : "") +
                      `
                        </div>
                      </div>`;
              });
            } else {
              let outputValue = v.round ? utils.round(value, v.round) : value;

              if (v.formatter) {
                outputValue = v.formatter({ options: v, dataDay, outputValue });
              }

              if (typeof outputValue == "number") {
                outputValue = utils.formatNumber(outputValue);
              }

              if (outputValue) {
                str += `
                    <div class="desc">
                          <span class="prefix">
                            ${v.prefix || ""}
                          </span>
                          <span class="value">
                            ${outputValue}
                          </span>
                          <span class="suffix">
                            ${v.suffix || ""}
                          </span>
                    </div>`;
              }
            }
          });

          str += `
                </div>`;

          str += `
              </div>`; // end .item-detail
        }
      });
    }

    str += `
        </div>`; // list-detail

    //PILLS
    if (
      config.timeline.detailDay.pillsVarName &&
      dataDay &&
      dataDay.values[config.timeline.detailDay.pillsVarName]
    ) {
      str += `
          <div class="elements-detail">
            <div class="name"> ${config.timeline.detailDay.pillsTitle} </div>
            <div class="elements-list">`;

      dataDay.values[config.timeline.detailDay.pillsVarName].forEach((e) => {
        let value = e.value;

        if (typeof outputValue == "number") {
          value = utils.formatNumber(value);
        }

        str += `
            <div class="element-detail">
              <b>${e.label}</b>
              ${value}
            </div>`;
      });

      str += `
            </div>
            </div>`; //end .element-detail > .element-list
    }

    //str += `</div>`;

    if (dataDay && dataDay.errors && dataDay.errors.length > 0) {
      dataDay.errors.forEach((err) => {
        str += `
          <div class="alert">
            ${icons.alert}
            ${err.label}
          </div>`;
      });
    }

    let msg = null;
    let author = null;
    let dateMsg = null;
    if (dataDay && tabSelected.floatBox && tabSelected.floatBox.messagePath) {
      msg = utils.getVarByPath(dataDay, tabSelected.floatBox.messagePath);
    }

    if (!msg && dataDay && dataDay.message && dataDay.message.value) {
      msg = dataDay.message.value;
      author = dataDay.message.author;
      dateMsg = dateUtils.formatDateReadeable(new Date(dataDay.message.date));
    }

    if (msg) {
      //msg = msg.replace(/(\r\n|\r|\n)/g, '<br>');
      str += `
          <div class="msj-container">
            <div class="msj">
            <pre>${msg || ""}</pre>
            </div>
            <div class="msj-detail">
              <span class="email"> ${author || ""} </span>
              <span class="date"> ${dateMsg || ""} </span>
            </div>
        </div>`;
    }

    let strContainer = `<div class="detail-day dd-hide"> ${str} </div>`;

    if (onlyStringContent) return str;

    return dom.createElementFromHTML(strContainer);
  },

  /**
   * Recrea HTML de detailDay
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} item - Item relacionado
   * @param {Object} dataDay - valores de item y Dia
   * @return {String} Elemento como String, de detailDay
   */
  redrawDetailDay(config, item, dataDay, tabSelected) {
    return this.createDetailDay(config, item, dataDay, tabSelected, true);
  },

  /**
   * Reposiciona FloatBox
   * verifica si el parent esta desplegado, para ocultar los datos
   *
   * Solo actualiza las alturas de los floatbox
   * no recrea los floatbox, es mas optimo
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} item - Item relacionado
   */
  repositionFloatBox(config, $floatBoxContainer) {
    $floatBoxContainer.querySelectorAll(".float-box").forEach(($f) => {
      let itemId = $f.getAttribute("item-id");
      let itemHeight = parseFloat(
        utils.getCSSVariable(config, "--item-height")
      );
      let item, parent;
      ({ item, parent } = utils.getItemById(config, itemId, "child", true));
      if (item) {
        let top = utils.getPositionByItem(config, item);
        $f.style.top = top * itemHeight + "px";
        if (parent && parent.isClosed) {
          $f.classList.add("hide");
        } else if ($f.classList.contains("hide")) {
          $f.classList.remove("hide");
        }
      }
    });
  },

  /**
   * Recrea indicadores de dias seleccionados
   *
   * @param {Object} $elem - Elemento Dom donde crea los elementos seleccionados
   * @param {Object[]} selected - Array con los dias seleccionados
   * @return {Object} Elemento Dom que contiene los indicadores de dias seleccionados
   */
  redrawSelectBoard($elem, selected) {
    $elem.innerHTML = "";
    let hasNumber = true;

    if (selected) {
      if (selected.length == 1) hasNumber = false;

      selected.forEach((s) => {
        let attributes = [];

        if (hasNumber) {
          attributes.push({ name: "cap-badge", value: s.index });
        }
        let $s = dom.createElement("div", {
          className: "selected-day",
          attributes,
        });
        $s.style.left = s.pos.left + "px";
        $s.style.top = s.pos.top + "px";
        $elem.append($s);
      });
    }
    return $elem;
  },

  /**
   * Obtiene String con estilos CSS de la posicion de un floatBox
   * calculado segun item y fecha de los valores
   *
   * @param {Object} config - Configuracion capibara, para ver fecha de inicio
   * @param {Object} data - Datos del dia indicado
   * @param {Object} item - Item relacionado
   * @return {String} String CSS top y left , para posicionar floatBox en table
   */
  getPositionFloatBox(config, data, item) {
    let left = dateUtils.diffDays(config.timeline.start, data.date);
    let top = utils.getPositionByItem(config, item);
    let dayWidth = parseFloat(utils.getCSSVariable(config, "--day-width"));
    let itemHeight = parseFloat(utils.getCSSVariable(config, "--item-height"));
    left *= dayWidth;
    top *= itemHeight;
    return "top: " + top + ".5px; left:" + left + ".5px;";
  },

  /**
   * Crea Boton como elemento Dom
   *
   * @param {Object} props - propiedades para boton
   * @param {String} props.className - classes css para boton
   * @param {*} props.key - key para identificar a boton
   * @param {String} props.label - Texto boton
   * @return {Object} Elemento Dom del boton creado
   */
  createButton(props) {
    return dom.createElementFromHTML(`
            <button class="${props.className || ""}" input-key="${
              props.key || ""
            }">
              ${props.label || ""}
            </button>`);
  },

  /**
   * Crea Botones de guardar y cancelar los datos de formulario del sidebar derecho
   *
   * @return {Object} Elemento Dom del botones creados
   */
  createNavRightbuttons(config) {
    let btnSection = dom.createElementFromHTML(
      `<div class="button-section"></div>`
    );
    if (
      config.timeline.nav.right &&
      config.timeline.nav.right.hideSaveButton != true
    ) {
      let saveBtn = this.createButton({
        label: "Guardar",
        className: "cap-btn-raised",
      });

      btnSection.appendChild(saveBtn);
      saveBtn.addEventListener("click", (e) => {
        saveBtn.dispatchEvent(
          new CustomEvent("saveMultiInput", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    }

    if (
      config.timeline.nav.right &&
      config.timeline.nav.right.hideCancelButton != true
    ) {
      const cancelLabelButton =
        config.timeline.nav.right.cancelLabelButton || "Cancelar";
      let cancelBtn = this.createButton({
        label: cancelLabelButton,
        className: "cap-btn-outline",
      });

      btnSection.appendChild(cancelBtn);

      cancelBtn.addEventListener("click", (e) => {
        cancelBtn.dispatchEvent(
          new CustomEvent("cancelMultiInput", {
            bubbles: true,
            cancelable: false,
          })
        );
      });
    }

    return btnSection;
  },

  /**
   * Crea Input como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} [dataDay = null] - valores de item y Dia, opcional
   * @param {Object} [daySelected = null] - datos del dia seleccionado
   * @param {Object[]} [selected = []] - array de objetos, de dias seleccionados
   * @return {Object} Elemento Dom del input creado
   */
  createInput(
    props,
    dataDay = null,
    daySelected = null,
    selected = [],
    tabSelected
  ) {
    let r = null;

    if (props.ignoreRender == true) return r;

    switch (props.type) {
      case "custom":
        r = props.render
          ? props.render({ props, dataDay, daySelected, selected })
          : null;
        r.setAttribute("input-key", props.key);
        break;
      case "select":
        r = this.createInputSelect(props, dataDay, daySelected);
        break;
      case "file":
        r = this.createInputFile(props, dataDay);
        break;
      case "fileLink":
        r = this.createFileLink(props, dataDay);
        break;
      case "text":
        r = this.createText(props, selected);
        break;
      case "title":
        r = this.createTitle(props);
        break;
      case "number":
        r = this.createInputNumber(props, dataDay);
        break;
      case "float":
        r = this.createInputFloat(props, dataDay);
        break;
      case "message":
        r = this.createInputMessage(props, dataDay);
        break;
      case "textarea":
        r = this.createInputTextArea(props, dataDay);
        break;
      case "checkbox":
        r = this.createInputCheckbox(props, dataDay);
        break;
      case "inputList":
        r = this.createInputList(props, dataDay, daySelected);
        break;
      case "input-box":
        r = this.createInputBox(props, dataDay);
        break;
      case "list":
        r = this.createList(props, dataDay);
        break;
      case "data-list":
        r = this.createDataList(
          props,
          dataDay,
          daySelected,
          selected,
          tabSelected
        );
        break;
      case "button":
        r = this.createInputButton(props, dataDay);
        break;
      case "buttons-group":
        r = this.createInputButtonsGroup(props, dataDay);
        break;
      case "time":
        r = this.createInputTime(props, dataDay);
        break;
      case "input-text":
      default:
        r = this.createInputText(props, dataDay);
    }

    return r;
  },

  /**
   * Crea Input con formato Hora, como elemento Dom
   *
   * Usa 2 inputs, hora y minutos
   *
   * el value que interpreta es String HH:MM
   *
   * @param {Object} props - config para Input
   * @param {Object} [dataDay = null] - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputTime(props, dataDay) {
    let hhValue = "";
    let mmValue = "";

    if (dataDay && dataDay.values) {
      let value = utils.getVarByPath(dataDay, props.varName);

      if (value) {
        value = value.split(":");
        hhValue = value[0];
        mmValue = value[1];
      }
    }

    let $elem = dom.createElementFromHTML(`
          <div class="input-section ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <span class="text-subtitle d-block">
              ${props.label || ""}
            </span>
            <div class="d-flex">
              <div class="time">
                <input type="number"
                      class="hour"
                      id="test"
                      placeholder="00"
                      min="0"
                      max="99"
                      value="${hhValue}">
                <span class="symbol">:</span>
                <input type="number"
                      class="minutes"
                      placeholder="00"
                      min="0"
                      max="59"
                      value="${mmValue}">
              </div>
              <span class="text-hint">
                ${props.textHint || ""}
              </span>
            </div>
          </div>`);

    let $hhInput = $elem.querySelector(".hour");
    let $mmInput = $elem.querySelector(".minutes");

    $hhInput.addEventListener("input", (e) => {
      $hhInput.dispatchEvent(
        new CustomEvent("changeTimeInputHH", {
          bubbles: true,
          cancelable: false,
          detail: {
            key: props.key,
            value: e.srcElement.value,
          },
        })
      );
    });

    $mmInput.addEventListener("input", (e) => {
      $mmInput.dispatchEvent(
        new CustomEvent("changeTimeInputMM", {
          bubbles: true,
          cancelable: false,
          detail: {
            key: props.key,
            value: e.srcElement.value,
          },
        })
      );
    });

    this.addKeyDownInputEvents($hhInput, $elem, props);
    this.addKeyDownInputEvents($mmInput, $elem, props);

    return $elem;
  },

  /**
   * Crea Text como elemento Dom
   * Se configura como input, pero no devuelve datos, es un texto
   *
   * @param {Object} props - config para Input
   * @return {Object} Elemento Dom del input creado
   */
  createText(props, selected) {
    let label = props.label;
    if (props.formatter) {
      label = props.formatter({ selected });
    }
    let $elem = dom.createElementFromHTML(`
          <div class="input-section ${props.isHidden ? "hide " : ""} ${
            props.toLeft ? "to-left " : ""
          } " input-key="${props.key || ""}">
            <span class="${props.classNames || ""}">
            ${label}
            </span>
          </div>`);
    return $elem;
  },

  /**
   * Crea Title como elemento Dom
   * Se configura como input, pero no devuelve datos, es un texto
   *
   * @param {Object} props - config para Input
   * @return {Object} Elemento Dom del input creado
   */
  createTitle(props) {
    let classNames = "text-title";
    if (props.isSubtitle) {
      classNames = "text-subtitle";
    }
    return dom.createElementFromHTML(`
          <div class="input-section ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <span class="${classNames}">
              ${props.label}
            </span>
          </div>`);
  },

  /**
   * Crea Input tipo Numerico como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputNumber(props, dataDay) {
    let value = "";
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }
    value = value || "";

    let elem = dom.createElementFromHTML(`
          <div class="input-section ${props.classNames || ""} ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <div class="input-section-title">
              <span class="text-subtitle">
                ${props.label}
              </span>
              <span class="text-subtitle-2 ">
                ${props.suffix ? "(" + props.suffix + ")" : ""}
              </span>
            </div>
            <div class="d-flex">
              <input type="number"
                    class="nav-input"
                    value="${value}">

              <span class="text-hint">
                  ${props.textHint || ""}
              </span>
            </div>
          </div>`);

    let inputElem = elem.querySelector("input");

    this.addKeyDownInputEvents(inputElem, elem, props);

    return elem;
  },

  /**
   * Crea Input tipo FIlepicker como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputFile(props, dataDay) {
    let value = "";
    let fileName = "";
    let filePath = "";

    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
      fileName = utils.getVarByPath(dataDay, props.fileNameValue);
      filePath = utils.getVarByPath(dataDay, props.filePathValue);
    }

    value = value || "";
    fileName = fileName || "";
    filePath = filePath || "";

    let elem = dom.createElementFromHTML(`
          <div class="input-section ${props.classNames || ""} ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">

            <div class="input-section-title">
              <span class="text-subtitle">
                ${props.label}
              </span>
            </div>

            <div class="nav-input-file" >
              <button class="small-close clear-file-input">x</button>
              <label >
                <span class="nav-input-file-label">
                  ${props.pickerLabel || "Seleccionar Archivo"}
                </span>
                <input type="file" value="${value}">
              </label>
            </div>

            <div class="input-section-link">
              <a href="${filePath}" target="_blank" >
                ${fileName}
              </a>
            </div>

          </div>`);

    let $inputElem = elem.querySelector("input");
    let $clearBtn = elem.querySelector(".clear-file-input");
    let $fileInputLabel = elem.querySelector(".nav-input-file-label");

    $inputElem.addEventListener("change", (evt) => {
      let fileName = $inputElem.files[0].name;
      $fileInputLabel.textContent = fileName;
    });

    $clearBtn.addEventListener("click", (evt) => {
      $inputElem.value = "";
      $fileInputLabel.textContent = props.pickerLabel || "Seleccionar Archivo";
      evt.stopPropagation();
    });

    return elem;
  },

  createFileLink(props, dataDay) {
    let fileName = "";
    let filePath = "";

    if (dataDay && dataDay.values) {
      //value = utils.getVarByPath(dataDay, props.varName);
      fileName = utils.getVarByPath(dataDay, props.fileNameValue);
      filePath = utils.getVarByPath(dataDay, props.filePathValue);
    }

    if (fileName == "") return null;

    fileName = fileName || "";
    filePath = filePath || "";

    let elem = dom.createElementFromHTML(`
          <div class="input-section ${props.classNames || ""} ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">

            <div class="input-section-title">
              <span class="text-subtitle">
                ${props.label}
              </span>
            </div>

            <div class="input-section-link">
              <a href="${filePath}" target="_blank" >
                ${fileName}
              </a>
            </div>

          </div>`);

    return elem;
  },

  /**
   * Crea lista de Inputs, con array de valores, como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @param {Object} [daySelected = null] - datos del dia seleccionado
   * @return {Object} Elemento Dom del input creado
   */
  createInputList(props, dataDay, daySelected = null) {
    let inputList = [];

    //obtiene inputs default, para crearlos, sin valores
    if (
      daySelected &&
      Array.isArray(daySelected.item.values[props.inputList])
    ) {
      inputList = daySelected.item.values[props.inputList].map((inputRow) => {
        //limpia inputList inputs
        inputRow.inputs.map((input) => {
          input.value = null;
          return input;
        });

        //obtiene valores del dia
        if (dataDay && dataDay.values) {
          let inputValues = utils.getVarByPath(dataDay, props.valuesPath);

          //autorrellena
          if (inputValues) {
            inputRow.inputs.forEach((input) => {
              let values = inputValues.find((v) => v.id == inputRow.id);
              input.value = values ? values[input.varName] : null;
            });
          }
        }

        return inputRow;
      });
    }

    let str = `<div input-key="${props.key || ""}" class="input-section-list ${
      props.isHidden ? "hide" : ""
    } ">
            <div class="input-section-title">
              <span class="text-title">
                ${props.label}
              </span> 
            </div>`;
    inputList.forEach((inputRow) => {
      str += `
                  <div class="input-section ${
                    props.toRight ? "to-right" : ""
                  }" >
                      <span class="text-subtitle">
                        ${inputRow.label}
                      </span>`;

      inputRow.inputs.forEach((input) => {
        str += ` 
                    <div class="input-section-col" >
                      <span class="text-subtitle ">
                        ${input.label}
                      </span>
                      <input type="number"
                            input-id="${inputRow.id}"
                            input-label="${input.label}"
                            class="nav-input small"
                            value="${input.value}">
                    </div>`;
      });
      str += `
                  </div>`;
    });
    str += `</div>`;

    let elem = dom.createElementFromHTML(str);

    let inputsElem = elem.querySelectorAll("input");
    inputsElem.forEach((inputElem) =>
      this.addKeyDownInputEvents(inputElem, elem, props)
    );
    return elem;
  },

  createInputSelect(props, dataDay, daySelected = null) {
    let value = null;
    let options = props.options;

    if (dataDay && dataDay.values) {
      let val = utils.getVarByPath(dataDay, props.varName);
      value = val ? val.value : null;
    }

    let str = `<div input-key="${props.key || ""}" class="${
      props.isHidden ? "hide" : ""
    }" >
                  <div class="input-section " >
                      <span class="text-subtitle">
                        ${props.label}
                      </span>

                      <select class="input-select">

                 <option value="" ${value == null ? "selected" : ""}>
                  ${props.noDataLabel || "-- sin asignar --"}
                 </option>

        `;

    options.forEach((option) => {
      str += `
                 <option value="${option.id}" ${
                   option.id == value ? "selected" : ""
                 }>
                 ${option.label}
                 </option>
                `;
    });
    str += `

                      </select>
                  </div>

      </div>`;

    let elem = dom.createElementFromHTML(str);

    return elem;
  },

  /**
   * Crea Input de texto TextArea como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputTextArea(props, dataDay) {
    let value = "";
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }
    let elem = dom.createElementFromHTML(`
          <div class="input-section ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <span class="text-subtitle d-block">
              ${props.label}
            </span>
            <textarea class="" value="${value}"></textarea>
          </div>`);

    let inputElem = elem.querySelector("textarea");
    this.addKeyDownInputEvents(inputElem, elem, props);
    return elem;
  },

  /**
   * Crea Input de Mensaje como elemento Dom
   * utiliza TextArea
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputMessage(props, dataDay) {
    let value = dataDay ? utils.getVarByPath(dataDay, props.varName) : null;
    let elem = dom.createElementFromHTML(`
          <div class="input-section to-left msj-input-container ${
            props.isHidden ? "hide" : ""
          } "
              input-key="${props.key || ""}">
            <span class="text-subtitle d-block">
              Mensaje
            </span>
            <textarea class="msj">${value || ""}</textarea>
          </div>`);

    let inputElem = elem.querySelector("textarea");
    //this.addKeyDownInputEvents(inputElem);
    return elem;
  },

  /**
   * Crea Input de texto como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputText(props, dataDay) {
    let value = "";
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }
    let elem = dom.createElementFromHTML(`
          <div class="input-section ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <span class="text-subtitle d-block">
              ${props.label}
            </span>
            <input type="text" class="nav-input" value="${value}">
          </div>`);

    let inputElem = elem.querySelector("input");
    this.addKeyDownInputEvents(inputElem, elem, props);
    return elem;
  },

  /**
   * Crea Input Numero Flotante, como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputFloat(props, dataDay) {
    let value = "";
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }
    let elem = dom.createElementFromHTML(`
          <div class="input-section ${
            props.isHidden ? "hide" : ""
          } " input-key="${props.key || ""}">
            <span class="text-subtitle d-block">
              ${props.label}
            </span>
            <input type="number" class="nav-input" value="${value}">
          </div>`);

    let inputElem = elem.querySelector("input");
    this.addKeyDownInputEvents(inputElem, elem, props);
    return elem;
  },

  createDataList(props, dataDay, daySelected, selected, tabSelected) {
    let list = [];
    if (dataDay && dataDay.values) {
      list = utils.getVarByPath(dataDay, props.varName) || [];
    }

    let searchInput = props.hideSearch
      ? ""
      : ` <div class="search-input-container">
                          <input id="" type="text" name="" class="search-input" placeholder="buscar">
                          <button class="search-input-clear">
                          ${icons.closeIcon}
                          </button>
                        </div>`;

    let editBtn = "";

    if (props.editButton) {
      editBtn = `<button class="cap-btn-raised cap-btn-sm edit-btn" input-key="${props.editButton.key}">
                    ${props.editButton.label} 
                  </button> `;
    }

    let str = `
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
        <div class="input-section-title">
            <span class="text-subtitle d-block">
                ${props.label}
              </span>
              ${editBtn}	
        </div>
        <div class="input-box " input-key="${props.key || ""}">
            ${searchInput}
        </div>
      </div> `;

    let $el = dom.createElementFromHTML(str);
    let $searchInput = $el.querySelector(".search-input");
    let $searchInputClear = $el.querySelector(".search-input-clear");
    let $inputBox = $el.querySelector(".input-box");

    let $items = list.map((item, j) => {
      let label =
        utils.getVarByPath(item, props.varNameLabel) || dataDay.label || "__";

      let $elItem = this.createDataListItem({
        label,
        valueText: utils.getVarByPath(item, props.varNameValueText) || "",
        subText: utils.getVarByPath(item, props.varNameSubText) || "",
        id: "list-item-" + (dataDay.id || j),
        key: "list-item-" + (dataDay.id || j),
        tooltip: utils.getVarByPath(item, props.varNameTooltip) || label,
        //colorIcon: "c-red",
        link: utils.getVarByPath(item, props.varNameLink) || null,
        suffix: utils.getVarByPath(item, props.varNameSuffix) || "",
        //value: JSON.stringify(utils.getVarByPath(item, props.varNameInput)) || {}
      });
      $inputBox.appendChild($elItem);
    });

    if ($searchInput) {
      $searchInputClear.addEventListener("click", (e) => {
        $searchInput.value = "";
        $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
      });

      $searchInput.addEventListener("keydown", (e) => {
        if (e.key === "Escape") {
          $searchInput.value = "";
          $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
        }
      });

      $searchInput.addEventListener("input", (e) => {
        let $inputs = [];
        $inputs = $el.querySelectorAll(".mini-input-section") || [];
        $inputs.forEach(($i) => {
          let label = $i.getAttribute("label");
          let search = $searchInput.value;
          if (label.toLowerCase().includes(search.toLowerCase())) {
            $i.classList.remove("hide");
          } else {
            $i.classList.add("hide");
          }
        });
      });
    }

    if (props.editButton.onClick) {
      let button = $el.querySelector("button.edit-btn");
      if (button && props.editButton) {
        button.addEventListener("click", (e) =>
          props.editButton.onClick({
            event: e,
            props,
            dataDay,
            daySelected,
            selected,
            $element: $el,
            tabSelected,
          })
        );
      }
      $items.forEach(($i) => {
        $el.addEventListener("click", (e) => {
          props.editButton.onClick({
            event: e,
            props,
            dataDay,
            daySelected,
            selected,
            $element: $el,
            tabSelected,
          });
        });
      });
    }

    return $el;
  },

  createDataListItem({
    label = "",
    valueText = "",
    subText = "",
    id = "",
    key = "",
    tooltip = "",
    colorIcon = "",
    link = null,
    suffix = null,
    value = "",
  }) {
    let tooltipStr = "";
    let iconStr = null;
    let linkStr = "";
    let str = "";

    if (tooltip) {
      tooltipStr = `
                  <div class="mini-input-section--tooltip">
                    ${tooltip}
                  </div>`;
    }

    if (colorIcon) {
      if (utils.isHexColor(itemFormated.colorIcon)) {
        iconStr = `<div class="mini-input-section--icon" style="background:${colorIcon}"> </div>`;
      } else {
        iconStr = `<div class="mini-input-section--icon ${colorIcon}"> </div>`;
      }
    }

    if (link) {
      linkStr = `<a class="mini-input-section--link" target="_blank" href="${link}">${icons.linkIcon}</a> `;
    }

    str += `
            <div class="mini-input-section clickeable ${
              iconStr ? "has-icon" : ""
            } " 
                label="${label}" 
                data-id="${id}"
                data-key="${key}"
                data-value="${value || ""}"
                >
              <label>
                ${iconStr || ""}

                <div class="mini-input-section--titles">
                  <div class="mini-input-section--title truncate">
                    ${label}
                  </div>
                  <div class="mini-input-section--subtitle truncate">
                    ${subText}
                  </div>
                </div>

                <div class="mini-input-section--content" >
                  ${valueText}
                </div>
                <div class="mini-input-section--suffix" >
                  ${suffix}
                </div>

                ${linkStr}

              </label>
              ${tooltipStr}
            </div>`;
    return dom.createElementFromHTML(str);
  },

  createList(props, dataDay) {
    let tabsContainer = "";

    if (props.tabs) {
      let tabs = "";
      props.tabs.forEach((tab) => {
        tabs += `
        <div class="input-box-tab" tab-key="${tab.key}">
          ${tab.name}
        </div>`;
      });

      tabsContainer = `
        <div class="input-box-tab-container">
        ${tabs}
        </div>
      `;
    }

    let searchInput = `
      <div class="search-input-container">
        <input id="" type="text" name="" class="search-input" placeholder="buscar">
        <button class="search-input-clear">
         ${icons.closeIcon}
        </button>
      </div>
    `;

    let str = `
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
           <span class="text-subtitle d-block">
              ${props.label}
            </span>

            ${tabsContainer}
        <div class="input-box " input-key="${props.key || ""}">

            ${searchInput}
      `;

    //TODO obtener input segun fila
    if (props.inputs) {
      props.inputs.forEach((i) => {
        if (i.isTitle) {
          let tooltip = "";
          let icon = null;

          if (i.tooltip) {
            tooltip = `
                  <div class="mini-input-section--tooltip">
                    ${i.tooltip}
                    ${i.suffix ? "(" + i.suffix + ")" : ""}
                  </div>`;
          }

          if (i.colorIcon) {
            if (utils.isHexColor(i.colorIcon)) {
              icon = `<div class="mini-input-section--icon" style="background:${i.colorIcon}"> </div>`;
            } else {
              icon = `<div class="mini-input-section--icon ${i.colorIcon}"> </div>`;
            }
          }

          str += `
            <div class="mini-input-section is-title  ${
              icon ? "has-icon" : ""
            }" input-tab-key="${i.tabKey || ""}" label="${i.label}">
              <label>
                ${icon || ""}
                <div class="mini-input-section--title truncate">
                  ${i.label}
                </div>
              </label>
              ${tooltip}
            </div>`;
        } else {
          let tooltip = "";
          let icon = null;
          let link = "";

          if (i.tooltip) {
            tooltip = `
                  <div class="mini-input-section--tooltip">
                    ${i.tooltip}
                  </div>`;
          }

          if (i.colorIcon) {
            if (utils.isHexColor(i.colorIcon)) {
              icon = `<div class="mini-input-section--icon" style="background:${i.colorIcon}"> </div>`;
            } else {
              icon = `<div class="mini-input-section--icon ${i.colorIcon}"> </div>`;
            }
          }

          if (i.link) {
            link = `<a class="mini-input-section--link" target="_blank" href="${i.link}">
                ${icons.linkIcon}
                </a> `;
          }

          str += `
            <div class="mini-input-section  ${
              icon ? "has-icon" : ""
            }" input-tab-key="${i.tabKey || ""}" label="${i.label}">
              <label>

                ${icon}

                <div class="mini-input-section--title truncate">
                  ${i.label}
                </div>

                <div class="mini-input-section--suffix" >${
                  i.suffix ? "(" + i.suffix + ")" : ""
                }</div>

                ${link}

              </label>
              <div class="mini-input-section--footer">
                <span class="start"> start</span>
                <span class="end"> end </span>
              </div>
              ${tooltip}
            </div>`;
        }
      });
    }

    str += `
        </div>
      </div> `;

    let $el = dom.createElementFromHTML(str);
    let $tabs = $el.querySelectorAll(".input-box-tab[tab-key]") || [];
    let $searchInput = $el.querySelector(".search-input") || null;
    let $searchInputClear = $el.querySelector(".search-input-clear") || null;

    $tabs.forEach((t) => {
      t.addEventListener("click", (e) => {
        this.selectInputBoxTab(t, $el);
        $searchInput.value = "";
        $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    });

    if ($searchInputClear) {
      $searchInputClear.addEventListener("click", (e) => {
        $searchInput.value = "";
        $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    }

    if ($searchInput) {
      $searchInput.addEventListener("keydown", (e) => {
        if (e.key === "Escape") {
          $searchInput.value = "";
          $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
        }
      });

      $searchInput.addEventListener("input", (e) => {
        let $tabSelected =
          $el.querySelector(".input-box-tab[tab-key].active") || null;
        let $inputs = [];
        if ($tabSelected) {
          let tabKey = $tabSelected.getAttribute("tab-key");
          $inputs =
            $el.querySelectorAll(
              ".mini-input-section[input-tab-key='" + tabKey + "']"
            ) || [];
        } else {
          $inputs = $el.querySelectorAll(".mini-input-section") || [];
        }
        $inputs.forEach(($i) => {
          let label = $i.getAttribute("label");
          let search = $searchInput.value;
          if (label.toLowerCase().includes(search.toLowerCase())) {
            $i.classList.remove("hide");
          } else {
            $i.classList.add("hide");
          }
        });
      });
    }

    this.selectInputBoxTab(_.first($tabs), $el);

    return $el;
  },

  /**
   * Crea InputBox, como elemento Dom
   * un InputBox es un contenedor con una lista de inputs
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputBox(props, dataDay) {
    let value = [];
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }

    let tabsContainer = "";

    if (props.tabs) {
      let tabs = "";
      props.tabs.forEach((tab) => {
        tabs += `
        <div class="input-box-tab" tab-key="${tab.key}">
          ${tab.name}
        </div>`;
      });

      tabsContainer = `
        <div class="input-box-tab-container">
        ${tabs}
        </div>
      `;
    }

    let searchInput = `
      <div class="search-input-container">
        <input id="" type="text" name="" class="search-input" placeholder="buscar">
        <button class="search-input-clear">
         ${icons.closeIcon}
        </button>
      </div>
    `;

    let str = `
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
           <span class="text-subtitle d-block">
              ${props.label}
            </span>

            ${tabsContainer}

            <div class="input-box " input-key="${props.key || ""}">

            ${searchInput}
      `;

    if (props.inputs) {
      props.inputs.forEach((i) => {
        if (i.isTitle) {
          let tooltip = "";
          let icon = "";

          if (i.tooltip) {
            tooltip = `
                  <div class="mini-input-section--tooltip">
                    ${i.tooltip}
                    ${i.suffix ? "(" + i.suffix + ")" : ""}
                  </div>`;
          }

          if (i.colorIcon) {
            if (utils.isHexColor(i.colorIcon)) {
              icon = `<div class="mini-input-section--icon" style="background:${i.colorIcon}"> </div>`;
            } else {
              icon = `<div class="mini-input-section--icon ${i.colorIcon}"> </div>`;
            }
          }

          str += `
            <div class="mini-input-section is-title" input-tab-key="${
              i.tabKey || ""
            }" label="${i.label}">
              <label>
                ${icon}
                <div class="mini-input-section--title truncate">
                  ${i.label}
                </div>
              </label>
              ${tooltip}
            </div>`;
        } else {
          let value_found = value ? value.find((val) => val.id == i.id) : null;
          let v = value_found || "";
          let inputValue =
            props.varNameInput && v[props.varNameInput]
              ? v[props.varNameInput]
              : v.value;
          let tooltip = "";
          let icon = "";
          let link = "";

          if (i.tooltip) {
            tooltip = `
                  <div class="mini-input-section--tooltip">
                    ${i.tooltip}
                  </div>`;
          }

          if (i.colorIcon) {
            if (utils.isHexColor(i.colorIcon)) {
              icon = `<div class="mini-input-section--icon" style="background:${i.colorIcon}"> </div>`;
            } else {
              icon = `<div class="mini-input-section--icon ${i.colorIcon}"> </div>`;
            }
          }

          if (i.link) {
            link = `<a class="mini-input-section--link" target="_blank" href="${i.link}">
                ${icons.linkIcon}
                </a> `;
          }

          str += `
            <div class="mini-input-section ${
              icon ? "has-icon" : ""
            }" input-tab-key="${i.tabKey || ""}" label="${i.label}">
              <label>
                <input type="number"
                      class="mini-input"
                      input-id="${i.id || ""}"
                      input-key="${i.key || i.label}"
                      value="${inputValue || ""}">

                ${icon || ""}

                <div class="mini-input-section--title truncate">
                  ${i.label}
                </div>

                <div class="mini-input-section--suffix" >${
                  i.suffix ? "(" + i.suffix + ")" : ""
                }</div>

                ${link}

              </label>
              ${tooltip}
            </div>`;
        }
      });
    }

    str += `
        </div>
      </div> `;

    let $el = dom.createElementFromHTML(str);
    let $tabs = $el.querySelectorAll(".input-box-tab[tab-key]") || [];
    let $inputs = $el.querySelectorAll("input.mini-input") || [];
    let $searchInput = $el.querySelector(".search-input") || null;
    let $searchInputClear = $el.querySelector(".search-input-clear") || null;

    $tabs.forEach((t) => {
      t.addEventListener("click", (e) => {
        this.selectInputBoxTab(t, $el);
        $searchInput.value = "";
        $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    });

    if ($searchInputClear) {
      $searchInputClear.addEventListener("click", (e) => {
        $searchInput.value = "";
        $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    }

    if ($searchInput) {
      $searchInput.addEventListener("keydown", (e) => {
        if (e.key === "Escape") {
          $searchInput.value = "";
          $searchInput.dispatchEvent(new Event("input", { bubbles: true }));
        }
      });

      $searchInput.addEventListener("input", (e) => {
        let $tabSelected =
          $el.querySelector(".input-box-tab[tab-key].active") || null;
        let $inputs = [];
        if ($tabSelected) {
          let tabKey = $tabSelected.getAttribute("tab-key");
          $inputs =
            $el.querySelectorAll(
              ".mini-input-section[input-tab-key='" + tabKey + "']"
            ) || [];
        } else {
          $inputs = $el.querySelectorAll(".mini-input-section") || [];
        }
        $inputs.forEach(($i) => {
          let label = $i.getAttribute("label");
          let search = $searchInput.value;
          if (label.toLowerCase().includes(search.toLowerCase())) {
            $i.classList.remove("hide");
          } else {
            $i.classList.add("hide");
          }
        });
      });
    }

    this.selectInputBoxTab(_.first($tabs), $el);

    $inputs.forEach((i) => this.addKeyDownInputEvents(i, $el, props));

    return $el;
  },

  selectInputBoxTab($tab, $el) {
    let $tabs = $el.querySelectorAll(".input-box-tab[tab-key]") || [];
    let $inputSections = $el.querySelectorAll(".mini-input-section") || [];

    $tabs.forEach((t) => {
      t.classList.remove("active");
    });

    if ($tab) {
      $tab.classList.add("active");

      $inputSections.forEach((i) => {
        let key = i.getAttribute("input-tab-key");
        if (key == $tab.getAttribute("tab-key")) {
          i.classList.remove("hide");
        } else {
          i.classList.add("hide");
        }
      });
    }
  },

  /**
   * Crea Checkbox, como elemento Dom
   *
   * @param {Object} props - config para Input
   * @param {Object} dataDay - valores de item y Dia, opcional
   * @return {Object} Elemento Dom del input creado
   */
  createInputCheckbox(props, dataDay) {
    let value = "";
    if (dataDay && dataDay.values) {
      value = utils.getVarByPath(dataDay, props.varName);
    }
    let r = dom.createElementFromHTML(`
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
        <label>
          <input type="checkbox" name="${props.key || ""}" input-key="${
            props.key || ""
          }" ${value ? "checked" : ""}>
          ${props.label}
        </label>
      </div>`);

    return r;
  },

  createInputButtonsGroup(props) {
    let $container = dom.createElementFromHTML(`
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
        <div class="buttons-group"> </div>
      </div>`);

    let $containerButtonGroup = $container.querySelector(".buttons-group");

    let $buttons = [];
    $buttons = props.buttons.map((buttonProp) => {
      let buttonStr = `
        <button 
          name="${buttonProp.key || ""}" 
          input-key="${buttonProp.key || ""}"
          class="cap-btn-outline cap-btn-sm ${
            buttonProp.isSelected ? "selected" : ""
          } ">
          ${buttonProp.label}
        </button>`;

      let $button = dom.createElementFromHTML(buttonStr);
      $containerButtonGroup.append($button);

      $button.addEventListener("click", (e) => {
        if (props.isSelectable) {
          $buttons.forEach(($b) => $b.classList.remove("selected"));
          $button.classList.add("selected");
        }
        buttonProp.onClick({ props, event: e, buttonsElements: $buttons });
      });
      return $button;
    });

    return $container;
  },

  createInputButton(props, dataDay) {
    let r = dom.createElementFromHTML(`
      <div class="input-section ${props.isHidden ? "hide" : ""} " input-key="${
        props.key || ""
      }">
          <button 
            name="${props.key || ""}"
            input-key="${props.key || ""}"
            class="cap-btn-outline">
            ${props.label}
          </button>
      </div>`);

    let button = r.querySelector("button");
    button.addEventListener("click", (e) =>
      props.onClick({ props, event: e, dataDay })
    );

    return r;
  },

  addKeyDownInputEvents($input, $inputContainer, props) {
    $input.addEventListener("keydown", function (event) {
      if (event.keyCode === 13) {
        //ENTER
        $input.dispatchEvent(
          new CustomEvent("saveMultiInput", {
            bubbles: true,
            cancelable: false,
          })
        );
      }
      if (event.keyCode === 27) {
        //ESC
        $input.dispatchEvent(
          new CustomEvent("cancelMultiInput", {
            bubbles: true,
            cancelable: false,
          })
        );
      }
    });
    $input.addEventListener("input", (e) => {
      if (props.onInput) {
        props.onInput({
          event: e,
          value: e.srcElement.value,
          props,
          $element: $inputContainer,
        });
      }
    });
  },

  createContextMenu({
    config,
    target,
    $capibaraElem = null,
    evt = null,
    date = null,
    item = null,
    selected = [],
    tabSelected = null,
    left = null,
    top = null,
    name = "",
  }) {
    if (
      config.timeline.contextMenu === undefined ||
      config.timeline.contextMenu.options == undefined
    )
      return { elem: null, isVisible: false };

    let className = "";
    let style = "";
    let strHtml = "";
    let leftPosition = null;
    let topPosition = null;
    let isVisible = true;
    let boundRectCapibara = null;
    let gap = 15; //px

    if ($capibaraElem) {
      boundRectCapibara = $capibaraElem.getBoundingClientRect();
    }

    if (evt && boundRectCapibara) {
      left = evt.clientX;
      top = evt.clientY - boundRectCapibara.top;
    }

    if (left) {
      leftPosition = left;
    }

    if (top) {
      topPosition = top;
    }

    if ((leftPosition !== null) & (topPosition !== null)) {
      if (boundRectCapibara) {
        let transformStyle = "";
        if (
          topPosition > (boundRectCapibara.height * 2) / 6 &&
          topPosition <= (boundRectCapibara.height * 4) / 6
        ) {
          transformStyle += `translateY(-50%)`;
        }
        if (topPosition > (boundRectCapibara.height * 4) / 6) {
          transformStyle += `translateY(-100%)`;
        }

        if (leftPosition > (boundRectCapibara.width * 4) / 6) {
          transformStyle += `translateX(-100%)`;
          leftPosition -= gap;
        } else {
          leftPosition += gap;
        }
        if (transformStyle != "") {
          style += ` transform: ${transformStyle};`;
        }
      }

      style += `top: ${topPosition}px; left: ${leftPosition}px;`;
    } else {
      className += "cm-hide";
      isVisible = false;
    }

    strHtml += ` <div class="cap-context-menu-container ${className}">
                    <div class="cap-context-menu" style="${style}">
                    </div>
                 </div>`;

    let $contextMenuContainer = dom.createElementFromHTML(strHtml);
    let $contextMenu = $contextMenuContainer.querySelector(".cap-context-menu");
    let hasSomeoption = false;

    config.timeline.contextMenu.options.forEach((option) => {
      if (option.showIf) {
        let show = option.showIf({ item, date, selected, name, tabSelected });
        if (!show) return;
      }

      let isEnabled = true;
      let isClickeable = true;
      if (option.enableIf) {
        isEnabled = option.enableIf({
          item,
          date,
          selected,
          name,
          tabSelected,
        });
      }

      let optionClassName = "";
      let strIcon = "";
      let label = "";

      if (!isEnabled) {
        optionClassName += "is-disabled ";
      }

      if (option.isDivider) {
        isClickeable = false;
        optionClassName += "is-divider ";
      }

      if (option.label) {
        label = `<span>${option.label}</span>`;
      }

      if (option.icon) {
        strIcon = `<div class="cap-context-menu_item_icon">
                      <svg viewBox="0 0 24 24">
                        <path d="${option.icon}" />
                      </svg>
                  </div>`;
      }

      let optionStrHtml = `
        <div class="cap-context-menu_item ${optionClassName}">
          ${strIcon}
          ${label}
        </div>`;

      let $option = dom.createElementFromHTML(optionStrHtml);

      if (isEnabled && isClickeable) {
        $option.addEventListener("click", (evtClick) =>
          option.onClick({
            config,
            evt: evtClick,
            date,
            item,
            selected,
            tabSelected,
            target,
          })
        );
      } else {
        $option.addEventListener("click", (evtClick) => {
          evtClick.preventDefault();
          evtClick.stopPropagation();
        });
      }

      $contextMenu.append($option);
      hasSomeoption = true;
    });

    return {
      elem: $contextMenuContainer,
      isVisible,
    };
  },
};
