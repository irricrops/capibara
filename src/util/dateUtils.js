import utils from "../util/utils";

export default {
  /**
   * Dia en milisegundos
   */
  dayInMilliseconds: 24 * 60 * 60 * 1000,

  /**
   * Obtiene Array de fechas con formato arbol en objetos
   *
   * basado en repuesta de stackoverflow
   * https://stackoverflow.com/questions/4413590/javascript-get-array-of-dates-between-2-dates
   *
   * @param {Date} dateIni - fecha inicio
   * @param {Date} dateEnd - fecha termino
   * @return {Object} objeto con formato fechas con formato de arbol
   */
  getDateTreeRange(dateIni, dateEnd) {
    let obj = {};
    let arr = this.getDateRange(dateIni, dateEnd);
    let today = new Date();
    arr.forEach((a) => {
      obj[a.getUTCFullYear()] = obj[a.getUTCFullYear()] || {};
      obj[a.getUTCFullYear()][a.getUTCMonth()] =
        obj[a.getUTCFullYear()][a.getUTCMonth()] || {};
      obj[a.getUTCFullYear()][a.getUTCMonth()][a.getUTCDate()] = {
        label: a.getUTCDate(),
        date: a,
        isWeekend: [5, 6].indexOf(a.getDate) > -1,
        isToday: today.toDateString() == a.toDateString(),
        isFuture: today.getTime() < a.getTime(),
      };
    });
    return obj;
  },

  /**
   * Obtiene Array de Dates, con las fechas entre 2 rangos
   *
   * basado en repuesta de stackoverflow
   * https://stackoverflow.com/questions/4413590/javascript-get-array-of-dates-between-2-dates
   *
   * @param {Date} dateIni - fecha inicio
   * @param {Date} dateEnd - fecha termino
   * @return {Date[]} array de Dates
   */

  getDateRange(dateIni, dateEnd) {
    let arr = [];
    let dateIniCorrected = new Date(
      dateIni.getUTCFullYear(),
      dateIni.getUTCMonth(),
      dateIni.getUTCDate()
    );
    let dateEndCorrected = new Date(
      dateEnd.getUTCFullYear(),
      dateEnd.getUTCMonth(),
      dateEnd.getUTCDate(),
      4
    );
    for (
      let dt = new Date(dateIniCorrected);
      dt <= dateEndCorrected;
      dt.setDate(dt.getDate() + 1)
    ) {
      arr.push(new Date(dt));
    }
    return arr;
  },

  /**
   * Compara 2 fechas, sin considerar las horas
   *
   * @param {Date} date1 - Fecha a comparar
   * @param {Date} date2 - Fecha a comparar
   * @return {Boolean} Retorna True si la fecha es la misma
   */
  isSameDate(date1, date2) {
    return (
      date1.getFullYear() === date2.getFullYear() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getDate() === date2.getDate()
    );
  },

  /**
   * Verifica si la fecha enviadas es futura, sin considerar las horas
   * la compara con un new Date()
   *
   * @param {Date} date - fecha a analizar
   * @return {Boolean} true si la fecha es futura
   */
  isFuture(date) {
    return date.setHours(0, 0, 0, 0) > new Date().setHours(0, 0, 0, 0);
  },

  /**
   * Verifica si la fecha enviadas es hoy
   * la compara con un new Date()
   *
   * @param {Date} date - fecha a analizar
   * @return {Boolean} true si la fecha es hoy
   */
  isToday(date) {
    return this.isSameDate(date, new Date());
  },

  /**
   * Verifica si es una fecha valida
   *
   * @param {Date} d - Fecha a analizar
   * @return {Boolean} retorna true si es que es valido
   */
  isValidDate(date) {
    return date instanceof Date && !isNaN(date);
  },

  /**
   * Obtiene la diferencia en dias entre 2 fechas
   *
   * Si la fecha de termino no se envia, se considera el dia actual
   *
   * Basado en
   * https://www.geeksforgeeks.org/how-to-calculate-the-number-of-days-between-two-dates-in-javascript/
   *
   * @param {Date} ini - Fecha inicio
   * @param {Date} [end = new Date() ] - Fecha Termino
   * @return {Integer} Cantidad de dias de diferencia
   */
  diffDays(ini, end) {
    let iniDate = new Date(ini);
    let endDate = end ? new Date(end) : new Date();
    if (this.isValidDate(iniDate) && this.isValidDate(endDate)) {
      let iniDate_utc = new Date(
        iniDate.getTime() + iniDate.getTimezoneOffset() * 60000
      );
      let endDate_utc = new Date(
        endDate.getTime() + endDate.getTimezoneOffset() * 60000
      );
      let time = endDate_utc.getTime() - iniDate_utc.getTime();
      let days = utils.round(time / (1000 * 3600 * 24), 0);
      return days;
    }
    return 0;
  },

  /**
   * Suma dias a una fecha (preferentemente String)
   * devuelve la fecha formateada como String "YYYY-MM-DD"
   *
   * @param {Date|String} date - Fecha de referencia
   * @param {Integer} days - Dias a sumar a la fecha
   * @return {String} Devuelve fecha formateada ya sumada
   */
  addDaysString(date, days) {
    let d = new Date(date);
    let date_utc = new Date(d.getTime() + d.getTimezoneOffset() * 60000);
    date_utc.setDate(date_utc.getDate() + days);
    let dateFormated = this.formatDate(date_utc);
    return dateFormated;
  },

  /**
   * Formatea fecha Date a string con formato YYYY-MM-DD
   *
   * @param {Date} date - Fecha a formatear
   * @return {String} Fecha formateada
   */
  formatDate(date) {
    let month = "" + (date.getMonth() + 1),
      day = "" + date.getDate(),
      year = date.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  },

  /**
   * Formatea fecha Date a string con formato 01 de Enero, 2023
   *
   * @param {Date} date - Fecha a formatear
   * @return {String} Fecha formateada
   */
  formatDateReadeable(dateParam) {
    let date = dateParam;

    if (typeof dateParam == "string") {
      date = new Date(dateParam + "T12:00:00-00:00");
    }

    let day = date.getDate();
    let year = date.getFullYear();

    let monthReadeable = utils.toCapitalize(
      date.toLocaleString("default", { month: "long" })
    );

    return `${day} de  ${monthReadeable} , ${year}`;
  },
};
