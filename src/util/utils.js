import dateUtils from '../util/dateUtils';

export default {
  tablePositions: {
    scrollLeft: 0,
    scrollTop: 0,
    scrollWidth: 0,
    scrollHeight: 0,
    boundingClientTable: null,
    boundingClientContainer: null,

    containerWidth: 0,
    containerHeight: 0,
    navLeftWidth: 0,
    headerHeight: 0,
    dayWidth: 0,
    itemHeight: 0
  },

  /**
   * Obtiene valor de un objeto segun su ruta
   * la ruta usa formato separada por punto
   * compatible con arrays
   *
   * Si no se encuentra, retorna null
   *
   * @param {Object} obj - objeto donde se busca el valor
   * @param {String} [path=''] - ruta del valor
   * @return {*} Valor encontrado
   */

  getVarByPath(obj = null, path = '', defaultReturn = null) {
    let paths = path.split('.');
    let current = obj || {};

    for (let i = 0; i < paths.length; ++i) {
      const pathSegment = paths[i];

      // Verificar si el segmento de ruta contiene una referencia a un array
      if (pathSegment.includes('[') && pathSegment.includes(']')) {
        const arrName = pathSegment.split('[')[0];
        const arrIndex = parseInt(pathSegment.split('[')[1].replace(']', ''));

        if (current[arrName] === undefined || !Array.isArray(current[arrName])) {
          return defaultReturn;
        } else {
          current = current[arrName][arrIndex];
        }
      } else {
        // Si no es un array, trata la ruta como una propiedad de objeto normal
        if (current[pathSegment] === undefined) {
          return defaultReturn;
        } else {
          current = current[pathSegment];
        }
      }
    }

    if (current === undefined) return defaultReturn;
    return current;
  },



  /**
   * Obtiene posicion de elemento de un array segun date e item
   * Usada para obtener posicion del dia seleccionado
   *
   * la lista de dias seleccionados en list contienen:
   *   {
   *     date: Date,
   *     item: {
   *       id: Integer,    
   *     }
   *   },
   *
   * Si no lo encuentra devuelve -1;
   *
   * @param {Array} list - Arreglo de dias seleccionados
   * @param {Object} props - propiedades de dia seleccionado
   * @param {Date} props.date - fecha para comparar posicion horizontal
   * @param {Object} props.item - item de sidebar para comparar posicion vertical
   * @return {[TODO:type]} [TODO:description]
   */
  indexSelectedDay(list, props) {
    let index = -1;
    list.forEach((l, i) => {
      if (l.date == props.date && l.item.id == props.item.id) {
        index = i;
      }
    })
    return index;
  },


  /**
   * Obtiene posicion vertical del item segun los datos
   * mostrados en la planilla
   *
   * si el arbol de un item padre no esta desplegado, no lo considera
   *
   * si no lo encuentra, devuelve -1
   *
   * TODO dar posibilidad de position para parents
   *
   * @param {Array} data - Arreglo de datos, con formato capibara
   * @param {item} item - Item a buscar en Array
   * @return {Integer} Posicion, default -1
   */
  getPositionByItem(config, item, typeNode = "child") {
    let position = -1;
    let finalPosition = -1;

    config.data.forEach(d => {
      if (d.isHidden) return;
      position++;
      if (d.id == item.id && typeNode == "parent") {
        finalPosition = position;
      }
      d.children.forEach(c => {
        if (c.isHidden) return;
        if (!d.isClosed) {
          position++;
          if (typeNode == "child" && c.id == item.id) {
            finalPosition = position;
          }
        }
      });

    });

    return finalPosition;
  },


  /**
   * Obtiene item que corresponda a la posicion de mouse
   * usado en eventos click
   *
   * si el arbol de un item padre no esta desplegado, no lo considera
   *
   * si no lo encuentra, devuelve null;
   *
   * @param {Object} config - Capibara config
   * @param {Object} evt - Evento mouse click
   * @param {Object} $table - Elemento de dom $table de planilla
   * @return {Object} Item encontrado
   */
  getItemByEvent(config, evt, $table) {
    let item = null;
    let actualPosition = 0;
    let pos = this.eventToPosition(config, evt, $table);
    let position = pos.y;
    config.data.forEach(d => {
      if (d.isHidden) return;
      if (actualPosition == position ) {
        item = d;
      }
      actualPosition++;
      if (!d.isClosed) {
        d.children.forEach(c => {
          if (c.isHidden) return;
          if (actualPosition == position ) {
            item = c;
          }
          actualPosition++;
        });
      }
    });
    return item;
  },


  /**
   * Obtiene fecha por posicion de mouse
   * usa evento click en $table
   *
   * @param {Object} config - Configuracion capibara
   * @param {Object} evt - Evento mouse click
   * @param {Object} $table - Elemento de dom $table de planilla
   * @return {String} Fecha formateada correspondiente
   */
  getDayByEvent(config, evt, $table) {
    let pos = this.eventToPosition(config, evt, $table);
    let day = dateUtils.addDaysString(config.timeline.start, pos.x);
    return day;
  },


  /**
   * Obtiene item de config.data capibara, usando ID
   *
   * @param {Object} data - Configuracion Capibara
   * @param {Integer} id - Id a buscar
   * @param {String} [type] - tipo de item a buscar en config.data
   * @param {Boolean} [withParent] - Si se devuelve solo el item o con el parent
   * @return {Object} item o {item, parent}
   */
  getItemById(config, id, typeNode = "child", withParent = false) {
    let item = null;
    let parent = null;

    config.data.forEach(d => {
      if (d.id == id && typeNode == "parent") {
        item = d;
      }
      d.children.forEach(c => {
        if (c.id == id && typeNode == "child") {
          item = c;
          parent = d;
        }
      });
    });

    if (withParent) {
      return { item, parent }
    }
    return item;
  },


  /**
   * Obtiene posicion de mouse segun evento
   * con respecto a la planilla

   *   left : posicion desde izquierda en pixeles, en multiplos del ancho del dia,
   *   top  : posicion desde arriba en pixeles, como multiplo del alto del item,
   *   x    : posicion desde izquierda, en orden numerico,
   *   y    : posicion desde arriba, en orden numerico,
   *   _real : {
   *     topOffset  : differencia entre posicion izquierda de pantalla y de table,
   *     leftOffset : differencia entre posicion superior de pantalla y de table,
   *     left       : posicion en pixeles,
   *     top        : posicion en pixeles
   *   }
   *
   * @param {Object} config - configuracion capibara
   * @param {Object} ev - evento mouse
   * @param {Object} elem - Elemento dom Target, para usar en calculos
   * @return {Object} objeto con posiciones
   */
  eventToPosition(config, ev, $table) {
    //console.log("eventToPosition");
    let $container = $table.parentElement;
    let rectTable = this.tablePositions.boundingClientTable; //target.getBoundingClientRect();
    let rectContainer = this.tablePositions.boundingClientContainer; //target.getBoundingClientRect();
    let itemHeight = parseFloat(this.getCSSVariable(config, "--item-height"));
    let dayWidth = parseFloat(this.getCSSVariable(config, "--day-width"));
    let headerHeight = parseFloat(this.getCSSVariable(config, "--header-height"));
    let navWidth =
      parseFloat(this.getCSSVariable(config, "--nav-width")) +
      parseFloat(this.getCSSVariable(config, "--ex-nav-width")) +
      parseFloat(this.getCSSVariable(config, "--btn-nav-width")) +
      parseFloat(this.getCSSVariable(config, "--initial-nav-width"));

    let paddingLeft = parseInt($table.style.paddingLeft || 0);           //  + parseInt(elem.offsetLeft);
    let paddingTop = parseInt($table.style.paddingTop || 0);            // + parseInt(elem.offsetTop);

    let leftPosition = ev.clientX - paddingLeft - parseInt(rectTable.left);
    leftPosition = leftPosition >= 0 ? leftPosition : 0;

    let topPosition = ev.clientY - paddingTop - parseInt(rectTable.top);
    topPosition = topPosition >= 0 ? topPosition : 0;

    let leftRounded = leftPosition - Math.abs(leftPosition % dayWidth);
    let topRounded = topPosition - Math.abs(topPosition % itemHeight);

    let leftClient = leftPosition - this.tablePositions.scrollLeft;
    let topClient = topPosition - this.tablePositions.scrollTop;
    let visibleWidth = rectContainer.width - navWidth;
    let visibleHeight = rectContainer.height - headerHeight;

    let position = {
      leftClient,      // left en pixeles, relativo al sector visible
      topClient,       // top en pixeles, relativo al sector visible
      leftPosition,    // left en pixeles, relativo al elemento
      topPosition,     // top en pixeles, relativo al elemento
      left: leftRounded,              // left en pixeles, relativo al elemento, redondeado
      top: topRounded,               // top en pixeles, relativo al elemento, redondeado
      x: leftRounded / dayWidth,   // left en posicion en dias
      y: topRounded / itemHeight,  // top en posicion de items
      yPercent: _.round((topClient / visibleHeight) * 100, 2),   // left en porcentaje, relativo al sector visible
      xPercent: _.round((leftClient / visibleWidth) * 100, 2),   // top en porcentaje, relativo al sector visible
      yInnerPercent: _.round((topPosition / (this.tablePositions.scrollHeight - headerHeight)) * 100, 2),   // left en porcentaje, relativo al sector visible
      xInnerPercent: _.round((leftPosition / (this.tablePositions.scrollWidth - navWidth)) * 100, 2),   // top en porcentaje, relativo al sector visible

      /*
        visibleWidth,
        visibleHeight,
        navWidth,
        _real    : {
            topOffset  : paddingTop + parseInt(rectTable.top),
            leftOffset : paddingLeft + parseInt(rectTable.left),
            left       : leftPosition,
            top        : topPosition
        }
        */
    }
    //console.table(position);
    //console.log(JSON.stringify(position));
    return position;
  },


  eventToPositionNavLeft(config, ev, elem) {
    let target = elem || ev.currentTarget;
    let rect = elem.getBoundingClientRect();
    let rectParent = elem.parentElement.getBoundingClientRect();
    let itemHeight = parseFloat(this.getCSSVariable(config, "--item-height"));
    let headerHeight = parseFloat(this.getCSSVariable(config, "--header-height"));

    let paddingTop = parseInt(target.style.paddingTop || 0);
    let topPosition = ev.clientY - paddingTop - parseInt(rect.top);
    topPosition = topPosition >= 0 ? topPosition : 0;
    let topRounded = topPosition - Math.abs(topPosition % itemHeight);
    let topClient = topPosition - elem.parentElement.scrollTop;
    let position = {
      topClient, // top en pixeles, relativo al sector visible
      top: topRounded,                                    // top en pixeles, relativo al elemento
      y: topRounded / itemHeight,                       // top en posicion de items
      yPercent: _.round((topClient / (rectParent.height - headerHeight)) * 100, 2),   // top en porcentaje, relativo al sector visible
    }
    return position;
  },




  getPositionByItemDate(config, item, date) {

    let h = parseFloat(this.getCSSVariable(config, "--item-height"));
    let w = parseFloat(this.getCSSVariable(config, "--day-width"));
    let y = this.getPositionByItem(config, item);
    let x = dateUtils.diffDays(config.timeline.start, date);
    let left = x * w;
    let top = y * h;

    let position = {
      left,
      top,
      x,
      y
    }

    return position;
  },



  /**
   * Obtiene promedio de datos, de una lista de datos
   * entre una fecha y los dias indicados, hacia el pasado
   *
   * si cuenta 0, devuelve null
   *
   * @param {Object} data - Datos para buscar
   * @param {Date} date - fecha de busqueda
   * @param {Integer} days - diferencia de dias
   * @return {Float} promedio de valores
   */
  getAverage(data, date, days) {
    let total = 0;
    let count = 0;
    let dataSorted = data.sort((a, b) => b.unix - a.unix);
    let from = new Date(date);
    let to = new Date(date);
    from.setDate(from.getDate() - days);

    dataSorted.forEach(d => {
      let dDate = new Date(d.date);
      if (from < dDate && dDate < to) {
        let value = parseFloat(d.value);
        if (!isNaN(value) && value > 0) {
          total += value;
          count++;
        } else {
          from.setDate(from.getDate() - 1);
        }
      }
    });
    return this.round(count > 0 ? total / count : 0, 2);
  },




  /**
   * Meses por posicion, en español
   *
   * @param {Integer} pos - Posicion de mes
   * @return {String} Nombre de mes
   */
  getMonthByPosition(pos) {
    let months = [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'];
    return months[pos];
  },

  /**
   * Redondea numeros
   * @param {Float} num - numero decimal para redondear
   * @param {Integer}  decimal - cantidad de decimales para redondear
   * @return {Float} Numero redondeado
   */
  round(num, decimal = 2) {
    //let base10 = 10 * decimal;
    //return base10 ? Math.round(num * base10) / base10 : Math.round(num);
    return _.round(num, decimal);
  },


  /**
   * Edita variable CSS desde Root
   * @param {String} varName - nombre de variable CSS
   * @param {Any} value - valor a asignar a variable CSS
   */

  setCSSVariable(config, varName, value) {
    let containerElement = document.querySelector(config.selector);
    if (containerElement) {
      containerElement.style.setProperty(varName, value);
    }
  },

  /**
   * Obtiene valor de variable CSS desde Root
   * @param {String} varName - nombre de variable CSS
   * @return {Float} Numero redondeado
   */

  getCSSVariable(config, varName) {
    let containerElement = document.querySelector(config.selector);
    return containerElement ? getComputedStyle(containerElement).getPropertyValue(varName) : null;
  },


  /**
   * Verifica si la pantalla tiene un tamaño mobile
   * modifica this.isMobile
   * breakpoint 960e
   * 
   * @return { Boolean } - si el tamaño de pantalla es menor al breakpoint
   */
  checkIsMobile() {
    let breakPoint = 960;
    return Boolean(window.matchMedia(
      "(max-width: " + (breakPoint - 1) + "px)"
    ).matches);
  },


  getTableDimensions(config) {
    let h = parseFloat(this.getCSSVariable(config, "--item-height"));
    let w = parseFloat(this.getCSSVariable(config, "--day-width"));
    let start = new Date(config.timeline.start);
    let end = new Date(config.timeline.end);
    let rows = 0;

    end.setTime(end.getTime() + dateUtils.dayInMilliseconds);// add 24 hours

    let diff_time = end.getTime() - start.getTime();
    let days = diff_time / dateUtils.dayInMilliseconds;

    config.data.forEach(d => {
      if (d.isHidden) return;
      rows++;
      if (d.children && !d.isClosed) {
        rows += d.children.filter(c => !c.isHidden).length;
      }
    });

    let height = rows * h;
    let width = days * w;

    return {
      height,
      width,
      dayWidth: w,
      itemHeight: h
    }

  },

  //https://stackoverflow.com/questions/8027423/how-to-check-if-a-string-is-a-valid-hex-color-representation
  isHexColor(str) {
    let reg = /^#([0-9a-f]{3}){1,2}$/i;
    return reg.test(str)
  },

  toCapitalize(str) {
    return (" " + str).toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
  },

  formatNumber(num) {
    return new Intl.NumberFormat("es-CL").format(num).replace(".", " ");
  },


  //https://stackoverflow.com/questions/35228052/debounce-function-implemented-with-promises
  debounce(f, interval) {
    let timer = null;
    return (...args) => {
      clearTimeout(timer);
      return new Promise((resolve) => {
        timer = setTimeout(() => resolve(f(...args)), interval);
      });
    };
  },

  //https://stackoverflow.com/questions/27078285/simple-throttle-in-javascript/27078401#27078401
  throttle(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function () {
      previous = options.leading === false ? 0 : Date.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };
    return function () {
      var now = Date.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  }

}
