export default {


  /**
   * Crea elemento Dom, con opciones de :
   * - classNames
   * - text
   * - style
   * - attributes
   *
   * @param {String} [tag = "div"] - tipo de tag, default DIV
   * @param {Object} [options = {}] - opciones 
   * @return {[TODO:type]} [TODO:description]
   */
  createElement(tag = "div", options = {}) {
    var e = document.createElement(tag);
    if (options.className) {
      e.classList.add(options.className);
    }
    if (options.classNames && options.classNames.length > 0) {
      options.classNames.forEach(c => {
        if (c !== "") {
          e.classList.add(c);
        }
      })
    }

    if (options.attributes) {
      options.attributes.forEach(attr => {
        e.setAttribute(attr.name, attr.value);
      });
    }

    if (options.text) {
      e.appendChild(document.createTextNode(options.text));
    }

    if (options.style) {
      Object.assign(e.style, options.style);
    }

    return e;
  },

  /**
   * Crea elemento Dom desde String
   *
   * Basado en 
   * https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro
   *
   * @param {String} [htmlString = ""] - String a evaluar
   * @return {Object} elemento Dom creado
   */
  createElementFromHTML(htmlString = "") {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    // Change this to div.childNodes to support multiple top-level nodes
    return div.firstChild;
  },

}
