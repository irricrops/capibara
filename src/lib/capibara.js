import dom           from '../util/domUtils';
import utils         from '../util/utils';
//import dateUtils     from '../util/dateUtils';
import tableElements from '../util/tableElements';
import styleUtils    from '../util/styleUtils';

/**
 * Capibara, planilla para despliegue de datos por fecha como GANTT
 * orientado inicialmente a datos diarios de items
 *
 * @author Patricio. M. Rojas. <patricio@irricrops.com>
 */
export class Capibara{


  /**
   * @constructor
   * Instancia variables de planilla Capibara, con configuracion
   * Crea el elemento contenedor {containerElement} segun selector
   *
   * @param {Object} attributes - atributos que se usaran al iniciar la planilla
   * @param {Object} attributes.config - Configuracion de planilla
   */
  constructor({config}){
    if(config == null) console.error("config no econtrado");

    this.containerElement = document.querySelector(config.selector);

    if(this.containerElement == null) console.error("Elemento dom segun config.selector, no encontrado");

    this.initialized              = false;
    this.$capibara                = null;
    this.$elements                = {};
    this.tabSelected              = null;
    this.$inputsNavRight          = [];
    this.$inputsWithReload        = [];
    this.$saveCancelBtns          = [];
    this.selected                 = [];
    this.config                   = config;
    this.lastDataDay              = null;
    this.isShowNavExtended        = false;
    this.isShowNavInitial         = true;
    this.isShowNavButtons         = true;
    this.isMobile                 = false;
    this.lastMousePosition        = null;
    this.isVisibleContextMenu     = false;
    this.isDisabledInputsNavRight = false;

  }






  /**
   * Inicializa Planilla:
   *  - vacia variables necesarias
   *  - crea planilla llamando a drawTable
   *  - Agrega eventos a planilla
   *  - mueve scroll al medio de la tabla
   */
  init(){

    this.tabSelected          = null;
    this.$capibara            = null;
    this.$elements            = {};
    this.$inputsNavRight      = [];
    this.$inputsWithReload    = [];
    this.$saveCancelBtns      = [];
    this.selected             = [];
    this.lastDataDay          = null;
    this.isMobile             = utils.checkIsMobile();
    this.isVisibleContextMenu = false;

    this.setAllCSSVariables();
    this.drawTable();
    this.addEvents();
    this.scrollToMiddle();
    this.toggleSidebarMobile();
    this.initialized = true;
  }



  /**
   * Cambia configuracion de planilla
   *
   * @param {Object} config - Configuracion nueva para planilla
   */
  setConfig(config){
    this.config = config;
  }


  /**
   * Crea la planilla
   * agregando todos los elementos en orden dentro
   *
   * @return { DomElement } $capibara - elemento del Dom que contiene toda la planilla
   */
  drawTable(){

    let config        = this.config
    let initialTab    = config.timeline.tabs[0];
    let firstChildren = {};

    if(config.data  && config.data.length > 0 && config.data[0].children.length > 0){
      firstChildren = config.data[0].children[0];
    }

    let $capibara                = tableElements.createAgTable(config);
    let $corner                  = tableElements.createCorner(config, initialTab);
    let $cornerRight             = tableElements.createCornerRight(config);
    let $headerTop               = tableElements.createHeaderTop(config);
    let $header                  = tableElements.createHeader(config);
    let $todayLine               = tableElements.createTodayLine(config);
    let $headerExtended          = tableElements.createHeaderExtended(config);
    let $navLeft                 = tableElements.createNavLeft(config)
    let $cornerFooter            = tableElements.createCornerFooter(config);
    let $footer                  = tableElements.createFooter(config);
    let $floatBoxContainer       = tableElements.createFloatBox(config, initialTab);
    let $selectContainer         = tableElements.createSelectBoard(config);
    let $plotVerticalContainer   = tableElements.createPlotVerticalContainer(config);
    let $plotHorizontalContainer = tableElements.createPlotHorizontalContainer(config);
    let $detailDay               = tableElements.createDetailDay(config, firstChildren);

    let { elem:$contextMenu, isVisible }  = tableElements.createContextMenu({config});
    this.isVisibleContextMenu = isVisible;

    let $navRight, inputs, inputsWithReload, saveCancelBtns;
    ({element:$navRight, inputs, inputsWithReload, saveCancelBtns} = tableElements.createNavRight(config));

    let $background      = tableElements.createBackground(config);
    let $container       = dom.createElement("div", { className:"container-tl"});
    let $dayMouseOver    = dom.createElement("div", { className: "day-mouse-over"});

    let {height, width } = utils.getTableDimensions(config);
    let $table           = dom.createElement("div", { className: "table", style:{
      'width': width+"px",
      'height': height+"px"
    }});

    $table.appendChild($background);
    $table.appendChild($dayMouseOver);
    $table.appendChild($detailDay);
    $table.appendChild($selectContainer);
    $table.appendChild($floatBoxContainer );

    if($headerExtended){
      $header.appendChild($headerExtended);
    }

    $container.appendChild($header);
    $container.appendChild($plotVerticalContainer);
    $container.appendChild($plotHorizontalContainer);
    $container.appendChild($todayLine);
    $container.appendChild($navLeft);
    $container.appendChild($table);

    if($cornerFooter) $container.appendChild($cornerFooter);

    if($footer) $container.appendChild($footer);

    $capibara.appendChild($headerTop);
    $capibara.appendChild($corner);
    $capibara.appendChild($cornerRight);
    $capibara.appendChild($container);
    $capibara.appendChild($navRight);
    if($contextMenu) $capibara.appendChild($contextMenu);


    this.$inputsNavRight   = inputs;
    this.$inputsWithReload = inputsWithReload;
    this.$saveCancelBtns = saveCancelBtns;
    this.$capibara = $capibara;
    this.$elements = {
      $headerTop,
      $detailDay,
      $contextMenu,
      $floatBoxContainer,
      $selectContainer,
      $table,
      $background,
      $plotVerticalContainer,
      $plotHorizontalContainer,
      $dayMouseOver,
      $container,
      $navRight,
      $navLeft,
      $header,
      $todayLine,
      $corner,
      $cornerRight
    }

    this.containerElement.innerHTML = "";
    this.containerElement.appendChild($capibara);
    this.selectTab(initialTab);

    return $capibara;
  }







  /**
   * Cambia el modo oscuro, segun el estado actual
   */
  toggleDark(){
    this.setDark(!Boolean(this.config.timeline.dark));
  }


  /**
   * Cambia el modo oscuro, segun parametro
   *
   * @param {Boolean} [isDark = false] - indica si se activa el modo oscuro o no
   */
  setDark( isDark = false){
    let darkClassName = "dark-theme";
    this.config.timeline.dark = isDark;

    if(isDark){
      this.$capibara.classList.add(darkClassName);
    } else {
      this.$capibara.classList.remove(darkClassName);
    }
  }



  /**
   * Cambia el tema de color secundario, segun parametro
   *  -Quita todos los posibles colores de la planilla, obteniendolos de Tabs
   *  -Agrega el color enviado por parametro como clase del domElement $capibara
   *
   * @param {String} color - color, como nombre de clase, para agregar 
   */
  changeColorTheme(color){
    this.config.timeline.tabs.forEach(tab => {
      this.$capibara.classList.remove(tab.color);
    });
    this.$capibara.classList.add(color);
  }



  /**
   * Selecciona tab en planilla
   * si redraw == true , redibuja navRight y floatBox
   * ya que son inputs y esteticas personalizadas por Tab
   *
   * @param {Object} tab - tab, obtenida desde configuracion
   * @param {Boolean} [redraw=true] - opcion de redibujado
   */
  selectTab(tab, redraw = true){
    if(tab){
      this.tabSelected = tab;

      this.cleanMultiInput();

      if(redraw){
        this.redraw({
          headerTop:true,
          cornerLeft: true,
          navLeft: true,
          navRight: true,
          floatBox:true
        })
      }

      this.changeColorTheme(tab.color);

      this.$capibara.dispatchEvent(new CustomEvent("selectTab", {
        bubbles: true,
        cancelable: false,
        detail:{
          tabSelected : this.tabSelected
        }
      }));

    }else{
      console.error("tab no encontrada");
    }
  }

  selectLegend(evt){
    let legend = evt.detail.legend;
    evt.detail.$legend.classList.toggle("active");
    if(legend){
      legend.active = !legend.active;
    }

    this.redraw({
      floatBox: true,
      detailDay: true
    });
  }

  /**
   * muestra alternadamente los sidebar initial y extended
   */
  toggleSidebar(){
    this.setShowNavInitial(!this.isShowNavInitial);
    if(!this.isMobile){
      this.setShowNavExtended(!this.isShowNavExtended);
    }else{
      let cornerInputTitles = this.$elements.$corner.querySelector(".input-titles");
      if(cornerInputTitles){
        cornerInputTitles.style.display = this.isShowNavInitial ? "none": "flex";
      }
    }
    this.refreshScrollUtilsVars();
  }

  /**
   * Oculta nav initial , extended y buttons
   */
  hideSidebars(){
    this.setShowNavInitial(false);
    this.setShowNavExtended(false);
    this.setShowNavButtons(false);
  }


  /**
   * Muestra/Oculta Sidebar inicial
   */
  setShowNavInitial(isShowNavInitial){
    this.isShowNavInitial  = isShowNavInitial;
    styleUtils.setNavLeftInitialStyle(this.config, this.isShowNavInitial, this.tabSelected);
  }

  /**
   * Muestra/Oculta Sidebar extendido
   */
  setShowNavExtended(isShowNavExtended){
    this.isShowNavExtended = isShowNavExtended;
    styleUtils.setNavLeftExtendedStyle(this.config, this.isShowNavExtended, this.tabSelected);
  }

  /**
   * Muestra/Oculta Sidebar buttons
   */
  setShowNavButtons(isShowNavButtons){
    this.isShowNavButtons = isShowNavButtons;
    styleUtils.setNavLeftButtonsStyle(this.config, this.isShowNavButtons, this.tabSelected);
  }



  /**
   * Muestra/oculta Sidebar Derecho
   */
  toggleSidebarRight(){
    let isHidden = !this.$capibara.classList.contains("extended-right");
    this.setVisibleSidebarRight(!isHidden);
    this.refreshScrollUtilsVars();
  }

  /**
   * Modifica la opcion de mostrar y ocultar sidebar derecho
   *
   * @param  {Boolean} [show = true] - indica si debe mostrarse u ocultarse
   */
  setVisibleSidebarRight(show = true){
    let fn = show ? "add" : "remove";
    this.$capibara.classList[fn]("extended-right");
  }


  /**
   * Muestra/oculta Header extendido (inputs)
   */
  toggleHeader(){
    this.$capibara.classList.toggle("header-extended");
    this.refreshScrollUtilsVars();
    this.refreshVirtualScrollVars();
  }



  /**
   * Muestra/oculta barra de carga de planilla 
   * Si se llama sin parametros, por defecto muestra el loading derecho
   * que corresponde al sidebar-right
   *
   * @param {String} [position = "right"] - escoge loading, puede ser "left" | "right"
   * @param {Boolean} [show = true] - indica si mostrar u ocultar loading
   */
  loading(position = "right", show = true){
    let fn      = show ? "remove" : "add";
    let element = this.$capibara.querySelector(".progress-bar-"+position);
    element.classList[fn]("hide-progress");
  }



  setDisableInputsNavRight(isDisabled = true){
    this.isDisabledInputsNavRight = isDisabled;
    
    this.$inputsNavRight.forEach($inputContainer => {
      let $inputs = $inputContainer.querySelectorAll("input");
      $inputs.forEach($i => {
        $i.setAttribute("disabled", isDisabled);
      });
    });
    
    let btns = this.$saveCancelBtns.querySelectorAll("button");

    btns.forEach(btn => {
        btn.setAttribute("disabled", isDisabled);
    });
  }


  /**
   * Busca un boton segun selector, y le agrega la clase "loading"
   * y deshabilita el boton
   *
   * @param { String } [ selector = '[cap-btn-calendar]' ] - selector de boton
   * @param { Boolean } [isLoading  = true] - define si esta cargando o no
   */
  loadingBtn(selector= "[cap-btn-calendar]", isLoading = true){
    let element = this.$capibara.querySelector(selector);
    if(element){
      let fn = isLoading ? "add" : "remove";
      element.classList[fn]("loading");
      if(isLoading){
        element.setAttribute("disabled","");
      }else{
        element.removeAttribute("disabled");
      }
    }
  }




  /**
   * Crea Evento de guardado de los inputs del Sidebar Right
   * obtiene todos los valores de los inputs registrados en this.$inputs y 
   * los retorna creando el evento "save"
   *
   */
  saveInputsNavRight(){

    let inputValues = [];

    let isValid = this.validateInputsNavRight();

    if(!isValid){
      return;
    }

    this.setDisableInputsNavRight(true);

    this.$inputsNavRight.forEach($input => {
      let key = $input.getAttribute("input-key");
      let inputConfig = this.tabSelected.inputs.find(t => t.key == key);
      if(inputConfig){
        inputValues.push({
          key         : key,
          inputConfig : inputConfig,
          el          : $input,
          value       : this.getInputValue($input, inputConfig)
        });
      }
    });


    this.$capibara.dispatchEvent(new CustomEvent("save", {
      bubbles: true,
      cancelable: false,
      detail:{
        inputValues : inputValues,
        selected    : this.selected,
        tabSelected : this.tabSelected
      }
    }));


  }

  validateInputsNavRight(){
    let isFormValid = true;
    this.$inputsNavRight.forEach($input => {
      let isInputValid = true;
      let key = $input.getAttribute("input-key");
      let inputConfig = this.tabSelected.inputs.find(t => t.key == key);
      if(inputConfig){
        let value = this.getInputValue($input, inputConfig);
        if(inputConfig.required){
          if(value === "" || value === null || value.length == 0){
            isFormValid = false;
            isInputValid = false;
          }
        }

        if(inputConfig.validation && !inputConfig.validation({value , inputConfig, $input}) ){
          isFormValid = false;
          isInputValid = false;
        }

        if(!isInputValid){
          $input.classList.add("is-invalid");
        }else{
          $input.classList.remove("is-invalid");
        }

      }
    });

    return isFormValid ;
  }


  /**
   * Cancela formulario de sidebar right
   */
  cancelMultiInput(){
    this.$capibara.dispatchEvent(new CustomEvent("cancel", {
      bubbles: true,
      cancelable: false,
      detail:{
        selected    : this.selected,
        tabSelected : this.tabSelected
      }
    }));
    this.cleanMultiInput();
  }



  /**
   * Limpia el formulario en el sidebar right
   * oculta el sidebar right
   */
  cleanMultiInput(){
    this.$capibara.classList.remove("extended-right");
    this.selected = [];
    this.redrawSelection();
  }





  /**
   * Obtiene el valor de un input
   *
   * @param {Object} elem - elemento del dom que representa al input, no necesariamente input
   * @param {Object} config - configuracion de input
   * @return {*} valor del Input
   */
  getInputValue(elem, inputConfig){
    let value         = null;
    let inputElement  = null;
    let inputsElement = null;

    switch (inputConfig.type) {

      case 'custom':
        value = inputConfig.getValue ? inputConfig.getValue({elem, props: inputConfig}) : null;
      break;

      case 'time':
        let hhElement= elem.querySelector(".hour");
        let mmElement= elem.querySelector(".minutes");
        let hhValue = hhElement.value;
        let mmValue = mmElement.value;
        if(hhValue){
          hhValue = parseFloat(hhValue);
        }
        if(mmValue){
          mmValue = parseFloat(mmValue);
        }
        value = {hh:hhValue, mm: mmValue};
        break;

      case 'message':
      case 'textarea':
        inputElement = elem.querySelector("textarea");
        value = inputElement.value || '';
        break;

      case "inputList":
        let valuesInputs = [];
        inputsElement = elem.querySelectorAll("input[type='number']");
        inputsElement.forEach(inputElement => {
          let valueInput = inputElement.value;
          if(valueInput){
            valueInput = parseFloat(valueInput);
          }
          let label = inputElement.getAttribute("input-label");
          let id = inputElement.getAttribute("input-id");
          valuesInputs.push({label:label, value:valueInput , id: id});
        })
        value = valuesInputs;
        break;

      case 'number':
      case 'float':
        inputElement = elem.querySelector("input[type='number']");
        value = inputElement.value;
        if(value){
          value = parseFloat(value);
        }
        break;

      case 'checkbox':
        inputElement = elem.querySelector("input[type='checkbox']");
        value = inputElement.checked;
        break;

      case 'select':
        inputElement = elem.querySelector("select.input-select");
        let inputValue = inputElement.value;
        value = inputConfig.options.find(o => o.id == inputValue)
        break;

      case 'file':
        inputElement = elem.querySelector("input[type='file']");
        value = inputElement.files;
        break;

      case 'input-box':
        inputsElement = elem.querySelectorAll(".mini-input-section input");
        value = [];
        inputsElement.forEach(i => {
          let v = i.value;
          if(v){
            v = parseFloat(v);
          }
          value.push({
            id    : i.getAttribute("input-id"),
            key   : i.getAttribute("input-key"),
            value : v,
          })
        });
        break;
      case 'data-list':	
        inputsElement = elem.querySelectorAll(".mini-input-section");
        value = [];
        inputsElement.forEach(i => {
          let v = i.getAttribute("data-value")
          if(v){
            v = parseFloat(v);
          }
          value.push({
            id    : i.getAttribute("data-id"),
            key   : i.getAttribute("data-key"),
            value : v || null,
          })
        });
        break;
      case 'title':
        //no es realmente un input, es solo un texto
        //no es necesario obtener valores de aqui
        break;

      case 'text':
      default:
        inputElement = elem.querySelector("input");
        if(inputElement){
          value = inputElement.value;
        }
    }

    return value;
  }








  /**
   * Funcion llamada al pasar sobre un item del sidebar izquierdo, 
   * para mostrar un detalle del item
   * 
   * El detalle del item se ancla de manera superior o inferior dependiendo de 
   * la posicion el mouse en pantalla, para no salir del campo visual 
   *
   *
   * @param {Object} evt -  Evento retornado por un mouseover
   */
  overNavItem(evt){
    let pos     = evt.detail.pos;
    let item    = evt.detail.item;
    //let dataDay = evt.detail.data;
    let $detail = this.$elements.$navLeft.querySelector(".detail-nav-item");

    if($detail.classList.contains("hide")){
      $detail.classList.remove("hide");
    };

    $detail.innerHTML = tableElements.getDetailNavItemContent(item);

    if(pos.yPercent > 50){ //mayor a 50%
      if(!$detail.classList.contains("to-bottom")){
        $detail.classList.add("to-bottom");
      }
    }else{
      if($detail.classList.contains("to-bottom")){
        $detail.classList.remove("to-bottom");
      }
    }

    $detail.style.top = pos.top+"px";
  }


  /**
   * Oculta detalle de item en sidebar izquierdo 
   *
   * @param {Object} evt -  Evento retornado por un mouseover
   */
  leaveNavItem(){
    this.$elements.$navLeft.querySelector(".detail-nav-item").classList.add("hide");
  }


  /**
   * Captura evento mousemove de floatBoxContainer
   * para refrescar la posicion y detener posteriores eventos
   *
   * @param {Object} evt - Evento de mousemove
   */
  mouseMoveOnFlexBoxContainer(evt){
    let position = utils.eventToPosition(this.config, evt, this.$elements.$table);
    this.refreshDetailDayPosition(position);
    evt.preventDefault();
  }

  /**
   * Captura evento mouseleave de floatBoxContainer
   * para ocultar detailDay
   *
   * @param {Object} evt - Evento de mouseleave
   */
  mouseLeaveOnFloatBoxContainer(evt){
    this.hideDetailDay();
    evt.preventDefault();
  }

  /**
   * Maneja el clic fuera del div específico
   * @param {Event} event - El evento de clic
   */
  handleClickOutside(event) {
    if (!this.isMobile && this.$elements && this.$elements.$detailDay && !this.$elements.$detailDay.contains(event.target)) {
      this.hideDetailDay();
    }
  }


  /**
   * Captura el evento mousemove en $table, para indicar la posicion de $dayMouseOver
   *
   * @param {Object} evt - Evento de mouseleave
   */
  mouseMoveOnTable(evt){
    if(this.isVisibleContextMenu) return;
    let pos = utils.eventToPosition(this.config, evt, this.$elements.$table);
    if(this.lastMousePosition == null || 
      this.lastMousePosition.left != pos.left ||
      this.lastMousePosition.top != pos.top
    ){
      this.$elements.$dayMouseOver.style.transform = "translate("+pos.left+"px ,"+pos.top+"px)";
      evt.preventDefault();
      this.lastMousePosition = pos;
    }
  };



  rightClickOnCapibara(evt){
    if(this.config.timeline.contextMenu){
      this.hideDetailDay();
      evt.preventDefault();
    }
  };

  rightClickOnTable(evt){
    if(this.config.timeline.contextMenu){
      this.hideDetailDay();
      this.showContextMenuOnTable(evt);
      evt.preventDefault();
    }
  };

  showContextMenuOnTable(evt){

    let date  = utils.getDayByEvent(this.config, evt, this.$elements.$table);
    let item  = utils.getItemByEvent(this.config, evt, this.$elements.$table);

    if(this.isVisibleContextMenu) {
      this.hideContextMenu();
      return;
    }

    //let position = utils.eventToPosition(this.config, evt, this.$elements.$table);
    let { elem:$elem, isVisible } = tableElements.createContextMenu({
      tabSelected: this.tabSelected,
      $capibaraElem : this.$capibara,
      config: this.config,
      evt,
      //position,
      date,
      item,
      selected: this.selected,
      name: "onTable"
    });

    if($elem){
      this.isVisibleContextMenu = isVisible;
      this.$elements.$contextMenu.replaceWith($elem);
      this.$elements.$contextMenu = $elem;
    }
    if(isVisible){
      evt.preventDefault();
    }
   }

  /**
   * Captura el evento click en $table, para seleccionar dias
   * los dias seleccionados se registran en this.selected[]
   *
   * @param {Object} evt - Evento de mouseleave
   */
  clickOnTable(evt){

    let date  = utils.getDayByEvent(this.config, evt, this.$elements.$table);
    let pos   = utils.eventToPosition(this.config, evt, this.$elements.$table);
    let item  = utils.getItemByEvent(this.config, evt, this.$elements.$table);

    //if(item == null) return; // cuano se selecciona un espacio no seleccionable

    //si es mobile, no selecciona, solo muestra detalles
    let dataByDate = _.find(item.data, {date});
    if(this.isMobile && dataByDate){
      if((!dataByDate.message || dataByDate.message.value == null) && Object.entries(dataByDate.values).length === 0 ) return;
      this.focusOnFloatBox({pos, evt});
      return;
    }

    if(!this.isSelectable({item, date, dataByDate})){
      this.$capibara.dispatchEvent(new CustomEvent("selectNoSelectable", {
        bubbles: true,
        cancelable: false,
        detail:{
          selected    : this.selected,
          tabSelected : this.tabSelected,
          lastSelection : {date, item, pos},
        }
      }));
      return; // cuando se selecciona un espacio no seleccionable
    } 

    let eventName = "deselectDay";

    if(this.toggleSelect(date, item, pos)){
      eventName = "selectDay";
    }

    this.$capibara.dispatchEvent(new CustomEvent(eventName, {
      bubbles: true,
      cancelable: false,
      detail:{
        config: this.config,
        lastSelection : {date, item, pos},
        selected    : this.selected,
        tabSelected : this.tabSelected
      }
    }));

  };





  toggleSelect(date, item, pos){
    if(this.isSelected(date, item)){
      this.deselect(date, item);
      return false;
    }else{
      this.select(date, item, pos);
      return true;
    }
  };





  isSelected(date, item){
    let index = utils.indexSelectedDay(this.selected, {date, item});
    return index > -1;
  };



  isSelectable({item, date, dataByDate}){

    if(!this.config.timeline.selectable){
      return false;
    }

    if(item.selectable === false){
      return false;
    }

    if(this.config.timeline.noFutureSelectable){
      let d = new Date(date);
      let today = new Date();
      if(d > today){
        return false;
      }
    }

    let itemIsSelectable = true;
    if(this.tabSelected && this.tabSelected.selectableIfHas){
      if(!item.values[this.tabSelected.selectableIfHas]){
        itemIsSelectable = false;
      }
    }
    return itemIsSelectable;
  };

  redrawSelection(){
      this.checkClassSidebarExtendedRight();
      tableElements.redrawSelectBoard(this.$elements.$selectContainer, this.selected);
  };






  deselect(date, item, redraw = true){
    if(this.isSelected(date, item)  ){
      let index = utils.indexSelectedDay(this.selected, {date, item});
      this.selected.splice(index, 1);
    }

    this.setIndexToSelected();

    if(redraw){
      this.redrawSelection()
    }
  };







  select(date, item, pos, redraw = true, refreshForm = true){
    if(!this.isSelected(date, item)){

      this.selected.push({
        index: 0,
        date,
        item,
        pos,
      });

      let dataDay = null;
      if(item.data){
        dataDay = item.data.find(d => {
          return d.date == date;
        });
      }

      // si es el primer item seleccionado, se autorrellena el formulario de navRight
      if(this.selected.length == 1 && refreshForm){
        this.redraw({
          cornerRight: true,
        });
        let {inputs, element, inputsWithReload, saveCancelBtns} = tableElements.redrawNavRight(this.config, this.$elements.$navRight, this.selected, this.tabSelected, dataDay, this.selected[0]);
        this.$elements.$navRight = element;
        this.$inputsNavRight = inputs;
        this.$inputsWithReload = inputsWithReload;
        this.$saveCancelBtns = saveCancelBtns;
      }

    }


    let focused = false;
    this.$inputsNavRight.forEach(inputNavRight => {
      if(!focused){
        let validInputs = inputNavRight.querySelectorAll("input");
        if(validInputs && validInputs.length > 0){
          let firstValidInput = _.first(validInputs);

          window.setTimeout(function () { 
            firstValidInput.focus();
          }, 0);

          focused = true;
        }
      }
    })

    this.setIndexToSelected();

    if(redraw){
      this.redrawSelection()
    }
  };


  setIndexToSelected(){
    this.selected.forEach((s, index) => {
      s.index = index+1;
    })
  };


  isSelectedAllDate(date){
    let isAllSelected = true;
    this.config.data.forEach(parent => {
      parent.children.forEach(item => {
        if(!this.isSelected(date, item)){
          isAllSelected = false;
        }
      });
    });
    return isAllSelected;
  };

  selectByDate(date){
    this.config.data.forEach(parent => {
      parent.children.forEach(item => {
        this.selectByItemDate(date, item, false);
      });
    });
  };

  deselectByDate(date){
    this.config.data.forEach(parent => {
      parent.children.forEach(item => {
        this.deselectByItemDate(date, item, false);
      });
    });
  };

  selectByItemDate(date, item, redraw = true, refreshForm = true){
    if(!this.isSelected(date, item)){
      let pos = utils.getPositionByItemDate(this.config, item, date);
      this.select(date, item, pos, redraw, refreshForm);
    }
  };


  deselectByItemDate(date, item, redraw = true){
    if(this.isSelected(date, item)){
      this.deselect(date, item, redraw);
    }
  };

  deselectAll(redraw = false){
    this.selected = [];
    if(redraw){
      this.redrawSelection();
    }
  };






  checkClassSidebarExtendedRight(){
    if(this.selected.length > 0){
      this.$capibara.classList.add("extended-right");
    }else{
      this.$capibara.classList.remove("extended-right");
    }

  };




  /**
   * oculta detailDay en el dom, usando la clase dd-hide
   */
  hideDetailDay(){
    this.$elements.$detailDay.classList.add("dd-hide");
  };

  hideContextMenu(){
    if(this.$elements.$contextMenu){
      this.$elements.$contextMenu.classList.add("cm-hide");
    }
    this.isVisibleContextMenu = false;
  };


  refreshScrollUtilsVars(){
    utils.tablePositions.scrollLeft = this.$elements.$container.scrollLeft;
    utils.tablePositions.scrollTop = this.$elements.$container.scrollTop;

    utils.tablePositions.scrollWidth = this.$elements.$container.scrollWidth;
    utils.tablePositions.scrollHeight = this.$elements.$container.scrollHeight;

    utils.tablePositions.boundingClientTable = this.$elements.$table.getBoundingClientRect();
    utils.tablePositions.boundingClientContainer = this.$elements.$container.getBoundingClientRect();
  }

  refreshVirtualScrollVars(){
    utils.tablePositions.containerWidth = this.$elements.$container.clientWidth;
    utils.tablePositions.containerHeight = this.$elements.$container.clientHeight;
    utils.tablePositions.navWidth = this.$elements.$navLeft.clientWidth;
    utils.tablePositions.headerHeight = this.$elements.$header.clientHeight;
    utils.tablePositions.dayWidth = parseFloat(utils.getCSSVariable(this.config, "--day-width"));
    utils.tablePositions.itemHeight = parseFloat(utils.getCSSVariable(this.config, "--item-height"));
  }

  refreshVirtualScroll(){
    let $floatBoxList = this.$elements.$floatBoxContainer.querySelectorAll(".float-box");
    $floatBoxList.forEach(f =>{
      if(this.isVisibleFloatBoxElement(f)){
        f.setAttribute("data-is-visible", true);
      }else{
        f.setAttribute("data-is-visible", false);
      }
    })
  }

  isVisibleFloatBoxElement(f){
    let containerWidth = utils.tablePositions.containerWidth;
    let containerHeight = utils.tablePositions.containerHeight;
    let navWidth = utils.tablePositions.navWidth;
    let headerHeight = utils.tablePositions.headerHeight;
    let dayWidth = utils.tablePositions.dayWidth;
    let itemHeight = utils.tablePositions.itemHeight;
    let scrollLeft = utils.tablePositions.scrollLeft;
    let scrollTop = utils.tablePositions.scrollTop;

    let margin = dayWidth;
    let left = parseFloat(f.style.left);
    let top = parseFloat(f.style.top);

    let visibleWidth = containerWidth - navWidth ;
    let visibleHeight = containerHeight - headerHeight;

    let isVisibleLeft =  left > scrollLeft - dayWidth - margin;
    let isVisibleRight = left < ( scrollLeft + visibleWidth + dayWidth + margin);
    let isVisibleTop = top > (scrollTop - itemHeight - margin);
    let isVisibleBottom = top < (scrollTop + visibleHeight + itemHeight + margin);

    return isVisibleLeft && isVisibleRight && isVisibleTop && isVisibleBottom;
  }


  onScrollContainer(){
    if(!this.refreshVirtualScrollDebounced){
      this.refreshVirtualScrollDebounced = utils.throttle(this.refreshVirtualScroll.bind(this), 100, 
        {leading: true, trailing: true});
    }
    this.refreshScrollUtilsVars();
    this.refreshVirtualScrollDebounced();
    this.hideContextMenu();
  }



  /**
   * Muestra Detail dialog y Refresca su posicion 
   * usando el evento del mouse
   *
   * @param {Object} evt - evento de mousemove, para posiciones en pantalla
   */
  refreshDetailDayPosition(position){
    let dayWidth = parseFloat(utils.getCSSVariable(this.config, "--day-width"));
    let itemHeight = parseFloat(utils.getCSSVariable(this.config, "--item-height"));

    this.$elements.$detailDay.style.top  = position.top + "px";
    this.$elements.$detailDay.style.left = position.left + dayWidth + "px";

    let yPositionClass = "has-top-arrow";
    let xPositionClass = "";

    //condicion para primeras 3 filas
    if(position.topClient <= itemHeight *3 ){
      yPositionClass = "has-ceil-arrow";
    }else if( position.yPercent > 20 &&
      position.yPercent <= 70){ //entre 20 y 50 %
      yPositionClass = "has-middle-arrow";
    }else if(position.yPercent > 70){
      yPositionClass = "has-bottom-arrow";
    }

    if(position.xPercent > 30 ){
      xPositionClass = "has-right-arrow";
    }

    //quita dd-hide, sobreescribiendo todas las clases
    this.$elements.$detailDay.className = "detail-day "+ yPositionClass + " " + xPositionClass;
  }



  /**
   * Captura evento click en boton para abrir arbol, en sidebar izquierdo
   *
   * @param {Object} evt - objeto evento click
   */
  clickNavLeftTreeIcon(){
    this.redraw({
      floatBox: true
    });
    this.refreshScrollUtilsVars();
    this.refreshVirtualScrollVars();
  }


  clickFloatBox(evt){
    if(this.isMobile){
      this.mouseOverFloatBox(evt, false);
    }
  }


  /**
   * Captura evento mouseover sobre elemento floatBox
   * usada para mostrar detalle de dia/item del floatbox
   */
  mouseOverFloatBox(evt, force){
      let item    = evt.detail.item;
      let dataDay = evt.detail.data;

      //verifica que sean dias distintos, comparandolo con el anterior,
      // para no crear nuevamente el mismo detail-day, con los mismos datos
      if(dataDay !== this.lastDataDay || force){
        let newDDContentString = tableElements.redrawDetailDay(this.config, item, dataDay, this.tabSelected);
        if(newDDContentString){
          this.$elements.$detailDay.innerHTML = newDDContentString;
        }
        this.lastDataDay = dataDay;
      }
  }

  clickDateHeader(evt){
    let date = evt.detail.date;
    this.toggleSelectByDate(date);
    this.redrawSelection();
  }

  toggleSelectByDate(date){
    if(this.isSelectedAllDate(date)){
      this.deselectByDate(date);
    }else{
      this.selectByDate(date);
    }
    this.selected = this.selected.sort((s, n) => {
      return (new Date(s.date)) - (new Date(n.date));
    });
    this.setIndexToSelected();
  }


  contextMenuDateHeader(evt){
    let left = evt.detail.clientX;
    let top = evt.detail.clientY - this.$capibara.getBoundingClientRect().top;
    let date = evt.detail.date;
    const target = evt.detail.$elem;

    let { elem:$elem, isVisible } = tableElements.createContextMenu({
      tabSelected: this.tabSelected,
      target,
      $capibaraElem: this.$capibara,
      config: this.config,
      selected: this.selected,
      date,
      left,
      top,
      name: "onDateHeader"
    });

    if($elem){
      this.isVisibleContextMenu = isVisible;
      this.$elements.$contextMenu.replaceWith($elem);
      this.$elements.$contextMenu = $elem;
    }

  }

  /**
   * Captura evento window resize
   * para verificar si es mobile o no
   */
  onWindowResize(){
    if(this.initialized){
      let oldValue = this.isMobile;
      let newValue = utils.checkIsMobile();
      if(oldValue != newValue ){
        this.isMobile = newValue;
        this.toggleSidebarMobile();
        this.setAllCSSVariables();
        this.redraw({ floatBox:true, navLeft:true });
      }
      this.refreshScrollUtilsVars();
      this.refreshVirtualScrollVars();
    }
  }

  /**
   * Cambia sidebar para mobile, segun variable this.isMobile
   */
  toggleSidebarMobile(){
      if(this.isMobile){
        this.setShowNavInitial(false);
        this.setShowNavExtended(false);
        this.setShowNavButtons(false);
        this.setEditable(false);
        this.setSelectable(false);
      }else{
        this.setShowNavInitial(true);
        this.setShowNavExtended(false);
        this.setShowNavButtons(true);
        this.setEditable(true);
        this.setSelectable(true);
      }
    this.refreshScrollUtilsVars();
    this.refreshVirtualScrollVars();
  }

  setEditable(newEditable){
    if(this.config.timeline.editable != newEditable){
      this.config.timeline.editable = newEditable;
      this.redraw({navLeft:true, header:true});
    }
  }

  setSelectable(newSelectable){
    if(this.config.timeline.selectable != newSelectable){
      this.config.timeline.selectable = newSelectable;
      this.cleanMultiInput();
    }
  }

  focusOnFloatBox({pos, evt}){
    if(this.isMobile){
      let containerWidth = this.$elements.$container.clientWidth;
      let dayWidth = parseFloat(utils.getCSSVariable(this.config, "--day-width"));
      let navWidth = parseFloat(utils.getCSSVariable(this.config, "--nav-width"));
      let left = pos.left - containerWidth + 1.5 * dayWidth + navWidth;

      //let containerHeight = this.$elements.$container.clientHeight;
      let itemHeight = parseFloat(utils.getCSSVariable(this.config, "--item-height"));
      //let headerHeight = parseFloat(utils.getCSSVariable(this.config, "--header-height"));
      let top = pos.top - 0.5 * itemHeight;

      let position = utils.eventToPosition(this.config, evt, this.$elements.$table);
      this.refreshDetailDayPosition({
        ...position,
        topClient : position.yInnerPercent >= 70 ? position.topClient : 0, // fuerza posicion de arrow en detailday
        xPercent  : position.x < 4 ? 0: 100  // fuerza arrow right
      });
      this.$elements.$container.scroll({
        top, left,
        behavior: 'smooth'
      });

    }
  }

  refreshInputsWithReload({detail}){
    this.$inputsWithReload = this.$inputsWithReload.map(({ $input, config}) => {
      let lastSelection = detail && detail.lastSelection ? detail.lastSelection : null;
      let newInput = tableElements.createInput(config, null, lastSelection, detail.selected);
      $input.parentElement.replaceChild(newInput, $input);
      let index = this.$inputsNavRight.indexOf($input);
      if(index >= 0){
        this.$inputsNavRight[index] = newInput;
      }
      return {$input: newInput, config}
    });
  }



  /**
   * Agrega eventos a los elementos del dom de la planilla
   * usando el objecto $capibara
   */
  addEvents(){
    this.$capibara.addEventListener("click"                , this.hideContextMenu.bind(this));
    this.$capibara.addEventListener("contextmenu"          , this.rightClickOnCapibara.bind(this));
    this.$capibara.addEventListener("toggleDark"           , this.toggleDark.bind(this));
    this.$capibara.addEventListener("toggleHeader"         , this.toggleHeader.bind(this));
    this.$capibara.addEventListener("toggleSidebar"        , this.toggleSidebar.bind(this));
    this.$capibara.addEventListener("toggleSidebarRight"   , this.toggleSidebarRight.bind(this));
    this.$capibara.addEventListener("saveMultiInput"       , this.saveInputsNavRight.bind(this));
    this.$capibara.addEventListener("cancelMultiInput"     , this.cancelMultiInput.bind(this));
    this.$capibara.addEventListener("overNavItem"          , this.overNavItem.bind(this));
    this.$capibara.addEventListener("leaveNavItem"         , this.leaveNavItem.bind(this));
    this.$capibara.addEventListener("clickNavLeftTreeIcon" , this.clickNavLeftTreeIcon.bind(this));
    this.$capibara.addEventListener("mouseOverFloatBox"    , this.mouseOverFloatBox.bind(this));
    this.$capibara.addEventListener("clickFloatBox"    , this.clickFloatBox.bind(this));
    this.$capibara.addEventListener("selectTabHeader"      , e => { this.selectTab(e.detail.tab); });
    this.$capibara.addEventListener("selectLegendHeader"   , this.selectLegend.bind(this));

    this.$capibara.addEventListener("selectDay"   , this.refreshInputsWithReload.bind(this));
    this.$capibara.addEventListener("deselectDay"   , this.refreshInputsWithReload.bind(this));

    this.$elements.$floatBoxContainer.addEventListener("mousemove", this.mouseMoveOnFlexBoxContainer.bind(this));
    this.$elements.$floatBoxContainer.addEventListener("mouseleave", this.mouseLeaveOnFloatBoxContainer.bind(this));

    this.$elements.$table.addEventListener("mousemove", this.mouseMoveOnTable.bind(this) );
    this.$elements.$table.addEventListener("click", this.clickOnTable.bind(this));
    this.$elements.$table.addEventListener("contextmenu", this.rightClickOnTable.bind(this));

    this.$elements.$container.addEventListener("scroll", this.onScrollContainer.bind(this));

    this.$elements.$container.addEventListener("clickDate", this.clickDateHeader.bind(this));
    this.$elements.$container.addEventListener("contextMenuDate", this.contextMenuDateHeader.bind(this));

    window.addEventListener('resize',this.onWindowResize.bind(this));
    document.addEventListener("click", this.handleClickOutside.bind(this));
  }




  /**
   * Funcion para redibujar partes de la planilla, indicado segun parametro
   *
   *    options : {
   *            floatBox: Boolean,
   *            plots: Boolean,
   *            header: Boolean,
   *            navLeft: Boolean,
   *            navRight: Boolean,
   *    }
   *
   *
   * @param {Object} options - objecto de configuracion
   */
  redraw(options){

    if(options.floatBox){
      tableElements.redrawFloatBox(this.config, this.tabSelected, this.$elements.$floatBoxContainer);
      tableElements.redrawBackground(this.config, this.$elements.$background);
      tableElements.redrawPlotVerticalContainer(this.config, this.$elements.$plotVerticalContainer);
      tableElements.redrawPlotHorizontalContainer(this.config, this.$elements.$plotHorizontalContainer);
      tableElements.redrawTodayLine(this.config, this.$elements.$todayLine);
    }

    if(options.plots){
      tableElements.redrawPlotVerticalContainer(this.config, this.$elements.$plotVerticalContainer);
      tableElements.redrawPlotHorizontalContainer(this.config, this.$elements.$plotHorizontalContainer);
    }

    if(options.header){
      tableElements.redrawHeader(this.config, this.$elements.$header);
      if(this.config.timeline.header.extend.enabled !== false){
        let $headerExtended = tableElements.createHeaderExtended(this.config);
        if($headerExtended){
          this.$elements.$header.append($headerExtended);
        }
      }
    }

    if(options.headerTop){
      this.$elements.$headerTop = tableElements.redrawHeaderTop(this.config, this.tabSelected, this.$elements.$headerTop);
    }

    if(options.detailDay){
      this.hideDetailDay();
      this.lastDataDay = null;
    }

    if(options.navLeft){
      tableElements.redrawNavLeft(this.$elements.$navLeft, this.config, this.tabSelected);
      styleUtils.setNavStyle(this.config);
      styleUtils.setNavLeftInitialStyle(this.config, this.isShowNavInitial, this.tabSelected);
      styleUtils.setNavLeftExtendedStyle(this.config, this.isShowNavExtended, this.tabSelected);
    }

    if(options.cornerLeft){
      tableElements.redrawCorner(this.$elements.$corner, this.config, this.tabSelected);
    }

    if(options.cornerRight){
        this.$elements.$cornerRight = tableElements.redrawCornerRight(this.$elements.$cornerRight, this.config, this.tabSelected);
    }

    if(options.navRight){
      let dataDay = null;
      let daySelected = null;

      if(this.selected.length > 0){
        daySelected = this.selected[0]
        if(daySelected.item.data){
          dataDay = daySelected.item.data.find(d => {
            return d.date == daySelected.date;
          });
        }
      }

      let {inputs, element, inputsWithReload, saveCancelBtns} = tableElements.redrawNavRight(this.config, this.$elements.$navRight, this.selected , this.tabSelected, dataDay, this.selected[0]);
      this.$elements.$navRight = element;
      this.$inputsNavRight     = inputs;
      this.$inputsWithReload = inputsWithReload;
      this.$saveCancelBtns = saveCancelBtns;
    }

    this.refreshScrollUtilsVars();
    this.refreshVirtualScrollVars();

  }


  /**
   * Mueve el Scroll de la planilla al inicio
   */
  scrollToStart(){
    this.$elements.$container.scrollLeft = 0;
  }

  /**
   * Mueve el Scroll de la planilla al inicio
   */
  scrollToEnd(){
    this.$elements.$container.scrollLeft = this.$elements.$container.scrollWidth;
  }

  /**
   * Mueve el Scroll de la planilla a la tercera parte
   * TODO mejorar logica de esta funcion
   */
  scrollToMiddle(){
    this.$elements.$container.scrollLeft = this.$elements.$container.scrollWidth / 3;
  }



  /**
   * Fija las variables de CSs segun Config
   */
  setAllCSSVariables(){
    let config = this.config;
    //styleUtils.cleanStyle(config);
    styleUtils.setHeaderStyle(config);
    styleUtils.setNavStyle(config);
    styleUtils.setNavLeftInitialStyle(config, this.isShowNavInitial, this.tabSelected);
    styleUtils.setNavLeftExtendedStyle(config, this.isShowNavExtended, this.tabSelected);
    styleUtils.setNavLeftButtonsStyle(config, this.isShowNavButtons, this.tabSelected);
    styleUtils.setTableStyle(config);
  }

}
