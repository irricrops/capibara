events



corner
- toggleSidebar



createHeaderTop
- toggleDark
- clickCalendarIcon
- toggleHeader
- toggleSidebarRight
- headerPrev
- headerCalendar
- headerNext
- selectTab
    - detail.{ tab }



headerExtend
- changeInputHeader
    - detail.{ date, typeData }



navLeft
- clickNavLeftItem
  - detail.{ itemId, type, value}
- saveNavLeftInput
  - detail.{ key, itemId, type, value}
- clickNavLeftButton
  - detail.{ key, itemId, type}
- clickNavLeftTreeIcon
  - detail.{ itemId, type}

floatBox
- mouseOverFloatBox
  - detail.{ item, data }


navRight
- saveMultiInput
- cancelMultiInput
