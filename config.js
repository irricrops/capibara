var config = {
  selector: "#timeline",
  timeline: {
    start: "2020-10-30",
    end: "2021-01-31",
    dark: false,
    loading: false,
    //editable: false,
    //selectable:false,
    plots: {
      horizontal: [
        {
          idNode: 1,
          typeNode: "parent",
          className: "plot-parent",
        },
        {
          idNode: 148,
          typeNode: "parent",
          className: "plot-parent",
        },
      ],
      vertical: [
        {
          date: "2020-12-14 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-15 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-16 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-17 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-18 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-19 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-20 00:00:00",
          className: "rain",
        },
        {
          date: "2020-12-21 00:00:00",
          className: "rain",
        },
      ],
    },
    tabs: [
      {
        id: 1,
        label: "Riego",
        color: "c-teal",
        floatBox: {
          nums: [
            {
              varName: "riego.mm",
              color: "c-blue",
            },
            {
              varName: "scholander.value",
              color: "c-deep-orange",
            },
            {
              varName: "programado.hh",
              color: "c-green",
            },
            {
              varName: "sugerencia.hh",
              color: "c-brown",
            },
          ],

          rowDots: [
            [
              {
                varName: "aplicaciones_count.insecticida_real",
                color: "#795548",
              },
              {
                varName: "aplicaciones_count.insecticida_prog",
                color: "#795548",
                shape:"rounded"
              },
              {
                varName: "aplicaciones_count.insecticida_proy",
                color: "#795548",
                shape:"outlined-rounded"
              },
            ],
            [

              {
                varName: "aplicaciones_count.herbicida_real",
                color: "#ffc107",
              },
              {
                varName: "aplicaciones_count.herbicida_prog",
                color: "#ffc107",
                shape:"rounded"
              },
              {
                varName: "aplicaciones_count.herbicida_proy",
                color: "#ffc107",
                shape:"outlined-rounded"
              },
            ],
            [

              {
                varName: "aplicaciones_count.fungicida_real",
                color: "#1976d2",
              },
              {
                varName: "aplicaciones_count.fungicida_prog",
                color: "#1976d2",
                shape:"rounded"
              },
              {
                varName: "aplicaciones_count.fungicida_proy",
                color: "#1976d2",
                shape:"outlined-rounded"
              },
            ],
            [

              {
                varName: "aplicaciones_count.foliar_real",
                color: "#2e7d32",
              },
              {
                varName: "aplicaciones_count.foliar_prog",
                color: "#2e7d32",
                shape:"rounded"
              },
              {
                varName: "aplicaciones_count.foliar_proy",
                color: "#2e7d32",
                shape:"outlined-rounded"
              },
            ],
          ],
          dots: [
            {
              varName: "scholander.value",
              color: "c-deep-orange",
            },
            {
              varName: "porometro.value",
              color: "c-teal",
            },
            {
              varName: "calicata",
              color: "c-teal",
            },
            {
              varName: "gramaje",
              color: "c-teal",
            },
            {
              varName: "calibre",
              color: "c-teal",
            },
            {
              varName: "sensorHumedad",
              color: "c-deep-purple",
            },
          ],
        },
        inputs: [
          {
            label:"test custom",
            type: "custom",
            key:"custom_input_test",
            render({
              props, 
              dataDay, 
              daySelected, 
              selected
            }){
              let domElement = document.createElement("div");
              domElement.classList.add("classname-test")
              domElement.appendChild(document.createTextNode("test node"))
              return domElement;
            },
            getValue({
              elem, props
            }){
              return 5;
            }
          },
          {
            label: "Horas de Riego",
            type: "time",
            key: "horas_riego",
            varName: "values.riego.hh",
          },
          {
            label: "Volumen de Riego",
            type: "number",
            suffix: "m<sup>3</sup>",
            key: "volumen",
            varName: "values.riego.volumen",
          },
          {
            label: "¿Con fertilización?",
            type: "checkbox",
            key: "con_fertilizacion",
            varName: "values.con_fertilizacion",
          },
          {
            label: "test button",
            type: "button",
            key: "test_btn",
            onClick(){
              alert("test button on click");
            }
          },
          {
            label: "fertilización",
            type: "list",
            key: "fertilizacion_list",
            showIf: "con_fertilizacion",
            varName: "values.fertilizacion",
            inputs: [
              {
                id: 1,
                label: "Producto numero 1 con un nombre largo",
                tooltip: "Producto numero 1 con un nombre largo ______",
                key: "producto_1",
              },
              {
                id: 2,
                label: "P 2",
                key: "producto_2",
              },
              {
                id: 3,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 4,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 5,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 6,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 7,
                label: "P 3",
                key: "producto_3",
              },
            ],
          },
          {
            label: "fertilización",
            type: "input-box",
            key: "fertilizacion_list",
            showIf: "con_fertilizacion",
            varName: "values.fertilizacion",
            inputs: [
              {
                id: 1,
                label: "Producto numero 1 con un nombre largo",
                tooltip: "Producto numero 1 con un nombre largo ______",
                key: "producto_1",
              },
              {
                id: 2,
                label: "P 2",
                key: "producto_2",
              },
              {
                id: 3,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 4,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 5,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 6,
                label: "P 3",
                key: "producto_3",
              },
              {
                id: 7,
                label: "P 3",
                key: "producto_3",
              },
            ],
          },
            {
              label:"Aplicador",
              type:"select",
              key:"fertilizacion_aplicador",
              showIf:"con_fertilizacion",
              varName: "values.aplicadorId",
              options:[{
                value: 1,
                label: "test"
              }]
            },
          {
            label: "Mensaje",
            type: "message",
            key: "msg_input",
            varName: "message.value",
          },
        ],
      },
      {
        id: 6,
        label: "Programación",
        color: "c-green",
        tooltip: "deshabilitado",
        inputs: [
          {
            label: "Horas Programadas",
            type: "time",
            key: "horas_programadas",
            varName: "values.programado.hh",
          },
          {
            label: "Mensaje",
            type: "message",
            key: "msg_input",
            varName: "message.value",
          },
        ],
      },
      {
        id: 2,
        label: "Calicata",
        color: "c-brown",
        tooltip: "habilitado",
        inputs: [
          {
            label: "Calicata",
            type: "arrayNumber",
            key: "calicata",
            varName: "values.calicata",
          },
          {
            label:"inputList",
            type:"inputList",
            key:"inputListTest",
            inputList:"inputListItems", // se obtiene desde data, para crear lista
            valuesPath: "values.inputListValues",
            toRight: true,

          },
          {
            label: "inputBox",
            type: "input-box",
            key: "inputBoxTest",
            varName: "values.inputBoxValues",
            varNameInput: "valueBox",
            tabs:[
              {
                key: "tab1",
                name: "tab 1",
              },
              {
                key: "tab2",
                name: "tab 1",
              }
            ],
            inputs:[
              {
                label: "header 1",
                isTitle:true,
                tooltip:"tooltip",
                suffix: "suff",
                preffix: "pref",
                colorIcon:"red",
                tabKey: "tab1",

              },{
                id: 1,
                tooltip: "tooltip input",
                colorIcon: "green",
                link: "https://www.google.com",
                tabKey: "tab1",
                label: "input 1",
                suffix: "suff",
                preffix: "pref",
              },
              {
                id: 2,
                tooltip: "tooltip input",
                colorIcon: "#FF0000",
                link: "https://www.google.com",
                tabKey: "tab1",
                label: "input 1",
                suffix: "suff",
                preffix: "pref",
              },
              {
                label: "header 2",
                isTitle:true,
                tooltip:"tooltip",
                suffix: "suff",
                preffix: "pref",
                colorIcon:"red",
                tabKey: "tab2",

              },{
                id: 3,
                tooltip: "tooltip input",
                colorIcon: "green",
                link: "https://www.google.com",
                tabKey: "tab2",
                label: "input 3",
                suffix: "suff",
                preffix: "pref",
              }
            ]
          },

          {
            label: "list",
            type: "list",
            key: "listTest",
            varName: "values.listValues",
            tabs:[
              {
                key: "tab1",
                name: "tab 1",
              },
              {
                key: "tab2",
                name: "tab 1",
              }
            ],
            inputs:[
              {
                label: "header 1",
                isTitle:true,
                tooltip:"tooltip",
                suffix: "suff",
                preffix: "pref",
                colorIcon:"red",
                tabKey: "tab1",

              },{
                id: 1,
                tooltip: "tooltip input",
                colorIcon: "green",
                link: "https://www.google.com",
                tabKey: "tab1",
                label: "input 1",
                suffix: "suff",
                preffix: "pref",
              },
              {
                id: 2,
                tooltip: "tooltip input",
                colorIcon: "#FF0000",
                link: "https://www.google.com",
                tabKey: "tab1",
                label: "input 1",
                suffix: "suff",
                preffix: "pref",
              },
              {
                label: "header 2",
                isTitle:true,
                tooltip:"tooltip",
                suffix: "suff",
                preffix: "pref",
                colorIcon:"red",
                tabKey: "tab2",

              },{
                id: 3,
                tooltip: "tooltip input",
                colorIcon: "green",
                link: "https://www.google.com",
                tabKey: "tab2",
                label: "input 3",
                suffix: "suff",
                preffix: "pref",
              }
            ]
          },



          {
            label: "data list test",
            type: "data-list",
            key: "dataListTest",
            varName: "values.dataListValues",
            varNameInput: "value",
            varNameLabel: "label",
            varNameValueText:"valueText",
            varNameSubText: "subText",
            varNameSuffix: "suffix",
            varNameLink: "link",
            varNameTooltip: "tooltip",
            editButton: {
              label: "Edit",
              key: "edit_button",
              onClick() {
                alert("clickEdit")
              },
            },
          }
        ],
      },
      {
        id: 3,
        label: "Sensores",
        color: "c-deep-purple",
        inputs: [
          {
            label: "Sensores de Humedad",
            type: "arrayNumber",
            key: "sensores_humedad",
            varName: "values.sensorHumedad",
          },
        ],
      },
      {
        id: 4,
        label: "Porometro",
        color: "c-light-blue",
        inputs: [
          {
            label: "Porometro",
            type: "number",
            key: "porometro",
            varName: "values.porometro.value",
          },
        ],
      },
      {
        id: 5,
        label: "Scholander",
        color: "c-deep-orange",
        selectableIfHas: "sector_scholander_id",
        floatBox: {
          nums: [
            {
              varName: "scholander.value",
              color: "c-orange",
            },
          ],
        },
        inputs: [
          {
            label: "Scholander",
            type: "number",
            key: "scholander",
            varName: "values.scholander.value",
          },
        ],
      },
    ],
    corner: {
      left: {
        label: "Sectores",
      },
      initial: {},
      extend: {},
      right: {
        label: "",
      },
    },
    footer: {
      enabled: false,
      label: "Totales",
      data: [
        {
          value: 1,
        },
        {
          value: 2,
        },
        {
          value: 3,
        },
        {
          value: 4,
        },
      ],
    },
    nav: {
      columnWidth: 50,
      left: {
        initial: {
          enabled: false,
          width: 200,
          mobile: {
            width: 140,
          },
          vars: [
            {
              label: "Riego Nominal <br /> mm",
              varName: "riego_nominal_mm",
              input: "number",
              key: "riego_nominal_mm",
            },
            {
              label: "ha",
              varName: "ha",
              round: 2,
            },
            {
              label: "caudal <br> m<sup>3</sup>/hr",
              varName: "caudal",
              round: 2,
            },
            {
              label: "precip <br> mm/hr",
              varName: "precip",
              round: 2,
            },
          ],
        },
        extend: {
          enabled: false,
          vars: [
            {
              label: "Riego Nominal <br /> mm",
              varName: "riego_nominal_mm",
              input: "number",
              key: "riego_nominal_mm",
            },
            {
              label: "Riego Nominal <br /> HH",
              varName: "riego_nominal_hh",
              input: "hour",
              key: "riego_nominal_hh",
            },
            {
              label: "Rep <br /> %",
              varName: "rep",
            },
            {
              label: "Rep <br /> Hoy",
              varName: "rep_hoy",
              round: 1,
              suffix: "%",
            },
            {
              label: "Et Acum <br /> mm",
              varName: "et_acum_mm",
            },
            {
              label: "Limite <br /> Sch",
              varName: "limite_scholander",
            },
          ],
        },
        buttons: {
          width: 90,
          vars: [
            {
              shape: "drop",
              tooltip: "tooltip hover",
              className: "",
              key: "icon-estados-fenologicos",
            },
            {
              shape: "circle",
              tooltip: "tooltip hover",
              className: "",
              key: "icon-etapas-fertilizacion",
              formatter(item, btn){
                return 0;
              },
              btnClassNameFormatter(item, btn){
                let className = null;
                if(btn.key = "icon-etapas-fertilización"){
                  let value = 0;
                  if(btn.formatter){
                    value = btn.formatter(item, btn);
                  }
                  className = value == 0 ? 'is-empty':null;
                }
                return className; 
              },
            },
          ],
        },
      },
      right: {},
    },
    header: {
      top: {
        //height: 80,
      },
      extend: {
        //enabled: true,
        isShow: false,
        data: [
          {
            label: "Eto",
            color: "green",
            suffix:"mm",
            average: 12,
            isAccent :true,
            isPersistent: true,
            editableFuture: false,
            editableToday: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 5,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 5,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 5,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 5,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 5,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 5,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 4,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 1,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 1,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 1,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 1,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 1,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 1,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 1,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 1,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 2,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 3,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 4,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 5,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 6,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 7,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "Etb",
            color: "teal",
            average: 12,
            isPersistent: true,
            editableFuture: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 0,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 0,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 0,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 0,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 0,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 0,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 0,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 2,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 2,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 2,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 2,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 2,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 2,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 2,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 2,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 0,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 0,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 0,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 0,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 0,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 0,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "Precip",
            color: "blue",
            editableFuture: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 0,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 0,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 0,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 0,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 0,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 0,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 0,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 3,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 3,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 3,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 3,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 3,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 3,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 3,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 3,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 0,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 0,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 0,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 0,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 0,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 0,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "T min",
            color: "red-d",
            editableFuture: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 0,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 0,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 0,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 0,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 0,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 0,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 0,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 4,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 4,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 4,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 4,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 4,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 4,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 4,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 4,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 0,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 0,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 0,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 0,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 0,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 0,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "T med",
            color: "red-d",
            editableFuture: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 0,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 0,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 0,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 0,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 0,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 0,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 0,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 6,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 6,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 6,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 6,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 6,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 6,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 6,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 6,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 0,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 0,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 0,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 0,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 0,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 0,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "T max",
            color: "red-d",
            editableFuture: false,
            data: [
              {
                date: "2020-11-09 00:00:00",
                unix: 1604890800,
                value: 0,
              },
              {
                date: "2020-11-10 00:00:00",
                unix: 1604977200,
                value: 0,
              },
              {
                date: "2020-11-11 00:00:00",
                unix: 1605063600,
                value: 0,
              },
              {
                date: "2020-11-12 00:00:00",
                unix: 1605150000,
                value: 0,
              },
              {
                date: "2020-11-13 00:00:00",
                unix: 1605236400,
                value: 0,
              },
              {
                date: "2020-11-14 00:00:00",
                unix: 1605322800,
                value: 0,
              },
              {
                date: "2020-11-15 00:00:00",
                unix: 1605409200,
                value: 0,
              },
              {
                date: "2020-11-16 00:00:00",
                unix: 1605495600,
                value: 0,
              },
              {
                date: "2020-11-17 00:00:00",
                unix: 1605582000,
                value: 0,
              },
              {
                date: "2020-11-18 00:00:00",
                unix: 1605668400,
                value: 0,
              },
              {
                date: "2020-11-19 00:00:00",
                unix: 1605754800,
                value: 0,
              },
              {
                date: "2020-11-20 00:00:00",
                unix: 1605841200,
                value: 0,
              },
              {
                date: "2020-11-21 00:00:00",
                unix: 1605927600,
                value: 0,
              },
              {
                date: "2020-11-22 00:00:00",
                unix: 1606014000,
                value: 0,
              },
              {
                date: "2020-11-23 00:00:00",
                unix: 1606100400,
                value: 0,
              },
              {
                date: "2020-11-24 00:00:00",
                unix: 1606186800,
                value: 0,
              },
              {
                date: "2020-11-25 00:00:00",
                unix: 1606273200,
                value: 0,
              },
              {
                date: "2020-11-26 00:00:00",
                unix: 1606359600,
                value: 0,
              },
              {
                date: "2020-11-27 00:00:00",
                unix: 1606446000,
                value: 0,
              },
              {
                date: "2020-11-28 00:00:00",
                unix: 1606532400,
                value: 0,
              },
              {
                date: "2020-11-29 00:00:00",
                unix: 1606618800,
                value: 0,
              },
              {
                date: "2020-11-30 00:00:00",
                unix: 1606705200,
                value: 0,
              },
              {
                date: "2020-12-01 00:00:00",
                unix: 1606791600,
                value: 0,
              },
              {
                date: "2020-12-02 00:00:00",
                unix: 1606878000,
                value: 0,
              },
              {
                date: "2020-12-03 00:00:00",
                unix: 1606964400,
                value: 0,
              },
              {
                date: "2020-12-04 00:00:00",
                unix: 1607050800,
                value: 0,
              },
              {
                date: "2020-12-05 00:00:00",
                unix: 1607137200,
                value: 0,
              },
              {
                date: "2020-12-06 00:00:00",
                unix: 1607223600,
                value: 0,
              },
              {
                date: "2020-12-07 00:00:00",
                unix: 1607310000,
                value: 0,
              },
              {
                date: "2020-12-08 00:00:00",
                unix: 1607396400,
                value: 0,
              },
              {
                date: "2020-12-09 00:00:00",
                unix: 1607482800,
                value: 0,
              },
              {
                date: "2020-12-10 00:00:00",
                unix: 1607569200,
                value: 0,
              },
              {
                date: "2020-12-11 00:00:00",
                unix: 1607655600,
                value: 0,
              },
              {
                date: "2020-12-12 00:00:00",
                unix: 1607742000,
                value: 0,
              },
              {
                date: "2020-12-13 00:00:00",
                unix: 1607828400,
                value: 0,
              },
              {
                date: "2020-12-14 00:00:00",
                unix: 1607914800,
                value: 5,
              },
              {
                date: "2020-12-15 00:00:00",
                unix: 1608001200,
                value: 5,
              },
              {
                date: "2020-12-16 00:00:00",
                unix: 1608087600,
                value: 5,
              },
              {
                date: "2020-12-17 00:00:00",
                unix: 1608174000,
                value: 5,
              },
              {
                date: "2020-12-18 00:00:00",
                unix: 1608260400,
                value: 5,
              },
              {
                date: "2020-12-19 00:00:00",
                unix: 1608346800,
                value: 5,
              },
              {
                date: "2020-12-20 00:00:00",
                unix: 1608433200,
                value: 5,
              },
              {
                date: "2020-12-21 00:00:00",
                unix: 1608519600,
                value: 5,
              },
              {
                date: "2020-12-22 00:00:00",
                unix: 1608606000,
                value: 0,
              },
              {
                date: "2020-12-23 00:00:00",
                unix: 1608692400,
                value: 0,
              },
              {
                date: "2020-12-24 00:00:00",
                unix: 1608778800,
                value: 0,
              },
              {
                date: "2020-12-25 00:00:00",
                unix: 1608865200,
                value: 0,
              },
              {
                date: "2020-12-26 00:00:00",
                unix: 1608951600,
                value: 0,
              },
              {
                date: "2020-12-27 00:00:00",
                unix: 1609038000,
                value: 0,
              },
              {
                date: "2020-12-28 00:00:00",
                unix: 1609124400,
                value: 0,
              },
              {
                date: "2020-12-29 00:00:00",
                unix: 1609210800,
                value: 0,
              },
              {
                date: "2020-12-30 00:00:00",
                unix: 1609297200,
                value: 0,
              },
              {
                date: "2020-12-31 00:00:00",
                unix: 1609383600,
                value: 0,
              },
              {
                date: "2021-01-01 00:00:00",
                unix: 1609470000,
                value: 0,
              },
              {
                date: "2021-01-02 00:00:00",
                unix: 1609556400,
                value: 0,
              },
              {
                date: "2021-01-03 00:00:00",
                unix: 1609642800,
                value: 0,
              },
              {
                date: "2021-01-04 00:00:00",
                unix: 1609729200,
                value: 0,
              },
              {
                date: "2021-01-05 00:00:00",
                unix: 1609815600,
                value: 0,
              },
              {
                date: "2021-01-06 00:00:00",
                unix: 1609902000,
                value: 0,
              },
              {
                date: "2021-01-07 00:00:00",
                unix: 1609988400,
                value: 0,
              },
              {
                date: "2021-01-08 00:00:00",
                unix: 1610074800,
                value: 0,
              },
              {
                date: "2021-01-09 00:00:00",
                unix: 1610161200,
                value: 0,
              },
              {
                date: "2021-01-10 00:00:00",
                unix: 1610247600,
                value: 0,
              },
              {
                date: "2021-01-11 00:00:00",
                unix: 1610334000,
                value: 0,
              },
              {
                date: "2021-01-12 00:00:00",
                unix: 1610420400,
                value: 0,
              },
              {
                date: "2021-01-13 00:00:00",
                unix: 1610506800,
                value: 0,
              },
              {
                date: "2021-01-14 00:00:00",
                unix: 1610593200,
                value: 0,
              },
              {
                date: "2021-01-15 00:00:00",
                unix: 1610679600,
                value: 0,
              },
              {
                date: "2021-01-16 00:00:00",
                unix: 1610766000,
                value: 0,
              },
              {
                date: "2021-01-17 00:00:00",
                unix: 1610852400,
                value: 0,
              },
              {
                date: "2021-01-18 00:00:00",
                unix: 1610938800,
                value: 0,
              },
              {
                date: "2021-01-19 00:00:00",
                unix: 1611025200,
                value: 0,
              },
              {
                date: "2021-01-20 00:00:00",
                unix: 1611111600,
                value: 0,
              },
              {
                date: "2021-01-21 00:00:00",
                unix: 1611198000,
                value: 0,
              },
              {
                date: "2021-01-22 00:00:00",
                unix: 1611284400,
                value: 0,
              },
              {
                date: "2021-01-23 00:00:00",
                unix: 1611370800,
                value: 0,
              },
              {
                date: "2021-01-24 00:00:00",
                unix: 1611457200,
                value: 0,
              },
              {
                date: "2021-01-25 00:00:00",
                unix: 1611543600,
                value: 0,
              },
              {
                date: "2021-01-26 00:00:00",
                unix: 1611630000,
                value: 0,
              },
              {
                date: "2021-01-27 00:00:00",
                unix: 1611716400,
                value: 0,
              },
              {
                date: "2021-01-28 00:00:00",
                unix: 1611802800,
                value: 0,
              },
              {
                date: "2021-01-29 00:00:00",
                unix: 1611889200,
                value: 0,
              },
              {
                date: "2021-01-30 00:00:00",
                unix: 1611975600,
                value: 0,
              },
              {
                date: "2021-01-31 00:00:00",
                unix: 1612062000,
                value: 0,
              },
            ],
          },
          {
            label: "oidio",
            editable: false,
            data: [],
          },
        ],
      },
    },
    day: {
      width: 60,
      height: 60,
      mobile: {
        width: 30,
        height: 35,
      },
    },
    detailDay: {
      pillsTitle: "Unidades / ha:",
      pillsVarName: "elementos",
      capsules: [
        {
          title: "Riego Real",
          color: "c-blue",
          values: [
            {
              name: "Riego MM",
              varName: "riego.mm",
              suffix: " mm",
            },
            {
              name: "Riego HH",
              varName: "riego.hh",
              suffix: " hrs",
            },
            {
              name: "Volumen",
              varName: "riego.volumen",
              suffix: " m<sup>3</sup>",
            },
          ],
        },
        {
          title: "Et acum",
          color: "c-light-blue",
          values: [
            {
              name: "Et acum",
              varName: "et_acum.value",
              suffix: " mm",
            },
          ],
        },
        {
          title: "Reposición",
          color: "c-light-blue",
          values: [
            {
              name: "reposicion",
              varName: "reposicion_real.value",
              suffix: " %",
            },
          ],
        },
        {
          title: "Sugerencia",
          color: "c-brown",
          values: [
            {
              name: "sugerencia horas",
              varName: "sugerencia.hh",
              suffix: " horas",
            },
            {
              name: "sugerencia milimetros",
              varName: "sugerencia.mm",
              suffix: " mm",
            },
          ],
        },
        {
          title: "Scholander",
          color: "c-deep-orange",
          values: [
            {
              name: "bars",
              varName: "scholander.value",
              suffix: " bar",
            },
          ],
        },
        {
          title: "Programado",
          color: "c-green",
          values: [
            {
              name: "Horas programadas",
              varName: "programado.hh",
              suffix: " Hrs",
            },
            {
              name: "mm programados",
              varName: "programado.mm",
              suffix: " mm",
            },
          ],
        },
        {
          title: "Porometro",
          color: "c-teal",
          values: [
            {
              name: "porometro",
              varName: "porometro.value",
              suffix: " %",
            },
          ],
        },
        {
          title: "Fertilización",
          color: "c-brown",
          isLabeledArray: true,
          values: [
            {
              name: "fert",
              varName: "aplicaciones",
            },
          ],
        },
        {
          title: "Sensor Humedad",
          color: "c-deep-purple",
          shape: "diamond",
          isLabeledArray: true,
          values: [
            {
              name: "sens hum",
              varName: "sensorHumedad",
              suffix: " %",
            },
          ],
        },
        {
          title: "Calicata",
          color: "c-teal",
          isLabeledArray: true,
          values: [
            {
              name: "calicata",
              varName: "calicata",
            },
          ],
        },
      ],
    },
  },
  data: [],
};
